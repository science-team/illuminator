dnl Autoconf macro to check for Illuminator library
dnl Copyright 2002 Adam Powell, redistributable under the terms of the GNU
dnl General Public License version 2 or later.

AC_DEFUN([ILLUMINATOR_CHECKS],[
	PETSC_CHECKS
	AM_PATH_GTK_2_0()
	PKG_CHECK_MODULES(IMLIB2, imlib2 >= 1.1)

	dnl AC_CHECK_HEADER(illuminator.h, ,
		dnl AC_ERROR("Missing Illuminator header file"),
		dnl INCLUDES="-I$PETSC_DIR/include")

	aLIBS="$LIBS"
	LIBS="$LIBS $PETSC_DM_LIB"
	dnl Use of DATriangulateLocalRange makes sure we have version 0.9+
	AC_CHECK_LIB(luminate, DATriangulateLocalRange,
		ILLUMINATOR_LIBS="-lluminate"
		ILLUMINATOR_CFLAGS="-I$PETSC_DIR/include $GTK_CFLAGS $IMLIB2_CFLAGS",
		AC_ERROR("Illuminator library \(version>=0.11.0\) not installed"))
	AC_SUBST(ILLUMINATOR_LIBS)
	AC_SUBST(ILLUMINATOR_CFLAGS)
	LIBS="$aLIBS"
])
