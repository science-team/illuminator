/***************************************
  $Header$

  Common files for cahnhill.c and programs which use it (e.g. chts.c), based on
  PETSc SNES tutorial common8and9.h.
***************************************/


#ifndef CAHNHILL_H
#define CAHNHILL_H

/* 
   Include "petscda.h" so that we can use distributed arrays (DAs).
   Include "petscts.h" so that we can use TS and SNES solvers.
   Note that this file automatically includes:
     petsc.h       - base PETSc routines  petscvec.h  - vectors
     petscsys.h    - system routines      petscmat.h  - matrices
     petscis.h     - index sets           petscksp.h  - Krylov subspace methods
     petscviewer.h - viewers              petscpc.h   - preconditioners
     petscsles.h   - linear solvers       petscsnes.h - nonlinear solvers
*/
#include <petscts.h>
#include <petscda.h>

/*+
   User-defined application context for chts.c - contains data needed by the
   application-provided callbacks: ch_residual_vector_xd() (x is 2 or 3). 
+*/

typedef struct {
  PetscTruth threedee;        /* obvious :-) */
  PetscScalar kappa,epsilon,gamma,mparam;  /* physical parameters */
  int        mx,my,mz;        /* discretization in x, y and z directions */
  int        mc;              /* components in unknown vector */
  int        chvar;           /* Which component in unks is Cahn-Hill */
  Vec        localX,localF;   /* ghosted local vector */
  DA         da;              /* distributed array data structure (unknowns) */
  int        rank;            /* processor rank */
  int        size;            /* number of processors */
  MPI_Comm   comm;            /* MPI communicator */
  int        ilevel,nlevels;  /* current/total levels through problem */
  Vec        x_old;           /* old solution vector */
  Mat        J, alpha;        /* Jacobian matrix, alpha values */
  DAPeriodicType period;      /* Periodicity: DA_XPERIODIC etc */
  ISLocalToGlobalMapping isltog; /* mapping from local-to-global indices */
  PetscViewer     theviewer;       /* Viewer for timesteps */
  char       **label;         /* labels for components */
  PetscTruth print_grid;      /* flag - 1 indicates printing grid info */
  PetscTruth print_vecs;      /* flag - 1 indicates printing vectors */
  PetscTruth no_contours;     /* flag - 1 indicates no contours */
  PetscTruth random;          /* flag - 1 indicates random initial condition */
  PetscTruth save_data;       /* flag - 1 indicates save each timestep */
  int load_data;              /* Timestep to load for IC, -1 if no load */
} AppCtx;

/*
   Define macros to allow us to easily access the components of the PDE
   solution and nonlinear residual vectors.  mc and chvar must be variables
   where these are used.
*/
#define C(i)     (mc*(i)+chvar)
/* #define U(i)     (mc*(i))
#define V(i)     (mc*(i)+1)
#define Omega(i) (mc*(i)+2)
#define Temp(i)  (mc*(i)+3) */

/* 
   User-defined routines in cahnhill.c
*/
extern int ch_residual_vector_2d (Vec,Vec,void*);
extern int ch_residual_vector_3d (Vec,Vec,void*);
extern int ch_jacobian_2d (Vec,Mat*,Mat*,MatStructure*,void*);
extern int ch_jacobian_3d (Vec,Mat*,Mat*,MatStructure*,void*);
extern int ch_jacobian_alpha_2d (AppCtx*);
extern int ch_jacobian_alpha_3d (AppCtx*);

#endif /* CAHNHILL_H */
