/***************************************
  $Header$

  This is the interface for the Illuminator library.
***************************************/

#ifndef ILLUMINATOR_H
#define ILLUMINATOR_H    /*+ To stop multiple inclusions. +*/
#include <petscda.h>
#include <glib.h>
#include <gtk/gtk.h>

/*+ The
  +latex+{\tt ISurface}
  +html+ <tt>ISurface</tt>
  type is the container (or object class) for triangle data which illuminator
  will render. +*/
typedef void * ISurface;

int ISurfCreate (ISurface *newsurf);
int ISurfDestroy (ISurface Surf);
int ISurfClear (ISurface Surf);

/*+ The
  +latex+{\tt IDisplay}
  +html+ <tt>IDisplay</tt>
  type is the container for display data, such as the geomview output pipe, RGB
  buffer, multi-layer z-buffer, etc. +*/
typedef void * IDisplay;

int IDispCreate (IDisplay *newdisp, int width,int height,int rowskip,int bpp,
		 int zbuf);
int IDispResize (IDisplay Disp, int width,int height,int rowskip,int bpp,
		 int zbuf);
int IDispDestroy (IDisplay Disp);
int IDispFill (IDisplay Disp, guchar *color);
int IDispDrawGdk (IDisplay Disp, GtkWidget *dataview, GdkRgbDither dith);
int IDispWritePPM (IDisplay Disp, char *filename);

/*+ A value of
  +latex+{\tt field\_plot\_type}
  +html+ <tt>field_plot_type</tt>
  is attached to each field in a simulation in order to visualize them
  properly.  Types are as follows:
  +*/
typedef enum {
  /*+Scalar field.+*/
  FIELD_SCALAR           = 0x00,
  /*+Ternary composition field with two components (third component is inferred
    from first two).+*/
  FIELD_TERNARY          = 0x10,
  /*+Ternary composition with pseudo-components mapping onto a rectangle
    instead of a triangle.+*/
  FIELD_TERNARY_SQUARE   = 0x18,
  /*+Vector field.+*/
  FIELD_VECTOR           = 0x20,
  /*+Full ds*ds tensor field, e.g. transformation.+*/
  FIELD_TENSOR_FULL      = 0x30,
  /*+Symmetric tensor field (using lines in principal stress directions).+*/
  FIELD_TENSOR_SYMMETRIC = 0x38,
  /*+Shear tensor field, both symmetric and inferring last diagonal from the
    opposite of the sum of the others.+*/
  FIELD_TENSOR_SHEAR     = 0x39
} field_plot_type;

/* Core stuff in illuminator.c */
int IllDrawTet (PetscScalar *coords, PetscScalar *vals, PetscScalar isoquant,
		PetscScalar *color);
int IllDrawHex (PetscScalar *coords, PetscScalar *vals, PetscScalar isoquant,
		PetscScalar *color);
int IllDraw3DBlock
(int xd, int yd, int zd, int xs, int ys, int zs, int xm, int ym, int zm,
 PetscScalar *minmax, PetscScalar *vals, int skip,
 int n_quants, PetscScalar *isoquants, PetscScalar *colors);

/* Utility stuff in utility.c */
int auto_scale
(PetscScalar *global_array, int points, int num_fields, int display_field,
 field_plot_type fieldtype, int dimensions, PetscScalar *scale);
int minmax_scale
(PetscScalar *global_array, int points, int num_fields, int display_field,
 field_plot_type fieldtype, int dimensions, PetscScalar *minmax);
void field_indices (int nfields, int ds, field_plot_type *plottypes,
		    int *indices);

/* PETSc stuff; xcut, ycut and zcut should almost always be PETSC_FALSE */
int DATriangulateRange
(ISurface Surf, DA theda, Vec globalX, int this, PetscScalar *minmax,
 int n_quants, PetscScalar *isoquants, PetscScalar *colors, int xmin,int xmax,
 int ymin,int ymax, int zmin,int zmax);
int DATriangulateLocalRange
(ISurface Surf, DA theda, Vec localX, int this, PetscScalar *minmax,
 int n_quants, PetscScalar *isoquants, PetscScalar *colors, int xmin,int xmax,
 int ymin,int ymax, int zmin,int zmax);
static inline int DATriangulate
(ISurface Surf, DA theda, Vec globalX, int this, PetscScalar *minmax,
 int n_quants, PetscScalar *isoquants, PetscScalar *colors, PetscTruth xcut,
 PetscTruth ycut, PetscTruth zcut)
{ return DATriangulateRange
    (Surf, theda, globalX, this, minmax, n_quants, isoquants, colors,
     0,xcut?-2:-1, 0,ycut?-2:-1, 0,zcut?-2:-1); }
static inline int DATriangulateLocal
(ISurface Surf, DA theda, Vec localX, int this, PetscScalar *minmax,
 int n_quants, PetscScalar *isoquants, PetscScalar *colors, PetscTruth xcut,
 PetscTruth ycut, PetscTruth zcut)
{ return DATriangulateLocalRange
    (Surf, theda, localX, this, minmax, n_quants, isoquants, colors,
     0,xcut?-2:-1, 0,ycut?-2:-1, 0,zcut?-2:-1); }
/* Note: IllErrorHandler is deprecated, use PETSc SETERRQ instead. */
int IllErrorHandler (int id, char *message);

/* Plotting functions to render data into an RGB buffer, transform #defines */
int render_scale_2d (IDisplay Disp, field_plot_type fieldtype, int symmetry);
int render_composition_path
(IDisplay Disp, PetscScalar *comp_array, int gridpoints, int num_fields,
 field_plot_type fieldtype, PetscScalar *scale,
 PetscScalar red,PetscScalar green,PetscScalar blue,PetscScalar alpha);
int render_rgb_local_2d
(IDisplay Disp, PetscScalar *global_array, int num_fields, int display_field,
 field_plot_type fieldtype, PetscScalar *scale, int nx,int ny, int xs,int ys,
 int xm,int ym, int transform, IDisplay SDisp, PetscScalar dpred,
 PetscScalar dpgreen, PetscScalar dpblue, PetscScalar dpalpha);
int render_rgb_local_3d
(IDisplay Disp, ISurface Surf, PetscScalar *eye, PetscScalar *dir,
 PetscScalar *right);
#define FLIP_HORIZONTAL 0x01
#define FLIP_VERTICAL   0x02
#define ROTATE_LEFT     0x04

/* IlluMulti load/save stuff, including compression #defines */
int IlluMultiLoad
(MPI_Comm comm, char *basename, DA *theda, PetscScalar *wx,PetscScalar *wy,
 PetscScalar *wz, field_plot_type **fieldtypes, int *usermetacount,
 char ***usermetanames, char ***usermetadata);
int IlluMultiRead
(MPI_Comm comm, DA theda, Vec X, char *basename, int *usermetacount,
 char ***usermetanames, char ***usermetadata);
int IlluMultiSave
(MPI_Comm comm, DA theda, Vec X, char *basename, PetscScalar wx,PetscScalar wy,
 PetscScalar wz, field_plot_type *fieldtypes, int usermetacount,
 char **usermetanames, char **usermetadata, int compressed);
#define COMPRESS_INT_MASK  0x30
#define COMPRESS_INT_NONE  0x00
#define COMPRESS_INT_LONG  0x10
#define COMPRESS_INT_SHORT 0x20
#define COMPRESS_INT_CHAR  0x30
#define COMPRESS_GZIP_MASK 0x0F
#define COMPRESS_GZIP_NONE 0x00
#define COMPRESS_GZIP_FAST 0x01
#define COMPRESS_GZIP_BEST 0x0A

/* Alternate storage format import/export */
typedef enum { DEFAULT=0, X_UP, X_DOWN, Y_UP, Y_DOWN, Z_UP, Z_DOWN } IllLayerStyle;
typedef enum { PPM=0, TIFF, PNG, JPEG, GIF } IllImageFormat;
int IllImageSave
(MPI_Comm comm, DA theda, Vec X, char *basename,
 int redfield,int greenfield,int bluefield,
 PetscScalar *rgbmin,PetscScalar *rgbmax, int *coordrange,
 IllLayerStyle layer, IllImageFormat format);

/* Geomview stuff */
int GeomviewBegin (MPI_Comm comm, IDisplay *newdisp);
int GeomviewEnd (MPI_Comm comm, IDisplay oldisp);
int GeomviewDisplayTriangulation
(MPI_Comm comm, ISurface Surf, IDisplay Disp, PetscScalar *minmax, char *name,
 PetscTruth transparent);

/* Imlib2 stuff */
#ifdef IMLIB2_EXISTS
#include <X11/Xlib.h>
#include <Imlib2.h>
int imlib2_render_triangles (DATA32 *data, int width, int height,
			     int num_triangles, int *triangle_coords,
			     PetscScalar *triangle_colors, int color_skip,
			     PetscScalar *triangle_shades, int shade_skip);
#endif

#endif /* ILLUMINATOR_H */
