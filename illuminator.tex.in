\documentclass{report}
\usepackage{fullpage,fonts,page,cxref,lmodern}
\usepackage[T1]{fontenc}
\begin{document}
\title{{\tt Illuminator} Distributed Visualization Library}
\author{Adam Powell}
\date{Version @VERSION@ released @RELEASE_DATE@\\Built \today$\ $on a {\tt
    @build_vendor@} running {\tt @build_os@}}
\maketitle

\begin{abstract}
  {\tt Illuminator} is a library whose current primary purpose is to graph {\tt
    PETSc} 3-D distributed arrays' isoquant contour surfaces using {\tt
    Geomview}.  It is not at all ``complete'', rather a first cut.  It
  currently creates the contour surface triangulation in parallel, then sends
  all of the triangles to the first node for display.  Future plans include
  making the display process more efficient on that first node, and later doing
  some kind of distributed rendering for faster visualization without the use
  of {\tt Geomview}.  As of version 0.4, {\tt Illuminator} also does
  distributed storage and retrieval of distributed arrays of any
  dimensionality, allowing local hard drives on Beowulf cluster nodes to act as
  a single giant ``RAID-0'' array with extremely high bandwidth; this can be
  used for fast retrieval and visualization of time series data.
\end{abstract}

\tableofcontents

\chapter{Introduction}

{\tt PETSc} is a great distributed object manager and Newton-Krylov nonlinear
system solver.  {\tt Geomview} is a wonderful 3-D surface visualization tool.
It seemed logical marry the two, and produce isoquant contour surfaces from
{\tt PETSc} distributed objects in parallel, then display them using {\tt
  Geomview}.

This is a first cut attempt at such a project.  Each node generates its own
isoquant contour surface(s), then all of those generated triangles are gathered
to the first node for viewing in {\tt Geomview}.  Right now, it does not do a
very efficient job of this: it sends three vertices for each triangle to the
first node, which sends those three vertices to {\tt Geomview}, in plain text
format.  In the future, it may do a number of things to reduce the number of
triangles involved, reduce the number of vertices generated per triangle, and
more efficiently transmit the vertices to {\tt Geomview}.

Long-term, there are other nice 3-D viewers out there such as Data Explorer
from IBM, some kind of unified front end to them would be nice.  Furthermore,
with parallel array storage, much more powerful parallel visualization concepts
are possible.  Like if we can assign to each point a certain luminosity and
transparency, it's easy to integrate those values along a line through the
locally-stored part of the array to produce a total transparency and luminosity
for that line in the local array, then just combine that with other line
integrals through the other local sections (maybe in e-vas?), and we have
generated a total image in parallel.  However, to do this easily each section
must be convex, or there will likely be regions with points both in front and
behind a given other region; so this will work with {\tt PETSc} distributed
arrays, but not with general partitioned finite element meshes.  But as you can
see, this opens many interesting avenues for exploration...

For now, this humble beginning.  Share and enjoy.

\chapter{Developer's Manual}

{\tt Illuminator} is intended for developers who are familiar with {\tt
  PETSc} and want to visualize distributed arrays, and as such, this manual
assumes knowledge of {\tt PETSc} programming.

For more details, refer to the complete source code reference which begins at
appendix \ref{file_illuminator.h}.

\section{Visualization}

With a bit of {\tt PETSc} programming knowledge, using {\tt Illuminator} is
pretty simple.  Start by invoking {\tt GeomviewBegin()} (appendix
\ref{func_GeomviewBegin_geomview.c}) to open the {\tt Geomview} display and the
pipes to and from {\tt Geomview}.

At an appropriate checkpoint in the program, invoke {\tt DATriangulate()}, or
for a local array {\tt DATriangulateLocal()}, or else their {\tt
  DATriangulate(Local)Range()} variants (appendices
\ref{func_DATriangulateRange_petsc.c} and
\ref{func_DATriangulateLocalRange_petsc.c}), to create the distributed
triangulation on each processor.  When that is complete, the function {\tt
  GeomviewDisplayTriangulation()} (appendix
\ref{func_GeomviewDisplayTriangulation_geomview.c}) gathers the triangulations
to node 0, sends them to {\tt Geomview} and resets the triangle counts to zero.
This cycle of {\tt DATriangulate()} and {\tt GeomviewDisplayTriangulation()}
may be repeated as necessary, subsequent cycles simply replace the existing
triangle set in {\tt Geomview} with a new set of triangles.  In the example
program {\tt chts}, this is done in the function {\tt ch\_ts\_monitor()}
(appendix \ref{func_ch_ts_monitor_chts.c}) whose function pointer is passed to
{\tt PETSc}'s {\tt TS\_Set\_Monitor()} so it displays the contour surfaces at
the end of every timestep calculation.

Finally, when you are done, call {\tt GeomviewEnd()} (appendix
\ref{func_GeomviewEnd_geomview.c}) to exit the {\tt Geomview} process and
close its pipes.

\subsection{Significant bug}

If an isoquant surface happens to exactly intersect one or more of the
vertices, the triangles on the adjacent tetrahedra may be generated with
coordinates ``{\tt nan nan nan}''.  This is a problem, and I'll try to figure
out a solution at some point.  In the meantime, the workaround is to choose a
slightly different isoquant surface value to avoid the intersection (e.g. if
1000 intersects a vertex, then try 999.999).

\section{Distributed storage}

Because 3-D timestep data can be quite voluminous, {\tt Illuminator} comes with
functions for distributed loading and storage of data.  That is, if you have a
Beowulf cluster with a hard disk on each node, why send all of the data to the
head node to store it?  That would be a waste of bandwidth, and would neglect
the available local hard drive storage!

Instead, you can use Illuminator's function {\tt IlluMultiSave()} (appendix
\ref{func_IlluMultiSave_illumulti.c}) to save the local data to a local hard
drive.  This will create two files for each CPU,
{\tt\{basename\}.cpu\#\#\#\#.meta} which contains XML-formatted metadata
describing that CPU's data, and {\tt\{basename\}.cpu\#\#\#\#.data}, which
contains the data stored in the distributed array ({\tt\#\#\#\#} is the CPU
number).  The data can be compressed using {\tt gzip} and/or by converting
floating point values to 32-, 16- or 8-bit integers scaled to the minimum and
maximum values of each field.  The function {\tt IlluMultiLoad()} (appendix
\ref{func_IlluMultiLoad_illumulti.c}) creates a new distributed array based on
the stored data, and {\tt IlluMultiRead()} (appendix
\ref{func_IlluMultiRead_illumulti.c}) reads the stored data into an existing
distributed array and vector, after verifying a match between existing and
stored distributed array parameters.

It is also possible to save a distributed array stored on multiple CPUs, and
load/read it back into a single CPU, {\tt IlluMultiLoad()} and {\tt
  IlluMultiRead()} automatically rearrange the data at load time accordingly.
At some point, it may be possible to load to arbitrary numbers of CPUs, but for
now, only $n\rightarrow n$ and $n\rightarrow 1$ storage/loading are supported.

{\tt IlluMultiSave()} saves in the native byte order of the CPU on which it is
compiled, and {\tt IlluMultiLoad()} and {\tt IlluMultiRead()} will
automatically byte-swap if stored data doesn't match the CPU's endianness.

\chapter{Example Programs Using Illuminator}

Two simple example programs using the {\tt Illuminator} library are provided:
function visualizer {\tt 3dgf}, and transient Cahn-Hilliard {\tt chts} (and its
GNOME/Glade front-end {\tt chui}).

\section{3-D function visualizer {\tt 3dgf}}

The 3-D function visualizer in {\tt 3dgf.c} (appendix \ref{file_3dgf.c})
currently displays Green's function data in three dimensions using the {\tt
  Geomview} frontend.  (Without {\tt Geomview} installed, {\tt 3dgf} will not
build.)  It includes two modes of operation: default contour surfaces for a 3-D
function $f(x,y,z)=C$ provided in {\tt function\_3d()} (appendix
\ref{func_function_3d_3dgf.c}), if {\tt -twodee} is specified on the command
line then height vs. 2-D space for a 2-D function $z=f(x,y)$ provided in {\tt
  function\_2d()} (appendix \ref{func_function_2d_3dgf.c}).  Note there is no
default box size, one must always use the {\tt -da\_grid\_x}, {\tt\_y} and
{\tt\_z} options to define this.

\section{Transient Cahn-Hilliard {\tt chts}}

This example program provided with {\tt Illuminator} solves the Cahn-Hilliard
equation in 3-D.  The implementation is split into files {\tt cahnhill.c}
(appendix \ref{file_cahnhill.c}) containing all of the free energy functions,
derivatives, etc.; and {\tt chts.c} (appendix \ref{file_chts.c}) with {\tt
  main()}, initialization, callbacks---essentially, the overhead of the
program; and a small header with the data structure in {\tt cahnhill.h}
(appendix \ref{file_cahnhill.h}).  The calls to {\tt Illuminator} library
functions are contained in {\tt chts.c}.

The idea behind Cahn-Hilliard is that we have a concentration field $C$, with
associated thermodynamic free energy density $f$ given by
\begin{equation}
  \label{eq:freedens}
  f = \frac{\alpha}{2}\left|\nabla C\right|^2 + \beta\Psi(C),
\end{equation}
where the two terms are the gradient penalty and homogeneous free energy
$\Psi$, and $\alpha$ and $\beta$ are model constants.  This functional form was
first used to explain the finite domain size during the initial phase
separation in spinodal decomposition.  A simple polynomial homogeneous free
energy function (the one used in {\tt cahnhill.c}) looks like
\begin{equation}
  \label{eq:psi}
  \Psi = C^2 (1-C)^2.
\end{equation}
This functional form gives two stable equilibria (minima) at $C=0$ and $C=1$,
and an unstable equilibrium (maximum) at $C=\frac{1}{2}$.

The total free energy of the system occupying the body $\Omega$ is
\begin{equation}
  \label{eq:freetot}
  {\cal F} = \int_\Omega f dV.
\end{equation}
The ``chemical'' potential $\mu$ is given by
\begin{equation}
  \label{eq:mu}
  \mu = \frac{\delta{\cal F}}{\delta C} = -\alpha \nabla^2 C + \beta\Psi'(C),
\end{equation}
which leads to the transport equation
\begin{equation}
  \label{eq:transport}
  \frac{\partial C}{\partial t} = \nabla\cdot\left(\kappa\nabla\mu\right)
\end{equation}
where $\kappa$ is the mobility.

The interface thickness $\epsilon$ is on the order of $\sqrt{\alpha/\beta}$,
and the surface energy $\gamma$ on the order of $\sqrt{\alpha\beta}$, so we can
set $\alpha=\gamma\epsilon$ and $\beta=\gamma/\epsilon$, with constants which
we'll worry about later.

Returning to equation \ref{eq:transport}, it expands to
\begin{equation}
  \label{eq:transex1}
  \frac{\partial C}{\partial t} = \nabla\kappa\cdot\nabla\mu +
  \kappa\nabla^2\mu
\end{equation}
and further in terms of $C$ to
\begin{equation}
  \label{eq:transex2}
  \frac{\partial C}{\partial t} =
  \nabla\kappa\cdot\nabla(-\alpha\nabla^2C + \beta\Psi')
  - \kappa\nabla^2(\alpha\nabla^2C+\beta\nabla^2\Psi'(C))
\end{equation}

Now we turn to the {\tt PETSc} implementation, using distributed arrays and
{\tt TS} timestepping solvers.  The nonlinear timestepping code uses
runtime-selectable timestepping solvers (explicit, implicit, or default
Crank-Nicholson) to solve the equation
\begin{equation}
  \label{eq:timestep}
  \frac{\partial u_i}{\partial t} = F_i(u_j),
\end{equation}
where $u_i$ is the vector of unknowns (here concentrations).  This is converted
by {\tt PETSc} into a system of nonlinear equations according to the solver
type and then solved using its {\tt SNES} solvers, but we must provide
subroutines to calculate $F_i$ and the derivatives $\frac{\partial
  F_i}{\partial u_j}$ which are used to calculate the Jacobian of the nonlinear
system (unless running matrix-free using the {\tt -snes\_mf} option).  Starting
for now with constant $\kappa$, $\alpha$ and $\beta$, the $F_i$ are given by
\begin{equation}
  \label{eq:timefunc}
  F_i = \kappa\left(-\alpha\nabla^2\nabla^2C + \beta\nabla^2\Psi'(C)\right)
\end{equation}
Let's suppose $C$ is on a 2-D finite difference mesh with uniform but possibly
different spacings $h_x$ and $h_y$ in the $x$- and $y$-directions, so we'll let
$C_{x,y}$ be the value of $C$ at coordinates $(xh_x, yh_y)$ for integer $x$ and
$y$.

Starting with the $\beta$ term, the Laplacian $\nabla^2\Psi'(C)$ at $(x,y)$ can
be approximated using the standard 5-node finite difference stencil:
\begin{equation}
  \label{eq:laplacian}
  \nabla^2\Psi'(C) \simeq
  \frac{\Psi'(C_{x-1,y})-2\Psi'(C_{x,y})+\Psi'(C_{x+1,y})}{h_x^2} +
  \frac{\Psi'(C_{x,y-1})-2\Psi'(C_{x,y})+\Psi'(C_{x,y+1})}{h_y^2}
\end{equation}
or expressed slightly differently, as the sum of the terms:
\begin{equation}
  \label{eq:lapview}
  \begin{array}[h]{ccc}
                            & \Psi'(C_{x,y+1})h_y^{-2}              & \\
    \Psi'(C_{x-1,y})h_x^{-2} & \Psi'(C_{x,y})(-2h_x^{-2} -2h_y^{-2}) &
      \Psi'(C_{x+1,y})h_x^{-2} \\
                            & \Psi'(C_{x,y-1})h_y^{-2}              &
  \end{array}
\end{equation}
So the product of $\kappa\beta$ and this is one part of the function $F_i$, and
that part of the derivative $\frac{\partial F_i}{\partial C_j}$ for the
Jacobian is simply the appropriate coefficient from equation \ref{eq:lapview}
times the second derivative $\Psi''(C)$ evaluated at the appropriate point.

For the $\alpha$-term, the Laplacian of the Laplacian (also known as the {\em
  biharmonic operator}) is a bit more messy.  Using the notation in equation
\ref{eq:lapview} but only in the first quadrant (the coefficients are
symmetric), the term will be $-\kappa\alpha$ times:
\begin{equation}
  \label{eq:laplapview}
  \begin{array}[h]{ccccc}
    & & C_{x,y+2}h_y^{-4} & & \\
    & ... & C_{x,y+1}(-4h_y^{-4}-4h_x^{-2}h_y^{-2}) & 2C_{x+1,y+1}h_x^{-2}h_y^{-2} & \\
    ... & ... & C_{x,y}(6h_x^{-4}+6h_y^{-4}+8h_x^{-2}h_y^{-2}) &
      C_{x+1,y}(-4h_x^{-4}-4h_x^{-2}h_y^{-2}) & C_{x+2,y}h_x^{-4} \\
    & ... & ... & ... & \\
    & & ... & &
  \end{array}
\end{equation}
(ellipsis indicates symmetry).  This is quite a bit more complicated, but at
least it's linear so the derivatives $\frac{\partial F_i}{\partial C_j}$ are
constant.

These $F_i$ functions are calculated by the function {\tt ch\_residual\_2d()}
(appendix \ref{func_ch_residual_2d_cahnhill.c}) and assembled into a {\tt
  PETSc} vector in {\tt ch\_residual\_vector\_2d()} (appendix
\ref{func_ch_residual_vector_2d_cahnhill.c}).  The derivatives are calculated
in two parts: the $\alpha$ term's derivative matrix is built at the start of
the run in {\tt ch\_jacobian\_alpha\_2d()} (appendix
\ref{func_ch_jacobian_alpha_2d_cahnhill.c}), and with each nonlinear iteration,
the $\beta$ term's derivative matrix is added to that in {\tt
  ch\_jacobian\_2d()} (appendix \ref{func_ch_jacobian_2d_cahnhill.c}).

Note that this is all in 2-D; the 3-D version is left as an exercise to the
reader, though it's already coded in the corresponding {\tt \_3d} functions in
{\tt cahnhill.c}.

\subsection{Cahn-Hilliard user interface {\tt chui}}

The Cahn-Hilliard User Interface, or CHUI, is a simple prototype Glade
front-end to {\tt chts} in a single C program file {\tt chui.c} (appendix
\ref{file_chui.c}) and Glade interface description file {\tt chui.glade}.  It
is included here as a demonstration/example rich front end to {\tt PETSc}
programs with simultaneous {\tt Illuminator} visualization.  As such, it
includes a number of standard options such as size of the simulation and
properties, controls the simultaneous visualization options, and also permits
remote running, optionally in multiple processes on a multi-processor machine
or cluster.  Finally, it includes multiple simultaneous progress bars to track
progress of the linear and nonlinear solvers and timesteps.

\chapter{Frequently Asked Questions}

\section{General install problems}

\noindent {\bf Q: ``{\tt make dist}'' doesn't work.  What's up with that?}

\vspace{\baselineskip}
\noindent A: That's because automake 1.4 doesn't like foreign variables from
included makefiles, like {\tt PETSC\_DM\_LIB} used here.  For this reason,
illuminator through 0.3.0 had targets mydistdir, mydist and mydistcheck in
Makefile.am, use those instead of distdir, dist and distcheck.  For 0.3.1 and
beyond, you must use automake 1.5 or higher.

\chapter{Appendix}
\appendix
\section{Version History}

A brief summary of information in the {\tt ChangeLog} file.

\subsection{{\tt PETScGraphics} 0.1}

The first release, this included basic functionality of making and displaying
multiple transparent contour surfaces in 3-D, but only in uniprocessor
operation.

\subsection{{\tt PETScGraphics} 0.2}

This release included the first truly parallel visualization, including
gathering of the triangles to node 1 for viewing.  It also included lots of
code cleanup, changes from the {\tt PETSc} coding style to something more like
GNU format.  And it added the {\tt -random} flag to the chts test program for
random initialization.

\subsection{{\tt PETScGraphics} 0.2.1}

This was largely a documentation update to the new 0.2 interface, reflecting
changes to the new version.

\subsection{{\tt PETScGraphics} 0.2.2}

More documentation updates, esp. for the example program; minor changes to
support gcc on Alpha processors; chts -twodee now works.

\subsection{{\tt Illuminator} 0.2.3}

Name change, as PETSc may be one of several backends in the future.  Shuffled
some functions around to petsc.c and geomview.c.

\subsection{{\tt Illuminator} 0.2.4}

Updated to work with PETSc 2.1.1.

\subsection{{\tt Illuminator} 0.2.5}

Minor fixes: loops in DATriangulate now work for non-periodic distributed
arrays; changed VecMin() and VecMax() to VecStrideMin() and VecStrideMax() to
properly obtain the minimum and maximum value of the target field.

\subsection{{\tt Illuminator} 0.3.0}

A major new addition is the {\tt chui} (Cahn-Hilliard User Interface) program,
this is not quite complete but illustrates what can be done with a bit of
{\tt libglade}.  The goal is eventually to use this kind of thing and {\tt
  Illuminator} to provide an interactive distributed simulation.

\subsection{{\tt Illuminator} 0.3.1}

A bugfix release, among other things this gets chui.glade into the distribution
(which sort of helps), and requires automake 1.5 or above.

\subsection{{\tt Illuminator} 0.3.2}

Another bugfix release, changed Makefile.am to remove static libs/objects from
shared libluminate to fix building on PIC-sensitive platforms.

\subsection{{\tt Illuminator} 0.4.0}

Major addition: the {\tt IlluMulti} system for extreme performance distributed
storage of PETSc distributed arrays on local disks of Beowulf cluster nodes.
This is intended to enable rapid (real-time?) distributed creation of
visualization movies from distributed timestep data stored on the local drives,
essentially turning those hard drives into a giant RAID-0 array.  For details,
see the source-embedded documentation in appendix \ref{file_illumulti.c}.

\subsection{{\tt Illuminator} 0.4.1}

Primarily a bugfix release (since 0.4.0 didn't compile out of the box), this
version also adds the {\tt 3dgf} program, designed to make it very easy to
visualize the isoquant surfaces of a function in 3-D.  As the name implies, it
has a couple of simple Green's functions in it now, and the distributed nature
of the function computation lends it to efficient parallel computation and
visualization of more complex Green's functions.  At some point this should
have a {\tt chui}-like {\tt libglade}-based interface to make interactive
modification of isoquants, colors, limits, resolution, function parameters and
such a lot easier.

\subsection{{\tt Illuminator} 0.4.2}

One bugfix: {\tt illuminator.m4} now installs.  Added {\tt debian} directory to
make future upgrades package more easily.

\subsection{{\tt Illuminator} 0.4.3}

Subtle bugfixes for {\tt IlluMultiLoad()} and {\tt IlluMultiRead()}, needed for
large $n\rightarrow 1$ redistributions, and one bugfix for {\tt
  GeomviewDisplayTriangulation()}.

\subsection{{\tt Illuminator} 0.4.4}

Subtle bugfix for {\tt IlluMultiParseXML()}; added {\tt tsview} program for
generic viewing of timestep data (just 2-D for now; moved from {\tt
  RheoPlast}).  Also added a {\tt --with-geomview=} configuration option.

\subsection{{\tt Illuminator} 0.5.0}

New {\tt DATriangulateLocal()} function should save time when the local array
is available; transparency option in {\tt GeomviewDisplayTriangulation()};
tsview supports 3-D, with ability to cycle through displayed field and turn
transparency on and off at will.

\subsection{{\tt Illuminator} 0.6.0}

Two new attributes added to IlluMulti file format, now version 0.2: field
visualization types to tag special scalar, vector, and even tensor fields; and
floating-point size of the ``physical'' array in each direction.  Support added
to {\tt tsview} for physical size in 2-D (was already there in 3-D).

\subsection{{\tt Illuminator} 0.6.1}

New field visualization types, small change to 0.2 IlluMulti file format,
little bug fixes.  (This is a ``brown paper bag'' release.)

\subsection{{\tt Illuminator} 0.6.2}

Bug fixes for {\tt tsview} viewer: non-square/cube geometry, ``v'' command to
change fields in 3-D, and field name for 3-D all now work properly.

\subsection{{\tt Illuminator} 0.6.9}

New {\tt tsview} commands: ``s'' (size {\tt PetscViewer} windows in 2-D) and
``p'' (set plot contours in 3-D).  Also new HeVeA \LaTeX{} to HTML translator
option, with updates to much of the documentation build system.  And a preview
of the new tsview called tsview-ng, which is sufficiently premature that it is
not included in the Debian package, but will be a substantial new addition to
0.7.0 (hence 0.6.9 for this version).

\subsection{{\tt Illuminator} 0.7.0}

A private release, with new {\tt DATriangulateLocal} function, new cut options
for {\tt DATriangulate(Local)}, three new tsview commands, readline support in
tsview, and a new {\tt -vector\_max} option to {\tt tsview-ng} to scale the
maximum vector size.

\subsection{{\tt Illuminator} 0.8.0}

Upgrade {\tt chui} and {\tt tsview-ng} to gtk+/GNOME version 2 libraries,
including new {\tt .desktop} entries.  The recommended 2-D viewer is now {\tt
  tsview-ng}.

\subsection{{\tt Illuminator} 0.8.9}

Fix for {\tt tsview-ng} so it can really be recommended.  Moved 2-D rendering
from {\tt tsview-ng} into the library, and added a prototype for 3-D (planned
to work in 0.9).  New variations on {\tt DATriangulate(Local)} create triangles
for just a range of grid points.

\subsection{{\tt Illuminator} 0.9.0}

New imlib2-based 3-D rendering engine, 2-D shear rendering, and and many
enhancements to {\tt tsview-ng} including manual scaling with scale figures and
rudimentary 3-D.

\subsection{{\tt Illuminator} 0.9.1}

2-D ternary includes a ``square'' option for composition mapped onto a
rectangle rather than a standard triangle.

\subsection{{\tt Illuminator} 0.10.0}

New data classes encapsulate triangle data ({\tt ISurface}) and display methods
({\tt IDisplay}) for better abstraction from the application.  Both {\tt
  Geomview} pipe and bitmap images can be written to {\tt .ppm} files, and this
is exposed via the user interfaces of {\tt tsview} and {\tt tsview-ng}.  2-D
rendering has new transforms, and both square and triangle ternary plots can
generate diffusion path images as well in any color; these are displayed in the
{\tt tsview-ng} scale area.  {\tt tsview-ng} can now view the Run log and
support non-timeseries data.

\section{Copying}

{\tt Illuminator} Distributed Visualization Library

Copyright (C) 2001, 2002 Adam Powell;
Copyright (C) 2003 Adam Powell, Jorge Vieyra, Bo Zhou;
Copyright (C) 2004-2006 Adam Powell, Ojonimi Ocholi, Jorge Vieyra, Bo Zhou.

The {\tt libluminate} library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version.

Note that it uses some chunks of code from Ken Brakke's public domain Surface
Evolver, those chunks are attributed in the source code.

The other source code and documentation in this Illuminator package are free
software; you can distribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This software is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU (Lesser) General Public License for more
details.

You should have received a copy of the GNU (Lesser) General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

You may contact the corresponding author by email at {\tt hazelsct@mit.edu}.

% Source code documentation which goes here is automatically generated from
% comments in source files by {\tt cxref}.  If you do not have {\tt cxref}, you
% will not see that documentation here.

% Begin-Of-Source-Files

% End-Of-Source-Files

\end{document}
