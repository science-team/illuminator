/***************************************
  $Header$

  This is the illuminator.c main file.  It has all of the routines which
  compute the triangulation in a distributed way.
***************************************/


#include "config.h"      /* esp. for inline */
#include "illuminator.h" /* Just to make sure the interface is "right" */
#include "structures.h"     /* For the isurface structure definition */
#include <stdlib.h>      /* For malloc() */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) PetscPrintf (PETSC_COMM_WORLD, "%s: " fmt, __FUNCT__, args)
#else
#define DPRINTF(fmt, args...)
#endif

#define MAX_TRIANGLES 10000000 /*+ Maximum number of generated triangles per
				node. +*/


#undef __FUNCT__
#define __FUNCT__ "ISurfCreate"

/*++++++++++++++++++++++++++++++++++++++
  Constructor for ISurface data object.

  int SurfCreate Returns zero or an error code.

  Surf *newsurf Address in which to put the new ISurface object.
  ++++++++++++++++++++++++++++++++++++++*/

int SurfCreate (ISurface *newsurf)
{
  struct isurface *thesurf;

  if (!(thesurf = (struct isurface *) malloc (sizeof (struct isurface))))
    SETERRQ (PETSC_ERR_MEM, "Unable to allocate memory for isurface object");

  thesurf->vertices = NULL;
  thesurf->vertisize = thesurf->num_triangles = 0;
  *newsurf = (ISurface) thesurf;
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "ISurfDestroy"

/*++++++++++++++++++++++++++++++++++++++
  Destructor for ISurface data object.

  int ISurfDestroy Returns zero or an error code.

  ISurface Surf ISurface object to destroy.
  ++++++++++++++++++++++++++++++++++++++*/

int ISurfDestroy (ISurface Surf)
{
  struct isurface *thesurf = (struct isurface *) Surf;

  if (thesurf->vertices)
    free (thesurf->vertices);
  free (thesurf);
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "ISurfClear"

/*++++++++++++++++++++++++++++++++++++++
  Clear out an isurface data object without freeing its memory.

  int SurfClear Returns zero or an error code.

  ISurface Surf ISurface object to clear out.
  ++++++++++++++++++++++++++++++++++++++*/

int SurfClear (ISurface Surf)
{
  struct isurface *thesurf = (struct isurface *) Surf;

  return thesurf->num_triangles = 0;
}


#undef __FUNCT__
#define __FUNCT__ "storetri"

/*++++++++++++++++++++++++++++++++++++++
  This little inline routine just implements triangle storage.

  int storetri Returns 0 or an error code.

  PetscScalar x0 X-coordinate of corner 0.

  PetscScalar y0 Y-coordinate of corner 0.

  PetscScalar z0 Z-coordinate of corner 0.

  PetscScalar x1 X-coordinate of corner 1.

  PetscScalar y1 Y-coordinate of corner 1.

  PetscScalar z1 Z-coordinate of corner 1.

  PetscScalar x2 X-coordinate of corner 2.

  PetscScalar y2 Y-coordinate of corner 2.

  PetscScalar z2 Z-coordinate of corner 2.

  PetscScalar *color R,G,B,A quad for this triangle.

  struct isurface *thesurf ISurface object to put the triangles in.
  ++++++++++++++++++++++++++++++++++++++*/

static inline int storetri
(PetscScalar x0,PetscScalar y0,PetscScalar z0,
 PetscScalar x1,PetscScalar y1,PetscScalar z1,
 PetscScalar x2,PetscScalar y2,PetscScalar z2, PetscScalar *color,
 struct isurface *thesurf) 
{
  if (thesurf->num_triangles >= thesurf->vertisize)
    {
      thesurf->vertices = (PetscScalar *) realloc
	(thesurf->vertices, 13*sizeof(double)*
	 (thesurf->vertisize = (thesurf->vertisize==0) ? 1000 :
	  ((thesurf->vertisize > MAX_TRIANGLES) ?
	   thesurf->vertisize : thesurf->vertisize*2)));
      if (thesurf->num_triangles >= thesurf->vertisize)
	{
	  int rank, ierr;
	  ierr = MPI_Comm_rank (PETSC_COMM_WORLD, &rank); CHKERRQ (ierr);
	  printf ("CPU %d: Number of triangles exceeded %d, emptying array.\n"
		  "(The limit is MAX_TRIANGLES in illuminator.c line 21.)",
		  rank, MAX_TRIANGLES);
	  thesurf->num_triangles = 0;
	}
      else
	DPRINTF ("Expanding vertices array to %d triangles, pointer at 0x%lx\n",
		 thesurf->vertisize, thesurf->vertices);
    }

  thesurf->vertices[13*thesurf->num_triangles+0] = x0;
  thesurf->vertices[13*thesurf->num_triangles+1] = y0;
  thesurf->vertices[13*thesurf->num_triangles+2] = z0;
  thesurf->vertices[13*thesurf->num_triangles+3] = x1;
  thesurf->vertices[13*thesurf->num_triangles+4] = y1;
  thesurf->vertices[13*thesurf->num_triangles+5] = z1;
  thesurf->vertices[13*thesurf->num_triangles+6] = x2;
  thesurf->vertices[13*thesurf->num_triangles+7] = y2;
  thesurf->vertices[13*thesurf->num_triangles+8] = z2;
  thesurf->vertices[13*thesurf->num_triangles+9] = color[0];
  thesurf->vertices[13*thesurf->num_triangles+10] = color[1];
  thesurf->vertices[13*thesurf->num_triangles+11] = color[2];
  thesurf->vertices[13*thesurf->num_triangles+12] = color[3];

  thesurf->num_triangles++;
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "DrawTetWithPlane"

/* Simplifying macro for DrawTetWithPlane */
#define COORD(c1, c2, index) ((index) * (c2) + (1.-(index)) * (c1))

/*++++++++++++++++++++++++++++++++++++++
  This function calculates triangle vertices for an isoquant surface in a
  linear tetrahedron, using the whichplane information supplied by the routine
  calling this one, and "draws" them using storetri().  This is really an
  internal function, not intended to be called by user programs.  It is used by
  DrawTet() and DrawHex().

  int DrawTetWithPlane Returns 0 or an error code.

  PetscScalar x0 X-coordinate of vertex 0.

  PetscScalar y0 Y-coordinate of vertex 0.

  PetscScalar z0 Z-coordinate of vertex 0.

  PetscScalar f0 Function value at vertex 0.

  PetscScalar x1 X-coordinate of vertex 1.

  PetscScalar y1 Y-coordinate of vertex 1.

  PetscScalar z1 Z-coordinate of vertex 1.

  PetscScalar f1 Function value at vertex 1.

  PetscScalar x2 X-coordinate of vertex 2.

  PetscScalar y2 Y-coordinate of vertex 2.

  PetscScalar z2 Z-coordinate of vertex 2.

  PetscScalar f2 Function value at vertex 2.

  PetscScalar x3 X-coordinate of vertex 3.

  PetscScalar y3 Y-coordinate of vertex 3.

  PetscScalar z3 Z-coordinate of vertex 3.

  PetscScalar f3 Function value at vertex 3.

  PetscScalar isoquant Function value at which to draw triangle.

  PetscScalar edge0 Normalized intercept at edge 0, 0. at node 0, 1. at node 1.

  PetscScalar edge1 Normalized intercept at edge 1, 0. at node 1, 1. at node 2.

  PetscScalar edge3 Normalized intercept at edge 3, 0. at node 0, 1. at node 3.

  int whichplane Index of which edge intercept(s) is between zero and 1.

  PetscScalar *color R,G,B,A quad for this tetrahedron.

  struct isurface *thesurf ISurface object to put the triangles in.
  ++++++++++++++++++++++++++++++++++++++*/

static inline int DrawTetWithPlane
(PetscScalar x0,PetscScalar y0,PetscScalar z0,PetscScalar f0,
 PetscScalar x1,PetscScalar y1,PetscScalar z1,PetscScalar f1,
 PetscScalar x2,PetscScalar y2,PetscScalar z2,PetscScalar f2,
 PetscScalar x3,PetscScalar y3,PetscScalar z3,PetscScalar f3,
 PetscScalar isoquant, PetscScalar edge0,PetscScalar edge1,PetscScalar edge3,
 int whichplane, PetscScalar *color, struct isurface *thesurf)
{
  PetscScalar edge2,edge4,edge5;

  switch (whichplane) {
  case 7: { /* Triangles on edges 0,1,3; 3,1,5 */
    edge5 = (isoquant - f2) / (f3 - f2);

    /* Use edge 0 direction to guarantee right-handedness */
    if (f1 > f0) {
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		color, thesurf);
      storetri (COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		color, thesurf);
    }
    else {
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		color, thesurf);
      storetri (COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		color, thesurf);
    }
    break;
  }

  case 6: { /* Triangles on edges 1,2,4; 4,2,3 */
    edge2 = (isoquant - f2) / (f0 - f2);
    edge4 = (isoquant - f1) / (f3 - f1);

    /* Use edge 1 direction to guarantee right-handedness */
    if (f2 > f1) {
      storetri (COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		color, thesurf);
      storetri (COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		color, thesurf);
    }
    else {
      storetri (COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		color, thesurf);
      storetri (COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		color, thesurf);
    }
    break;
  }

  case 5: { /* Triangles on edges 0,2,3 */
    edge2 = (isoquant - f2) / (f0 - f2);

    /* Use edge 0 direction to guarantee right-handedness */
    if (f1 > f0)
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		color, thesurf);
    else
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		color, thesurf);
    break;
  }

  case 4: { /* Triangles on edges 3,4,5 */
    edge4 = (isoquant - f1) / (f3 - f1);
    edge5 = (isoquant - f2) / (f3 - f2);

    /* Use edge 3 direction to guarantee right-handedness */
    if (f3 > f0)
      storetri (COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		color, thesurf);
    else
      storetri (COORD(x0,x3,edge3), COORD(y0,y3,edge3), COORD(z0,z3,edge3),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		color, thesurf);
    break;
  }

  case 3: { /* Triangles on edges 0,1,4 */
    edge4 = (isoquant - f1) / (f3 - f1);

    /* Use edge 0 direction to guarantee right-handedness */
    if (f1 > f0)
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		color, thesurf);
    else
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		color, thesurf);
    break;
  }

  case 2: { /* Triangles on edges 1,2,5 */
    edge2 = (isoquant - f2) / (f0 - f2);
    edge5 = (isoquant - f2) / (f3 - f2);

    /* Use edge 1 direction to guarantee right-handedness */
    if (f2 > f1)
      storetri (COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		color, thesurf);
    else
      storetri (COORD(x1,x2,edge1), COORD(y1,y2,edge1), COORD(z1,z2,edge1),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		color, thesurf);
    break;
  }

  case 1: { /* Triangles on edges 0,2,4; 4,2,5 */
    edge2 = (isoquant - f2) / (f0 - f2);
    edge4 = (isoquant - f1) / (f3 - f1);
    edge5 = (isoquant - f2) / (f3 - f2);

    /* Use edge 0 direction to guarantee right-handedness */
    if (f1 > f0) {
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		color, thesurf);
      storetri (COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		color, thesurf);
    }
    else {
      storetri (COORD(x0,x1,edge0), COORD(y0,y1,edge0), COORD(z0,z1,edge0),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		color, thesurf);
      storetri (COORD(x2,x0,edge2), COORD(y2,y0,edge2), COORD(z2,z0,edge2),
		COORD(x1,x3,edge4), COORD(y1,y3,edge4), COORD(z1,z3,edge4),
		COORD(x2,x3,edge5), COORD(y2,y3,edge5), COORD(z2,z3,edge5),
		color, thesurf);
    }
    break;
  }
  }
  return 0;
}
#undef COORD


#undef __FUNCT__
#define __FUNCT__ "DrawTet"

/*++++++++++++++++++++++++++++++++++++++
  This sets the edge and whichplane parameters and then passes everything to
  DrawTetWithPlane to actually draw the triangle.  It is intended for use by
  developers with distributed arrays based on tetrahedra, e.g. a finite element
  mesh.

  int DrawTet Returns 0 or an error code.

  ISurface Surf ISurface object into which to draw this tetrahedron's isoquant.

  PetscScalar *coords Coordinates of tetrahedron corner points: x0, y0, z0, x1,
  etc.

  PetscScalar *vals Function values at tetrahedron corners: f0, f1, f2, f3.

  PetscScalar isoquant Function value at which to draw triangle.

  PetscScalar *color R,G,B,A quad for this tetrahedron.
  ++++++++++++++++++++++++++++++++++++++*/

int DrawTet
(ISurface Surf, PetscScalar *coords, PetscScalar *vals, PetscScalar isoquant,
 PetscScalar *color)
{
  PetscScalar edge0, edge1, edge3;
  int whichplane, ierr=0;
  struct isurface *thesurf = (struct isurface *) Surf;

  edge0 = (isoquant-vals[0]) / (vals[1]-vals[0]);
  edge1 = (isoquant-vals[1]) / (vals[2]-vals[1]);
  edge3 = (isoquant-vals[0]) / (vals[3]-vals[0]);
  whichplane = (edge0>0. && edge0<1.) | ((edge1>0. && edge1<1.) << 1) |
    ((edge3>0. && edge3<1.) << 2);
  if (whichplane)
    ierr = DrawTetWithPlane (coords[0],coords[1],coords[2],vals[0],
			     coords[3],coords[4],coords[5],vals[1],
			     coords[6],coords[7],coords[8],vals[2],
			     coords[9],coords[10],coords[11],vals[3],
			     isoquant, edge0,edge1,edge3, whichplane, color,
			     thesurf);
  return ierr;
}


#undef __FUNCT__
#define __FUNCT__ "DrawHex"

/*++++++++++++++++++++++++++++++++++++++
  This divides a right regular hexahedron into tetrahedra, and loops over them
  to generate triangles on each one.  It calculates edge and whichplane
  parameters so it can use DrawTetWithPlane directly.

  int DrawHex Returns 0 or an error code.

  ISurface Surf ISurface object into which to draw this hexahedron's isoquant.

  PetscScalar *coords Coordinates of hexahedron corner points: xmin, xmax,
  ymin, etc.

  PetscScalar *vals Function values at hexahedron corners: f0, f1, f2, etc.

  PetscScalar isoquant Function value at which to draw triangles.

  PetscScalar *color R,G,B,A quad for this hexahedron.
  ++++++++++++++++++++++++++++++++++++++*/

int DrawHex
(ISurface Surf, PetscScalar *coords, PetscScalar *vals, PetscScalar isoquant,
 PetscScalar *color)
{
  int tetras[6][4] = {{0,1,2,4}, {1,2,4,5}, {2,4,5,6}, {1,3,2,5}, {2,5,3,6}, {3,6,5,7}};
  int tet,node,ierr;
  int c0,c1,c2,c3,whichplane;
  PetscScalar edge0,edge1,edge3;
  struct isurface *thesurf = (struct isurface *) Surf;

  for(tet=0; tet<6; tet++)
    {
      /* Within a tetrahedron, edges 0 through 5 connect corners:
	 0,1; 1,2; 2,0; 0,3; 1,3; 2,3 respectively */
      c0 = tetras[tet][0];
      c1 = tetras[tet][1];
      c2 = tetras[tet][2];
      c3 = tetras[tet][3];
      edge0 = (isoquant-vals[c0]) / (vals[c1]-vals[c0]);
      edge1 = (isoquant-vals[c1]) / (vals[c2]-vals[c1]);
      edge3 = (isoquant-vals[c0]) / (vals[c3]-vals[c0]);
      whichplane = (edge0>0. && edge0<1.) | ((edge1>0. && edge1<1.) << 1) |
	((edge3>0. && edge3<1.) << 2);
      if (whichplane)
	{
	  ierr=DrawTetWithPlane
	    (coords[c0&1],coords[2+((c0&2)>>1)],coords[4+((c0&4)>>2)],vals[c0],
	     coords[c1&1],coords[2+((c1&2)>>1)],coords[4+((c1&4)>>2)],vals[c1],
	     coords[c2&1],coords[2+((c2&2)>>1)],coords[4+((c2&4)>>2)],vals[c2],
	     coords[c3&1],coords[2+((c3&2)>>1)],coords[4+((c3&4)>>2)],vals[c3],
	     isoquant, edge0,edge1,edge3, whichplane, color, thesurf);
	  CHKERRQ (ierr);
	}
    }

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "Draw3DBlock"

/*++++++++++++++++++++++++++++++++++++++
  Calculate vertices of isoquant triangle(s) in a 3-D regular array of right
  regular hexahedra.  This loops through a 3-D array and calls DrawHex to
  calculate the triangulation of each hexahedral cell.

  int Draw3DBlock Returns 0 or an error code.

  ISurface Surf ISurface object into which to draw this block's isoquants.

  int xd Overall x-width of function value array.

  int yd Overall y-width of function value array.

  int zd Overall z-width of function value array.

  int xs X-index of the start of the array section we'd like to draw.

  int ys Y-index of the start of the array section we'd like to draw.

  int zs Z-index of the start of the array section we'd like to draw.

  int xm X-width of the array section we'd like to draw.

  int ym Y-width of the array section we'd like to draw.

  int zm Z-width of the array section we'd like to draw.

  PetscScalar *minmax Position of block corners: xmin, xmax, ymin, ymax, zmin,
  zmax.

  PetscScalar *vals The array of function values at vertices.

  int skip Number of interlaced fields in this array.

  int n_quants Number of isoquant surfaces to draw (isoquant values).

  PetscScalar *isoquants Array of function values at which to draw triangles.

  PetscScalar *colors Array of color R,G,B,A quads for each isoquant.
  ++++++++++++++++++++++++++++++++++++++*/

int Draw3DBlock
(ISurface Surf,int xd,int yd,int zd, int xs,int ys,int zs,int xm,int ym,int zm,
 PetscScalar *minmax, PetscScalar *vals, int skip, int n_quants,
 PetscScalar *isoquants, PetscScalar *colors)
{
  int i,j,k,q, start, ierr;
  PetscScalar boxcoords[6], funcs[8];

  /* Simple check */
  if (xd<=xs+xm || yd<=ys+ym || zd<=zs+zm)
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE, "Array size mismatch");

  /* The big loop */
  for (k=0; k<zm; k++)
    {
      boxcoords[4] = minmax[4] + (minmax[5]-minmax[4])*k/zm;
      boxcoords[5] = minmax[4] + (minmax[5]-minmax[4])*(k+1)/zm;
      for(j=0;j<ym;j++)
	{
	  boxcoords[2] = minmax[2] + (minmax[3]-minmax[2])*j/ym;
	  boxcoords[3] = minmax[2] + (minmax[3]-minmax[2])*(j+1)/ym;
	  for(i=0;i<xm;i++)
	    {
	      boxcoords[0] = minmax[0] + (minmax[1]-minmax[0])*i/xm;
	      boxcoords[1] = minmax[0] + (minmax[1]-minmax[0])*(i+1)/xm;
	      start = ((k+zs)*yd + j+ys)*xd + xs + i;
	      funcs[0] = vals[skip*start];
	      funcs[1] = vals[skip*(start+1)];
	      funcs[2] = vals[skip*(start+xd)];
	      funcs[3] = vals[skip*(start+xd+1)];
	      funcs[4] = vals[skip*(start+xd*yd)];
	      funcs[5] = vals[skip*(start+xd*yd+1)];
	      funcs[6] = vals[skip*(start+xd*yd+xd)];
	      funcs[7] = vals[skip*(start+xd*yd+xd+1)];
	      for (q=0; q<n_quants; q++)
		{
		  ierr = DrawHex (Surf, boxcoords, funcs, isoquants[q],
				  colors+4*q); CHKERRQ (ierr);
		}
	    }
	}
    }

  return 0;
}
