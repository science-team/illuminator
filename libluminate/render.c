/***************************************
  $Header$

  This file contains the rendering code for Illuminator, which renders 2-D or
  3-D data into an RGB(A) unsigned char array (using perspective in 3-D).
***************************************/

#include "illuminator.h"
#include "structures.h"

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) PetscPrintf (PETSC_COMM_WORLD, "%s: " fmt, __FUNCT__, args)
#else
#define DPRINTF(fmt, args...)
#endif


#undef __FUNCT__
#define __FUNCT__ "pseudocolor"

/*++++++++++++++++++++++++++++++++++++++
  This little function converts a scalar value into an rgb color from red to
  blue.

  PetscScalar val Value to convert.

  PetscScalar* scale Array with minimum and maximum values in which to scale
  val.

  guchar *pixel Address in rgb buffer where this pixel should be painted.
  ++++++++++++++++++++++++++++++++++++++*/

static inline void pseudocolor (PetscScalar val, PetscScalar* scale,
				guchar *pixel)
{
  PetscScalar shade = (val - scale[0]) / (scale[1] - scale[0]);
  /* Old stuff *
  if (shade < 0.2) {      /* red -> yellow *
    *pixel++ = 255;
    *pixel++ = 1275*shade;
    *pixel++ = 0; }
  else if (shade < 0.4) { /* yellow -> green *
    *pixel++ = 510-1275*shade;
    *pixel++ = 255;
    *pixel++ = 0; }
  else if (shade < 0.6) { /* green -> cyan *
    *pixel++ = 0;
    *pixel++ = 255;
    *pixel++ = 1275*shade-510; }
  else if (shade < 0.8) { /* cyan -> blue *
    *pixel++ = 0;
    *pixel++ = 1020-1275*shade;
    *pixel++ = 255; }
  else {                  /* blue -> magenta *
    *pixel++ = 1275*shade-1020;
    *pixel++ = 0;
    *pixel++ = 255; }
  /* New stuff */
  if (shade < 0.25) {      /* red -> yellow */
    *pixel++ = 255;
    *pixel++ = 1020*shade;
    *pixel++ = 0; }
  else if (shade < 0.5) { /* yellow -> green */
    *pixel++ = 510-1020*shade;
    *pixel++ = 255;
    *pixel++ = 0; }
  else if (shade < 0.75) { /* green -> cyan */
    *pixel++ = 0;
    *pixel++ = 255;
    *pixel++ = 1020*shade-510; }
  else { /* cyan -> blue */
    *pixel++ = 0;
    *pixel++ = 1020-1020*shade;
    *pixel++ = 255; }
}


#undef __FUNCT__
#define __FUNCT__ "pseudoternarycolor"

/*++++++++++++++++++++++++++++++++++++++
  This little function converts two ternary fractions into an rgb color, with
  yellow, cyan and magenta indicating the corners.

  PetscScalar A First ternary fraction.

  PetscScalar B Second ternary fraction.

  PetscScalar *scale Array first and second ternary fractions of each of the
  three corner values for scaling.

  guchar *pixel  Address in rgb buffer where this pixel should be painted.
  ++++++++++++++++++++++++++++++++++++++*/

static inline void pseudoternarycolor
(PetscScalar A, PetscScalar B, PetscScalar *scale, guchar *pixel)
{
  PetscScalar x1, x2, inverse_det;

  /* Transform A,B into x1,x2 based on corners */
  inverse_det = 1./((scale[2]-scale[0])*(scale[5]-scale[1]) -
		    (scale[4]-scale[0])*(scale[3]-scale[1]));
  x1 = ((A-scale[0])*(scale[5]-scale[1]) -
	(B-scale[1])*(scale[4]-scale[0])) * inverse_det;
  x2 = ((B-scale[1])*(scale[2]-scale[0]) -
	(A-scale[0])*(scale[3]-scale[1])) * inverse_det;

  /* Now colorize */
  /* Simple R-G-B */
  /* *pixel++ = 255*x1;
  *pixel++ = 255*(1.-x1-x2);
  *pixel++ = 255*sqrt(x2); */
  /* *pixel++ = 255*(1.-x1);
  *pixel++ = 255*(x1+x2);
  *pixel++ = 255*(1.-x2); */
  /* Fancy three-square colorization */
  if (2*x1+x2<=1 && 2*x2+x1<=1)
    {
      *pixel++ = 245;
      *pixel++ = 245*(2*x1+3*x1*x2);
      *pixel = 245*(2*x2+3*x1*x2);
    }
  else if (x2>=x1)
    {
      *pixel++ = 245*(2*(1-x1-x2)+3*(1-x1-x2)*x1);
      *pixel++ = 245*(2*x1+3*(1-x1-x2)*x1);
      *pixel = 245;
    }
  else
    {
      *pixel++ = 245*(2.-2*x1-2*x2+3*(1-x1-x2)*x2);
      *pixel++ = 245;
      *pixel = 245*(2*x2+3*(1-x1-x2)*x2);
    }
  /* Even fancier three-square colorization */
  /*if (2*x1+x2<=1 && 4*x2+x1<=1) // 0,0 corner
    {
      *pixel++ = 254;
      *pixel++ = 254*(2*x1+7./6.*x1*x2);
      *pixel = 254*(4*x2+7./4.*x1*x2);
    }
  else if (3*x2>=x1) // 0,1 corner
    {
      *pixel++ = 254*(4./3.*(1-x1-x2)+7./3.*(1-x1-x2)*x1);
      *pixel++ = 254*(4./3.*x1+7./3.*(1-x1-x2)*x1);
      *pixel = 254;
    }
  else // 1,0 corner
    {
      *pixel++ = 254*(2*(1-x1-x2)+7./3.*(1-x1-x2)*x2);
      *pixel++ = 254;
      *pixel = 254*(4*x2+7./3.*(1-x1-x2)*x2);
    }*/
}


#undef __FUNCT__
#define __FUNCT__ "pseudoternarysquarecolor"

/*++++++++++++++++++++++++++++++++++++++
  This little function converts two composition values into an rgb color, with
  blue, red, green and yellow indicating the corners.

  PetscScalar A First ternary composition.

  PetscScalar B Second ternary composition.

  PetscScalar *scale Array: minimum and maximum first and second compositions
  for scaling.

  guchar *pixel  Address in rgb buffer where this pixel should be painted.
  ++++++++++++++++++++++++++++++++++++++*/

static inline void pseudoternarysquarecolor
(PetscScalar A, PetscScalar B, PetscScalar *scale, guchar *pixel)
{
  PetscScalar x1, x2, inverse_det;

  /* Transform A,B into x1,x2 based on corners */
  x1 = (A-scale[0])/(scale[1]-scale[0]);
  x2 = (B-scale[2])/(scale[3]-scale[2]);

  /* Now colorize */
  *pixel++ = 255*x2;
  *pixel++ = 255*x1;
  *pixel++ = 255*(1.-x1)*(1.-x2);
}


#undef __FUNCT__
#define __FUNCT__ "pseudohueintcolor"

/*++++++++++++++++++++++++++++++++++++++
  This little function converts a vector into an rgb color with hue indicating
  direction (green, yellow, red, blue at 0, 90, 180, 270 degrees) and intensity
  indicating magnitude relative to reference magnitude in scale[1].

  PetscScalar vx Vector's
  +latex+$x$-component.
  +html+ <i>x</i>-component.

  PetscScalar vy Vector's
  +latex+$y$-component.
  +html+ <i>y</i>-component.

  PetscScalar *scale Array whose second entry has the reference magnitude.

  guchar *pixel Address in rgb buffer where this pixel should be painted.
  ++++++++++++++++++++++++++++++++++++++*/

static inline void pseudohueintcolor
(PetscScalar vx, PetscScalar vy, PetscScalar *scale, guchar *pixel)
{
  PetscScalar mag=sqrt(vx*vx+vy*vy), theta=atan2(vy,vx), red, green, blue;
  if (scale[1] <= 0.)
    {
      *pixel = *(pixel+1) = *(pixel+2) = 0;
      return;
    }
  mag = (mag > scale[1]) ? 1.0 : mag/scale[1];

  red = 2./M_PI * ((theta<-M_PI/2.) ? -M_PI/2.-theta :
		   ((theta<0.) ? 0. : ((theta<M_PI/2.) ? theta : M_PI/2.)));
  green = 2./M_PI * ((theta<-M_PI/2.) ? 0. :
		     ((theta<0.) ? theta+M_PI/2. :
		      ((theta<M_PI/2.) ? M_PI/2. : M_PI-theta)));
  blue = 2./M_PI * ((theta<-M_PI/2.) ? theta+M_PI :
		    ((theta<0.) ? -theta : 0.));

  *pixel++ = 255*mag*red;
  *pixel++ = 255*mag*green;
  *pixel++ = 255*mag*blue;
}


#undef __FUNCT__
#define __FUNCT__ "pseudoshearcolor"

/*++++++++++++++++++++++++++++++++++++++
  This little function converts a shear tensor (symmetric, diagonals sum to
  zero) into an rgb color with hue indicating direction of the tensile stress
  (red, yellow, green, cyan, blue, magenta at 0, 30, 60, 90, 120, 150 degrees
  respectively; 180 is equivalent to zero for stress) and intensity
  indicating magnitude relative to reference magnitude in scale[0].

  PetscScalar gxx Tensor's
  +latex+$xx$-component.
  +html+ <i>xx</i>-component.

  PetscScalar gxy Tensor's
  +latex+$xy$-component.
  +html+ <i>xy</i>-component.

  PetscScalar *scale Array whose first entry has the reference magnitude.

  guchar *pixel Address in rgb buffer where this pixel should be painted.
  ++++++++++++++++++++++++++++++++++++++*/

static inline void pseudoshearcolor
(PetscScalar gxx, PetscScalar gxy, PetscScalar *scale, guchar *pixel)
{
  PetscScalar mag=sqrt(gxx*gxx+gxy*gxy), theta=atan2(gxy,gxx),
    red,green,blue;
  if (scale[0] <= 0.)
    {
      *pixel = *(pixel+1) = *(pixel+2) = 0;
      return;
    }
  mag = (mag > scale[0]) ? 1.0 : mag/scale[0];

  red = 3./M_PI * ((theta < -2.*M_PI/3.) ? 0. :
		   ((theta < -M_PI/3.) ? theta + 2.*M_PI/3. :
		    ((theta < M_PI/3.) ? M_PI/3. :
		     ((theta < 2.*M_PI/3.) ? 2.*M_PI/3. - theta: 0.))));
  green = 3./M_PI * ((theta < -M_PI/3.) ? M_PI/3. :
		     ((theta < 0.) ? -theta :
		      ((theta < 2.*M_PI/3.) ? 0. : theta - 2.*M_PI/3.)));
  blue = 3./M_PI * ((theta < -2.*M_PI/3) ? -2.*M_PI/3. - theta :
		    ((theta < 0.) ? 0. :
		     ((theta < M_PI/3.) ? theta : M_PI/3.)));

  *pixel++ = 255*mag*red;
  *pixel++ = 255*mag*green;
  *pixel++ = 255*mag*blue;
}


#undef __FUNCT__
#define __FUNCT__ "render_scale_2d"

/*++++++++++++++++++++++++++++++++++++++
  This draws a little rwidth x rheight image depicting the scale of a field
  variable.

  int render_scale_2d It returns zero or an error code.

  IDisplay Disp Display object into which to draw the scale.

  field_plot_type fieldtype Type of plot.

  int symmetry Symmetry order for vector scale image.
  ++++++++++++++++++++++++++++++++++++++*/

int render_scale_2d (IDisplay Disp, field_plot_type fieldtype, int symmetry)
{
  PetscScalar scale[6];
  int i, j, myheight;
  struct idisplay *thedisp = (struct idisplay *) Disp;

  switch (fieldtype)
    {
    case FIELD_SCALAR:
      scale[0]=0.;
      scale[1]=1.;
      for (j=0; j<thedisp->rgb_height; j++)
	for (i=0; i<thedisp->rgb_width; i++)
	  pseudocolor ((PetscScalar) i/(thedisp->rgb_width-1), scale,
		       thedisp->rgb+(j*thedisp->rgb_rowskip + i) *
		       thedisp->rgb_bpp);
      return 0;

    case FIELD_VECTOR:
      scale[0] = 0.;
      scale[1] = 1.;
      myheight = ((thedisp->rgb_height > thedisp->rgb_width) ?
		  thedisp->rgb_width : thedisp->rgb_height)/2;
      for (j=-myheight; j<myheight; j++)
	for (i=-(int)(myheight*sqrt(1.-((PetscScalar)j*j/myheight/myheight)));
	     i<(int)(myheight*sqrt(1.-((PetscScalar)j*j/myheight/myheight)));
	     i++)
	  {
	    PetscScalar mag = (PetscScalar) sqrt (i*i+j*j)/myheight,
	      theta = atan2 (j, i);
	    pseudohueintcolor
	      (mag * cos (symmetry*theta), mag * sin (symmetry*theta), scale,
	       thedisp->rgb + ((myheight-j)*thedisp->rgb_rowskip +
			       thedisp->rgb_width/2+i)*thedisp->rgb_bpp);
	  }
      return 0;

    case FIELD_TERNARY:
      scale[0] = scale[1] = scale[3] = scale[4] = 0.;
      scale[2] = scale[5] = 1.;
      myheight = ((PetscScalar) thedisp->rgb_height >
		  0.5*sqrt(3.) * thedisp->rgb_width) ?
	(int) (0.5*sqrt(3.)* thedisp->rgb_width) : thedisp->rgb_height;
      for (j=0; j<myheight; j++)
	for (i=0; i<(int) (2./sqrt(3.)*(myheight-j)); i++)
	  pseudoternarycolor
	    ((PetscScalar) i/(myheight*2./sqrt(3.)),
	     (PetscScalar) j/myheight, scale, thedisp->rgb +
	     (((thedisp->rgb_height+myheight)/2 -j) * thedisp->rgb_rowskip +
	      i + (int)(sqrt(3.)/3.*j)) * thedisp->rgb_bpp);
      return 0;

    case FIELD_TERNARY_SQUARE:
      scale[0] = scale[2] = 0.;
      scale[1] = scale[3] = 1.;
      for (j=0; j<thedisp->rgb_height; j++)
	for (i=0; i<thedisp->rgb_width; i++)
	  pseudoternarysquarecolor
	    ((PetscScalar) i/(thedisp->rgb_width-1),
	     (PetscScalar) j/(thedisp->rgb_height-1), scale,
	     thedisp->rgb+(j*thedisp->rgb_rowskip + i) * thedisp->rgb_bpp);
      return 0;

    case FIELD_TENSOR_SHEAR:
      scale[0] = 1.;
      myheight = ((thedisp->rgb_height > thedisp->rgb_width) ?
		  thedisp->rgb_width : thedisp->rgb_height)/2;
      for (j=-myheight; j<myheight; j++)
	for (i=-(int)(myheight*sqrt(1.-((PetscScalar)j*j/myheight/myheight)));
	     i<(int)(myheight*sqrt(1.-((PetscScalar)j*j/myheight/myheight)));
	     i++)
	  {
	    PetscScalar mag = (PetscScalar) sqrt (i*i+j*j)/myheight,
	      theta = atan2 (j, i);
	    pseudoshearcolor
	      (mag * cos (2*theta), mag * sin (2*theta), scale,
	       thedisp->rgb + ((myheight-j)*thedisp->rgb_rowskip +
			       thedisp->rgb_width/2+i)*thedisp->rgb_bpp);
	  }
      return 0;
    }
  SETERRQ (PETSC_ERR_ARG_OUTOFRANGE, "Field type not yet supported");
}


#undef __FUNCT__
#define __FUNCT__ "render_composition_path"


/*++++++++++++++++++++++++++++++++++++++
  Render a composition path, such as a diffusion path or phase diagram, onto an
  IDisplay image.

  int render_composition_path Returns zero or an error code.

  IDisplay Disp IDisplay object into which to draw the path.

  PetscScalar comp_array Array of compositions for drawing.

  int gridpoints Number of gridpoints in the composition array.

  int num_fields Total number of fields in the composition array.

  field_plot_type fieldtype The type of this field.

  PetscScalar *scale Array of minimum and maximum values to pass to the
  various pseudocolor functions; if NULL, call auto_scale to determine those
  values.

  PetscScalar red Red color for composition path.

  PetscScalar green Green color for composition path.

  PetscScalar blue Blue color for composition path.

  PetscScalar alpha Alpha channel for composition path.
  ++++++++++++++++++++++++++++++++++++++*/

int render_composition_path
(IDisplay Disp, PetscScalar *comp_array, int gridpoints, int num_fields,
 field_plot_type fieldtype, PetscScalar *scale,
 PetscScalar red,PetscScalar green,PetscScalar blue,PetscScalar alpha)
{
  int i, ierr;
  PetscScalar local_scale[6];
  struct idisplay *thedisp = (struct idisplay *) Disp;

  /* Sanity checks */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Null display object");
  if (!(thedisp->rgb))
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "Display object has no RGB buffer to draw in");

  /* Determine default min and max if none are provided */
  if (scale == NULL)
    {
      scale = local_scale;
      ierr = auto_scale (comp_array, gridpoints, num_fields, 0, fieldtype, 2,
			 scale); CHKERRQ (ierr);
    }

  for (i=0; i<gridpoints; i++)
    {
      PetscScalar A=comp_array [i*num_fields], B=comp_array [i*num_fields+1];
      int sx, sy;
      guchar *spixel;

      if (fieldtype == FIELD_TERNARY)
	{
	  int myheight = ((PetscScalar) thedisp->rgb_height >
			  0.5*sqrt(3.)*thedisp->rgb_width) ?
	    (int) (0.5*sqrt(3.)* thedisp->rgb_width) : thedisp->rgb_height;
	  PetscScalar inverse_det =
	    1./((scale[2]-scale[0])*(scale[5]-scale[1]) -
		(scale[4]-scale[0])*(scale[3]-scale[1]));
	  sy = myheight*(((B-scale[1])*(scale[2]-scale[0]) -
			  (A-scale[0])*(scale[3]-scale[1])) *inverse_det);
	  sx = thedisp->rgb_width* (((A-scale[0])*(scale[5]-scale[1]) -
				     (B-scale[1])*(scale[4]-scale[0])) *inverse_det)+
	    thedisp->rgb_width*sy/myheight/2;
	  sy = (thedisp->rgb_height+myheight)/2 - sy;
	}
      else if (fieldtype == FIELD_TERNARY_SQUARE)
	{
	  sx = thedisp->rgb_width * (A-scale[0])/(scale[1]-scale[0]);
	  sy = thedisp->rgb_height*(B-scale[2])/(scale[3]-scale[2]);
	}
      else
	sx=sy=-1; /* Don't draw diffusion path */

      if (sx>=0 && sy>=0 && sx<thedisp->rgb_width &&
	  sy<thedisp->rgb_height)
	{
	  spixel = thedisp->rgb +
	    thedisp->rgb_bpp*(sy*thedisp->rgb_rowskip + sx);
	  *spixel = (1.-alpha)* *spixel + red*alpha*255;
	  *(spixel+1) = (1.-alpha)* *(spixel+1) + green*alpha*255;
	  *(spixel+2) = (1.-alpha)* *(spixel+2) + blue*alpha*255;
	}
    }
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "render_rgb_local_2d"

/*++++++++++++++++++++++++++++++++++++++
  Render data from global_array into local part of an RGB buffer.  When running
  in parallel, these local buffers should be collected and layered to produce
  the full image.

  int render_rgb_local_2d Returns zero or an error code.

  IDisplay Disp Display object holding the RGB buffer and its characteristics.

  PetscScalar *global_array Local array of global vector data to render.

  int num_fields Number of field variables in the array.

  int display_field The (first) field we are rendering now.

  field_plot_type fieldtype The type of this field.

  PetscScalar *scale Array of minimum and maximum values to pass to the
  various pseudocolor functions; if NULL, call auto_scale to determine those
  values.

  int nx Width of the array.

  int ny Height of the array.

  int xs Starting
  +latex+$x$-coordinate
  +html+ <i>x</i>-coordinate
  of the local part of the global vector.

  int ys Starting
  +latex+$y$-coordinate
  +html+ <i>y</i>-coordinate
  of the local part of the global vector.

  int xm Width of the local part of the global vector.

  int ym Height of the local part of the global vector.

  int transform Transformation flags.

  IDisplay SDisp Display object in which to draw the diffusion path (optional,
  ignored if NULL).

  PetscScalar dpred Red color for diffusion path points (0-1).

  PetscScalar dpgreen Green color for diffusion path points (0-1).

  PetscScalar dpblue Blue color for diffusion path points (0-1).

  PetscScalar dpalpha Alpha channel for diffusion path points (0-1).
  ++++++++++++++++++++++++++++++++++++++*/

int render_rgb_local_2d
(IDisplay Disp, PetscScalar *global_array, int num_fields, int display_field,
 field_plot_type fieldtype, PetscScalar *scale, int nx,int ny, int xs,int ys,
 int xm,int ym, int transform, IDisplay SDisp, PetscScalar dpred,
 PetscScalar dpgreen, PetscScalar dpblue, PetscScalar dpalpha)
{
  int ix,iy, ierr=0;
  PetscScalar local_scale[6];
  struct idisplay *thedisp = (struct idisplay *) Disp;

  /* Sanity checks */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Null display object");
  if (!(thedisp->rgb))
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "Display object has no RGB buffer to draw in");

  /* Determine default min and max if none are provided */
  if (scale == NULL)
    {
      scale = local_scale;
      ierr = auto_scale (global_array, xm*ym, num_fields, display_field,
			   fieldtype, 2, scale); CHKERRQ (ierr);
    }
  DPRINTF ("Final scale: %g %g %g %g %g %g\n", scale[0],
	   scale[1], scale[2], scale[3], scale[4], scale[5]);

  /* Do the rendering (note switch in inner loop, gotta get rid of that) */
  for (iy=thedisp->rgb_height*ys/ny; iy<thedisp->rgb_height*(ys+ym)/ny; iy++)
    for (ix=thedisp->rgb_width*xs/nx; ix<thedisp->rgb_width*(xs+xm)/nx; ix++)
      {
	int localx, localy;
	if (transform & ROTATE_LEFT)
	  {
	    localx = ((transform & FLIP_HORIZONTAL) ?
		      iy : thedisp->rgb_height-iy-1) * nx/thedisp->rgb_height;
	    localy = ((transform & FLIP_VERTICAL) ?
		      ix : thedisp->rgb_width-ix-1) * ny/thedisp->rgb_width;
	  }
	else
	  {
	    localx = ((transform & FLIP_HORIZONTAL) ?
		      thedisp->rgb_width-ix-1 : ix) * nx/thedisp->rgb_width;
	    localy = ((transform & FLIP_VERTICAL) ?
		      iy : thedisp->rgb_height-iy-1) * ny/thedisp->rgb_height;
	  }

	int vecindex = (localy*xm + localx)*num_fields + display_field;
	guchar *pixel = thedisp->rgb +
	  thedisp->rgb_bpp*(iy*thedisp->rgb_rowskip + ix);

	switch (fieldtype)
	  {
	  case FIELD_SCALAR:
	  case FIELD_SCALAR+1:
	    {
	      pseudocolor (global_array [vecindex], scale, pixel);
	      break;
	    }
	  case FIELD_TERNARY:
	    {
	      pseudoternarycolor
		(global_array [vecindex], global_array [vecindex+1], scale,
		 pixel);
	      break;
	    }
	  case FIELD_TERNARY_SQUARE:
	    {
	      pseudoternarysquarecolor
		(global_array [vecindex], global_array [vecindex+1], scale,
		 pixel);
	      break;
	    }
	  case FIELD_VECTOR:
	  case FIELD_VECTOR+1:
	    {
	      pseudohueintcolor
		(global_array [vecindex], global_array [vecindex+1], scale,
		 pixel);
	      break;
	    }
	  case FIELD_TENSOR_SHEAR:
	    {
	      pseudoshearcolor
		(global_array [vecindex], global_array [vecindex+1], scale,
		 pixel);
	      break;
	    }
	  default:
	    SETERRQ (PETSC_ERR_ARG_OUTOFRANGE, "Field type not yet supported");
	  }
      }

  /* (Optional) diffusion path */
  DPRINTF ("Done main rendering, doing scale.\n",0);
  if (SDisp)
    ierr=render_composition_path (SDisp, global_array+display_field, xm*ym,
				  num_fields, fieldtype, scale,
				  dpred,dpgreen,dpblue,dpalpha);

  return ierr;
}


#undef __FUNCT__
#define __FUNCT__ "render_rgb_local_3d"

/*++++++++++++++++++++++++++++++++++++++
  Render triangle data into an RGB buffer.  When called in parallel, the
  resulting images should be layered to give the complete picture.  Zooming is
  done by adjusting the ratio of the dir vector to the right vector.
  +latex+\par
  +html+ <p>
  The coordinate transformation is pretty simple.
  +latex+A point $\vec{p}$ in space is transformed to $x,y$ on the screen by
  +latex+representing it as:
  +latex+\begin{equation}
  +latex+  \label{eq:transform}
  +latex+  \vec{p} = \vec{\imath} + a \vec{d} + ax \vec{r} + ay \vec{u},
  +latex+\end{equation}
  +latex+where $\vec{\imath}$ is the observer's point (passed in as {\tt eye}),
  +latex+$\vec{d}$ is the direction of observation (passed in as {\tt dir}),
  +latex+$\vec{r}$ is the rightward direction to the observer (passed in as
  +latex+{\tt right}), and $\vec{u}$ is the upward direction to the observer
  +latex+which is given the direction of $\vec{r}\times\vec{d}$ and the
  +latex+magnitude of $\vec{r}$.
  +html+ A point <u><i>p</i></u> in space is transformed to <i>x, y</i> on the
  +html+ screen by representing it as:
  +html+ <center><u><i>p</i></u> = <u><i>i</i></u> + <i>a <u>d</u></i> +
  +html+ <i>ax <u>r</u></i> + <i>ay <u>u</u></i>,</center>
  +html+ where <u><i>i</i></u> is the observer's point (passed in as
  +html+ <tt>eye</tt>), <u><i>d</i></u> is the direction of observation (passed
  +html+ in as <tt>dir</tt>), <u><i>r</i></u> is the rightward direction to the
  +html+ observer (passed in as <tt>right</tt>), and <u><i>u</i></u> is the
  +html+ upward direction to the observer which is given the direction of
  +html+ the cross product of <u><i>d</i></u> and <u><i>r</i></u> and the
  +html+ magnitude of <u><i>r</i></u>.
  +latex+\par
  +html+ <p>
  +latex+This system in equation \ref{eq:transform} can easily be solved for
  +latex+$x$ and $y$ by first making it into a matrix equation:
  +latex+\begin{equation}
  +latex+  \label{eq:matrix}
  +latex+  \left(\begin{array}{ccc} d_x&r_x&u_x \\ d_y&r_y&u_y \\ d_z&r_z&u_z
  +latex+    \end{array}\right)
  +latex+  \left(\begin{array}{c} a \\ ax \\ ay \end{array}\right) =
  +latex+  \left(\begin{array}{c} p_x-i_x \\ p_y-i_y \\ p_z-i_z
  +latex+    \end{array}\right).
  +latex+\end{equation}
  +latex+Calling the matrix $M$ the inverse of the matrix on the left side of
  +latex+equation \ref{eq:matrix} gives the result:
  +latex+\begin{eqnarray}
  +latex+  a&=& M_{00} (p_x-i_x) + M_{01} (p_y-i_y) + M_{02} (p_z-i_z),
  +latex+  \\ x&=& \frac{1}{a}[M_{10}(p_x-i_x)+M_{11}(p_y-i_y)+M_{12}(p_z-i_z)],
  +latex+  \\ y&=& \frac{1}{a}[M_{20}(p_x-i_x)+M_{21}(p_y-i_y)+M_{22}(p_z-i_z)].
  +latex+\end{eqnarray}
  +html+ This system can easily be solved for <i>x</i> and <i>y</i> by first
  +html+ making it into a matrix equation:
  +html+ <center>(<i><u>d</u> <u>r</u> <u>u</u></i>) (<i>a, ax, ay</i>) =
  +html+ <u><i>p</i></u> - <u><i>i</i></u>.</center>
  +html+ Calling the matrix <u><i>M</i></u> the inverse of the matrix on the
  +html+ left side gives the result:
  +html+ <center><table>
  +html+ <tr><td><i>a</i></td><td>=</td><td>
  +html+ <i>M</i><sub>00</sub> (<i>p<sub>x</sub></i>-<i>i<sub>x</sub></i>) +
  +html+ <i>M</i><sub>01</sub> (<i>p<sub>y</sub></i>-<i>i<sub>y</sub></i>) +
  +html+ <i>M</i><sub>02</sub> (<i>p<sub>z</sub></i>-<i>i<sub>z</sub></i>),
  +html+ </td></tr><tr><td><i>x</i></td><td>=</td><td>
  +html+ [<i>M</i><sub>10</sub> (<i>p<sub>x</sub></i>-<i>i<sub>x</sub></i>) +
  +html+ <i>M</i><sub>11</sub> (<i>p<sub>y</sub></i>-<i>i<sub>y</sub></i>) +
  +html+ <i>M</i><sub>12</sub> (<i>p<sub>z</sub></i>-<i>i<sub>z</sub></i>)]
  +html+ / <i>a</i>,</td></tr><tr><td><i>y</i></td><td>=</td><td>
  +html+ [<i>M</i><sub>00</sub> (<i>p<sub>x</sub></i>-<i>i<sub>x</sub></i>) +
  +html+ <i>M</i><sub>01</sub> (<i>p<sub>y</sub></i>-<i>i<sub>y</sub></i>) +
  +html+ <i>M</i><sub>02</sub> (<i>p<sub>z</sub></i>-<i>i<sub>z</sub></i>)]
  +html+ / <i>a</i>.</td></tr></table></center>
  +latex+\par
  +html+ <p>
  The triple product of the vector from the light source to the triangle
  centroid and the two vectors making up the triangle edges determines the
  cosine of the angle made by the triangle normal and the incident light, whose
  absolute value is used here to shade the triangle.  At this point, the light
  source is taken as the observer location at
  +latex+``{\tt eye}'',
  +html+ "<tt>eye</tt>",
  but that can easily be modified to use one or more independent light sources.

  int render_rgb_local_3d Returns zero or an error code.

  IDisplay Disp Display object holding the RGB buffer and its characteristics.

  ISurface Surf ISurface object containing triangles to render using imlib2
  backend.

  PetscScalar *eye Point from where we're looking (x,y,z).

  PetscScalar *dir Direction we're looking (x,y,z).

  PetscScalar *right Rightward direction in physical space (x,y,z).
  ++++++++++++++++++++++++++++++++++++++*/

int render_rgb_local_3d
(IDisplay Disp, ISurface Surf, PetscScalar *eye, PetscScalar *dir,
 PetscScalar *right)
{
  int *screen_coords, i;
  PetscScalar *shades, up[3], m00,m01,m02,m10,m11,m12,m20,m21,m22, a;
  struct isurface *thesurf = (struct isurface *) Surf;
  struct idisplay *thedisp = (struct idisplay *) Disp;

  /* Sanity checks */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Null display object");
  if (!(thedisp->rgb))
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "Display object has no RGB buffer to draw in");

  if (thedisp->rgb_bpp != 4)
    SETERRQ (PETSC_ERR_ARG_SIZ, "Imlib2 rendering requires 4 bytes per pixel");

  /* Allocate memory for the shades and integer coordinates */
  i = PetscMalloc (6*thesurf->num_triangles * sizeof(int), &screen_coords);
  CHKERRQ (i);
  i = PetscMalloc (thesurf->num_triangles * sizeof(PetscScalar), &shades);
  CHKERRQ (i);

  /* Calculate the "up" vector as described above, storing the magnitude of dir
     in a */
  a = sqrt (dir[0]*dir[0] + dir[1]*dir[1] + dir[2]*dir[2]);
  up[0] = (right[1]*dir[2] - right[2]*dir[1]) / a;
  up[1] = (right[2]*dir[0] - right[0]*dir[2]) / a;
  up[2] = (right[0]*dir[1] - right[1]*dir[0]) / a;

  /* Calculate the inverse of the dir, right, up columnar matrix, storing the
     determinant of the matrix in a */
  a = (dir[0] * (right[1]*up[2] - right[2]*up[1]) +
       dir[1] * (right[2]*up[0] - right[0]*up[2]) +
       dir[2] * (right[0]*up[1] - right[1]*up[0]));
  m00 = (right[1]*up[2]   - right[2]*up[1])    / a;
  m01 = (right[2]*up[0]   - right[0]*up[2])    / a;
  m02 = (right[0]*up[1]   - right[1]*up[0])    / a;
  m10 = (up[1]   *dir[2]  - up[2]   *dir[1])   / a;
  m11 = (up[2]   *dir[0]  - up[0]   *dir[2])   / a;
  m12 = (up[0]   *dir[1]  - up[1]   *dir[0])   / a;
  m20 = (dir[1] *right[2] - dir[2]  *right[1]) / a;
  m21 = (dir[2] *right[0] - dir[0]  *right[2]) / a;
  m22 = (dir[0] *right[1] - dir[1]  *right[0]) / a;

  /* Loop over triangles, transforming their coordinates and setting shading */
  for (i=0; i<thesurf->num_triangles; i++)
    {
      PetscScalar pmi00,pmi01,pmi02,pmi10,pmi11,pmi12,pmi20,pmi21,pmi22, c[3];

      /* Calculate coordinates relative to observer p-i (hence pmi) */
      pmi00 = thesurf->vertices[13*i]   - eye[0];
      pmi01 = thesurf->vertices[13*i+1] - eye[1];
      pmi02 = thesurf->vertices[13*i+2] - eye[2];
      pmi10 = thesurf->vertices[13*i+3] - eye[0];
      pmi11 = thesurf->vertices[13*i+4] - eye[1];
      pmi12 = thesurf->vertices[13*i+5] - eye[2];
      pmi20 = thesurf->vertices[13*i+6] - eye[0];
      pmi21 = thesurf->vertices[13*i+7] - eye[1];
      pmi22 = thesurf->vertices[13*i+8] - eye[2];

      /* Calculate x,y for point 0 */
      a = m00*pmi00 + m01*pmi01 + m02*pmi02;
      if (a < 0)
	screen_coords [6*i+0] = screen_coords [6*i+1] = -1;
      else
	{
	  screen_coords [6*i+0] = thedisp->rgb_width/2 *
	    (1. + (m10*pmi00 + m11*pmi01 + m12*pmi02) / a);
	  screen_coords [6*i+1] = thedisp->rgb_height/2 - thedisp->rgb_width/2 *
	    (m20*pmi00 + m21*pmi01 + m22*pmi02) / a;
	}

      /* Calculate x,y for point 1 */
      a = m00*pmi10 + m01*pmi11 + m02*pmi12;
      if (a <= 0)
	screen_coords [6*i+2] = screen_coords [6*i+3] = -1;
      else
	{
	  screen_coords [6*i+2] = thedisp->rgb_width/2 *
	    (1. + (m10*pmi10 + m11*pmi11 + m12*pmi12) / a);
	  screen_coords [6*i+3] = thedisp->rgb_height/2 - thedisp->rgb_width/2 *
	    (m20*pmi10 + m21*pmi11 + m22*pmi12) / a;
	}

      /* Calculate x,y for point 2 */
      a = m00*pmi20 + m01*pmi21 + m02*pmi22;
      if (a <= 0)
	screen_coords [6*i+4] = screen_coords [6*i+5] = -1;
      else
	{
	  screen_coords [6*i+4] = thedisp->rgb_width/2 *
	    (1. + (m10*pmi20 + m11*pmi21 + m12*pmi22) / a);
	  screen_coords [6*i+5] = thedisp->rgb_height/2 - thedisp->rgb_width/2 *
	    (m20*pmi20 + m21*pmi21 + m22*pmi22) / a;
	}

#ifdef DEBUG
      if (i==0)
	DPRINTF("First triangle: (%g,%g,%g), (%g,%g,%g), (%g,%g,%g);\n"
		"2-D coordinates: (%d,%d), (%d,%d), (%d,%d)\n",
		thesurf->vertices[0],thesurf->vertices[1],thesurf->vertices[2],
		thesurf->vertices[3],thesurf->vertices[4],thesurf->vertices[5],
		thesurf->vertices[6],thesurf->vertices[7],thesurf->vertices[8],
		screen_coords[0],screen_coords[1],screen_coords[3],
		screen_coords[3],screen_coords[4],screen_coords[5]);
#endif

      /* Calculate relative triangle centroid, triangle arms, and shade */
      c[0] = (pmi00 + pmi10 + pmi20) / 3.;
      c[1] = (pmi01 + pmi11 + pmi21) / 3.;
      c[2] = (pmi02 + pmi12 + pmi22) / 3.;
      pmi10 -= pmi00;   pmi11 -= pmi01;   pmi12 -= pmi02;
      pmi20 -= pmi00;   pmi21 -= pmi01;   pmi22 -= pmi02;
      shades [i] = PetscAbsScalar (c[0] * (pmi11*pmi22 - pmi12*pmi21) +
				   c[1] * (pmi12*pmi20 - pmi10*pmi22) +
				   c[2] * (pmi10*pmi21 - pmi11*pmi20)) /
	sqrt (c[0]*c[0] + c[1]*c[1] + c[2]*c[2]) /
	sqrt (pmi10*pmi10 + pmi11*pmi11 + pmi12*pmi12) /
	sqrt (pmi20*pmi20 + pmi21*pmi21 + pmi22*pmi22);
    }

#ifdef IMLIB2_EXISTS
  /* Do the Imlib2 rendering */
  imlib2_render_triangles
    ((DATA32 *) thedisp->rgb, thedisp->rgb_width, thedisp->rgb_height, thesurf->num_triangles, screen_coords,
     thesurf->vertices+9, 13, shades, 1);

  /* Imlib2 switches red and blue for some reason */
  for (i=0; i<thedisp->rgb_height; i++)
    {
      int j;
      for (j = i*thedisp->rgb_rowskip;
	   j < i*thedisp->rgb_rowskip + thedisp->rgb_width; j++)
	{
	  guchar tmp = thedisp->rgb [4*j];
	  thedisp->rgb[4*j] = thedisp->rgb[4*j+2];
	  thedisp->rgb[4*j+2] = tmp;
	}
    }
#else
  SETERRQ (PETSC_ERR_SUP, "3-D rendering requires Imlib2");
#endif

  i = PetscFree (shades); CHKERRQ (i);
  i = PetscFree (screen_coords); CHKERRQ (i);
}
