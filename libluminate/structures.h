/***************************************
  $Header$

  This file defines the isurface and idisplay structures which map to the
  ISurface and IDisplay data types in illuminator.h.  It also contains the
  zbuffer data type for multiz.c.
  ***************************************/


#ifndef SURFACE_H
#define SURFACE_H    /*+ To stop multiple inclusions. +*/

#include <illuminator.h> /* For various types */

struct isurface {
  /*+ Number of triangles in this node.  Its value is initialized to zero, and
    incremented as each triangle is added, then reset to zero when the
    triangulation is displayed. +*/
  int num_triangles;

  /*+ Number of triangles allocated in the vertices array. +*/
  int vertisize; 

  /*+ Array of vertex corners of triangles.  The number of triangles is given
    by num_triangles, and size of the array by vertisize.  For each triangle,
    this array has the coordinates of the three nodes, and its R, G, B and A
    color values, hence 13 PetscScalars for each triangle. +*/
  PetscScalar *vertices;
};

/*+ The zbuffer type, used to store an entry for a pixel. +*/
typedef struct {
  guchar r,g,b,a;
  float z;
} zbuffer;

struct idisplay {
  /*+ Geomview output pipe +*/
  FILE *to_geomview;

  guchar *rgb;
  int rgb_width, rgb_height, rgb_rowskip, rgb_bpp;

  zbuffer *zbuf;
  int zbuf_width, zbuf_height, zbuf_rowskip, zbuf_depth;
};

#endif /* SURFACE_H */
