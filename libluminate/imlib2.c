/***************************************
  $Header$

  This file uses Imlib2 to turn guchar arrays into Imlib images, render sets of
  triangles into those images, and send back the guchar arrays.
  ***************************************/


#include "illuminator.h"


#undef __FUNCT__
#define __FUNCT__ "imlib2_render_triangles"

/*++++++++++++++++++++++++++++++++++++++
  This simply takes a bunch of triangle vertex and color data and renders it
  into the "data" buffer in RGBA format using Imlib2.

  int imlib2_render_triangles It returns zero or an error code.

  DATA32 *data The data buffer into which to render with
  +latex+4$\times$width$\times$height
  +html+ 4 x width x height
  bytes.

  int width Width of the data buffer in pixels.

  int height Height of the data buffer in pixels.

  int num_triangles Number of triangles to render.

  int *triangle_coords Integer coordinates of the triangles.

  PetscScalar *triangle_colors R,G,B,A colors of the triangles between 0 and 1.

  int color_skip Number of PetscScalars to skip between color sets.

  PetscScalar *triangle_shades Shading of each triangle, zero for black to one
  for normal.

  int shade_skip Number of PetscScalars to skip between shades.
  ++++++++++++++++++++++++++++++++++++++*/

int imlib2_render_triangles (DATA32 *data, int width, int height,
			     int num_triangles, int *triangle_coords,
			     PetscScalar *triangle_colors, int color_skip,
			     PetscScalar *triangle_shades, int shade_skip)
{
  int i;
  Imlib_Image myimage =
    imlib_create_image_using_data (width, height, data);
  imlib_context_set_image (myimage);

  for (i=0; i<num_triangles; i++)
    {
      ImlibPolygon polly = imlib_polygon_new ();
      imlib_polygon_add_point
	(polly, triangle_coords [6*i], triangle_coords [6*i+1]);
      imlib_polygon_add_point
	(polly, triangle_coords[6*i+2], triangle_coords[6*i+3]);
      imlib_polygon_add_point
	(polly, triangle_coords[6*i+4], triangle_coords[6*i+5]);
      imlib_context_set_color (255 * triangle_colors [color_skip*i] *
			       triangle_shades [shade_skip*i],
			       255 * triangle_colors [color_skip*i+1] *
			       triangle_shades [shade_skip*i],
			       255 * triangle_colors [color_skip*i+2] *
			       triangle_shades [shade_skip*i],
			       255 * triangle_colors [color_skip*i+3]);
      imlib_image_fill_polygon (polly);
      imlib_polygon_free (polly);
    }

  imlib_free_image ();
}
