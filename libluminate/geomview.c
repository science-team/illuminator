/***************************************
  $Header$

  This file has the Geomview interface, including the PETSc vector gather
  operations to bring everything to CPU 0.
***************************************/


#include <stdio.h>
#include <petscvec.h>
#include "config.h"      /* esp. for inline */
#include "illuminator.h" /* Just to make sure the interface is "right" */
#include "structures.h"     /* For isurface structure definition */
#include <stdlib.h>      /* For malloc() */
#include <unistd.h>      /* For execlp() */
#include <glib.h>        /* For guint*, GUINT*_SWAP_LE_BE */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) PetscPrintf (PETSC_COMM_WORLD, "%s: " fmt, __FUNCT__, args)
#else
#define DPRINTF(fmt, args...)
#endif


#undef __FUNCT__
#define __FUNCT__ "GeomviewBegin"

/*++++++++++++++++++++++++++++++++++++++
  Spawn a new geomview process.  Most of this was shamelessly ripped from Ken
  Brakke's Surface Evolver.

  int GeomviewBegin Returns 0 or an error code.

  MPI_Comm comm MPI communicator for rank information, if NULL it uses
  PETSC_COMM_WORLD.

  IDisplay *newdisp Pointer in which to put a new IDisplay object.
  ++++++++++++++++++++++++++++++++++++++*/

int GeomviewBegin(MPI_Comm comm, IDisplay *newdisp)
{
  int rank, ierr, gv_pid,gv_pipe[2], to_gv_pipe[2];
  char gv_version[100];
  struct idisplay *thedisp;

  if (!(thedisp = (struct idisplay *) malloc (sizeof (struct idisplay))))
    SETERRQ (PETSC_ERR_MEM, "Unable to allocate memory for isurface object");

  /* Initialize display image to zero */
  thedisp->rgb = NULL;
  thedisp->rgb_width = thedisp->rgb_height = thedisp->rgb_rowskip =
    thedisp->rgb_bpp = 0;
  thedisp->zbuf = NULL;
  thedisp->zbuf_width = thedisp->zbuf_height = thedisp->zbuf_depth =
    thedisp->zbuf_rowskip = 0;

  if (!comm)
    comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank (comm,&rank); CHKERRQ(ierr);
  if (!rank)
    {
      pipe (gv_pipe); /* from geomview stdout */
      pipe (to_gv_pipe); /* to geomview stdin */
      gv_pid = fork ();

      if(gv_pid==0) /* child */
	{
	  close (0);
	  dup (to_gv_pipe[0]);
	  close (to_gv_pipe[0]);
	  close (to_gv_pipe[1]);
	  close (1);
	  dup (gv_pipe[1]);
	  close (gv_pipe[0]);
	  close (gv_pipe[1]);
	  /* signal(SIGINT,SIG_IGN); */
	  execlp (GEOMVIEW,GEOMVIEW,"-c","(interest (pick world))","-",NULL);
	  perror (GEOMVIEW); /* only error gets here */
	  SETERRQ (PETSC_ERR_ARG_WRONGSTATE,"Geomview invocation failed.\n");
	}

      /* PETSc program execution resumes here */
      close (gv_pipe[1]);
      close (to_gv_pipe[0]);
      thedisp->to_geomview = fdopen (to_gv_pipe[1], "w"); /* geomview stdin */
      fprintf (thedisp->to_geomview, "(echo (geomview-version) \"\n\")\n");
      fflush (thedisp->to_geomview);
      read (gv_pipe[0], gv_version, sizeof (gv_version));
    }

  *newdisp = (IDisplay) thedisp;
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "GeomviewEnd"

/*++++++++++++++++++++++++++++++++++++++
  Exit the current running Geomview process and close its pipe.  Based in part
  on Ken Brakke's Surface Evolver.

  int GeomviewEnd Returns 0 or an error code.

  MPI_Comm comm MPI communicator for rank information, if NULL it uses
  PETSC_COMM_WORLD.

  IDisplay Disp IDisplay object whose geomview FILE we're closing.
  ++++++++++++++++++++++++++++++++++++++*/

int GeomviewEnd (MPI_Comm comm, IDisplay Disp)
{
  int rank, ierr;
  struct idisplay *thedisp = (struct idisplay *) Disp;

  if (!comm)
    comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank (comm, &rank); CHKERRQ (ierr);
  if (!rank)
    {
      if (thedisp->to_geomview)
	{
	  fprintf (thedisp->to_geomview, "(exit)");
	  fclose (thedisp->to_geomview);
	  thedisp->to_geomview = NULL;
	}
    }
  return 0;
}


#ifdef WORDS_BIGENDIAN
# define GEOMVIEW_SET_INT(j,x) gvint[j]=(guint32)(x)
# define GEOMVIEW_SET_FLOAT(j,x) gvfloat[j]=(float)(x)
#else
# define GEOMVIEW_SET_INT(j,x) \
  gvint[j]=GUINT32_SWAP_LE_BE_CONSTANT((guint32) (x))
# define GEOMVIEW_SET_FLOAT(j,x) \
  {gvfloat[j]=(float)(x); gvint[j]=GUINT32_SWAP_LE_BE_CONSTANT(gvint[j]);}
#endif

/* #define GEOMVIEW_BINARY */


#undef __FUNCT__
#define __FUNCT__ "GeomviewDisplayTriangulation"

/*++++++++++++++++++++++++++++++++++++++
  Pipe the current triangulation to Geomview for display.  Much of this is
  based on Ken Brakke's Surface Evolver.

  int GeomviewDisplayTriangulation Returns 0 or an error code.

  MPI_Comm comm MPI communicator for rank information, if NULL it uses
  PETSC_COMM_WORLD.

  ISurface Surf ISurface object containing triangles to render using Geomview.

  IDisplay Disp IDisplay object whose geomview FILE we're closing.

  PetscScalar *minmax Position of block corners: xmin, xmax, ymin, ymax, zmin, zmax.

  char *name Name to give the Geomview OOGL object which we create.

  PetscTruth transparent Geomview transparency flag.
  ++++++++++++++++++++++++++++++++++++++*/

int GeomviewDisplayTriangulation
(MPI_Comm comm, ISurface Surf, IDisplay Disp, PetscScalar *minmax, char *name,
 PetscTruth transparent)
{
  int ierr, i, total_entries, start, end, rank;
  PetscScalar *localvals;
  Vec globalverts, localverts;
  VecScatter vecscat;
  IS islocal;
  struct isurface *thesurf = (struct isurface *) Surf;
  struct idisplay *thedisp = (struct idisplay *) Disp;

  /* Simple "assertion" check */
  if (!comm)
    comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank (comm, &rank); CHKERRQ (ierr);
  if (!rank && !thedisp->to_geomview)
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,"Geomview display is not open!");

  /*+ First, this creates global and local vectors for all of the triangle
    vertices. +*/
  ierr = VecCreateMPIWithArray (comm, 13*thesurf->num_triangles,
				PETSC_DETERMINE,
				thesurf->vertices, &globalverts); CHKERRQ (ierr);
  ierr = VecGetSize (globalverts, &total_entries); CHKERRQ (ierr);
  ierr = VecCreateMPI (comm, (rank == 0) ? total_entries:0, total_entries,
		       &localverts); CHKERRQ (ierr);
  DPRINTF ("Total triangles: %d\n", total_entries/13);

  /* Diagnostics which may be useful to somebody sometime */
  /* ierr = VecGetOwnershipRange (globalverts, &start, &end); CHKERRQ (ierr);
  ierr = PetscSynchronizedPrintf
    (comm, "[%d] Global vector local size: %d, ownership from %d to %d\n",
     rank, end-start, start, end); CHKERRQ (ierr);
  ierr = PetscSynchronizedFlush (comm); CHKERRQ (ierr);

  ierr = VecGetOwnershipRange (localverts, &start, &end); CHKERRQ (ierr);
  ierr = PetscSynchronizedPrintf
    (comm, "[%d] Local vector local size: %d, ownership from %d to %d\n", rank,
     end-start, start, end); CHKERRQ (ierr);
  ierr = PetscSynchronizedFlush (comm); CHKERRQ (ierr);
  ierr = PetscPrintf (comm, "Global vector size: %d\n", total_entries);
  CHKERRQ (ierr); */

  /*+ It then gathers (``scatters'') all vertex data to processor zero, +*/
  ierr = ISCreateStride (comm, total_entries, 0, 1, &islocal); CHKERRQ (ierr);
  ierr = VecScatterCreate (globalverts, PETSC_NULL, localverts, islocal,
			   &vecscat); CHKERRQ (ierr);
  DPRINTF ("Starting triangle scatter to head node\n",0);
  ierr = VecScatterBegin (vecscat, globalverts, localverts, INSERT_VALUES,
			  SCATTER_FORWARD); CHKERRQ (ierr);
  DPRINTF ("Triangle scatter to head node in progress...\n",0);
  ierr = VecScatterEnd (vecscat, globalverts, localverts, INSERT_VALUES,
			SCATTER_FORWARD); CHKERRQ (ierr);
  DPRINTF ("Triangle scatter to head node complete\n",0);
  ierr = VecScatterDestroy (vecscat); CHKERRQ (ierr);
  ierr = ISDestroy (islocal); CHKERRQ (ierr);

  /*+ and puts them in an array. +*/
  ierr = VecGetArray (localverts, &localvals); CHKERRQ (ierr);

  /*+ Finally, it sends everything to Geomview, +*/
  if (!rank) {
#ifdef GEOMVIEW_BINARY
    guint32 gvint[10];
    float *gvfloat = (float *)gvint;
#endif

    fprintf (thedisp->to_geomview, "(geometry \"%s\" { : bor })", name);
    fprintf (thedisp->to_geomview, "(read geometry { define bor \n");
    fprintf (thedisp->to_geomview, "appearance {%s}\nOFF%s\n",
	     transparent ? "+transparent" : "",
#ifdef GEOMVIEW_BINARY
	     " BINARY"
#else
	     ""
#endif
	     );
#ifndef GEOMVIEW_BINARY
    fprintf (thedisp->to_geomview, "%d %d 0\n", 3*(total_entries/13) + 2,
	     total_entries/13);
#else
    GEOMVIEW_SET_INT (0, 3*(total_entries/13) + 2);
    GEOMVIEW_SET_INT (1, total_entries/13);
    GEOMVIEW_SET_INT (2, 0);
    fwrite (gvint, sizeof (guint32), 3, thedisp->to_geomview);
#endif
    for (i=0; i<total_entries; i+=13)
      {
#ifndef GEOMVIEW_BINARY
	fprintf (thedisp->to_geomview, "%g %g %g\n%g %g %g\n%g %g %g\n",
		 localvals[i], localvals[i+1], localvals[i+2], localvals[i+3],
		 localvals[i+4], localvals[i+5], localvals[i+6],localvals[i+7],
		 localvals[i+8]);
#else
	GEOMVIEW_SET_FLOAT(0, localvals[i]);
	GEOMVIEW_SET_FLOAT(1, localvals[i+1]);
	GEOMVIEW_SET_FLOAT(2, localvals[i+2]);
	GEOMVIEW_SET_FLOAT(3, localvals[i+3]);
	GEOMVIEW_SET_FLOAT(4, localvals[i+4]);
	GEOMVIEW_SET_FLOAT(5, localvals[i+5]);
	GEOMVIEW_SET_FLOAT(6, localvals[i+6]);
	GEOMVIEW_SET_FLOAT(7, localvals[i+7]);
	GEOMVIEW_SET_FLOAT(8, localvals[i+8]);
	fwrite (gvfloat, sizeof (float), 9, thedisp->to_geomview);
#endif
      }
#ifndef GEOMVIEW_BINARY
    fprintf (thedisp->to_geomview, "%g %g %g\n%g %g %g\n", minmax[0],minmax[2],
	     minmax[4], minmax[1], minmax[3], minmax[5]);
#else
    GEOMVIEW_SET_FLOAT(0, minmax[0]);
    GEOMVIEW_SET_FLOAT(1, minmax[2]);
    GEOMVIEW_SET_FLOAT(2, minmax[4]);
    GEOMVIEW_SET_FLOAT(3, minmax[1]);
    GEOMVIEW_SET_FLOAT(4, minmax[3]);
    GEOMVIEW_SET_FLOAT(5, minmax[5]);
    fwrite (gvfloat, sizeof (float), 6, thedisp->to_geomview);
#endif
    for (i=0; i<total_entries/13; i++)
      {
#ifndef GEOMVIEW_BINARY
        fprintf (thedisp->to_geomview,"3 %d %d %d %g %g %g %g\n",
		 3*i, 3*i+1, 3*i+2,
		 localvals[13*i+9], localvals[13*i+10], localvals[13*i+11],
		 localvals[13*i+12]);
#else
	GEOMVIEW_SET_INT(0, 3);
	GEOMVIEW_SET_INT(1, 3*i);
	GEOMVIEW_SET_INT(2, 3*i+1);
	GEOMVIEW_SET_INT(3, 3*i+2);
	GEOMVIEW_SET_INT(4, 4);
	fwrite (gvint, sizeof (guint32), 5, thedisp->to_geomview);
	GEOMVIEW_SET_FLOAT(0, localvals[13*i+9]);
	GEOMVIEW_SET_FLOAT(1, localvals[13*i+10]);
	GEOMVIEW_SET_FLOAT(2, localvals[13*i+11]);
	GEOMVIEW_SET_FLOAT(3, localvals[13*i+12]);
	fwrite (gvfloat, sizeof (float), 4, thedisp->to_geomview);
#endif
      }
    fprintf (thedisp->to_geomview, "})\n");
    fflush (thedisp->to_geomview);
  }

  /*+ and cleans up the mess. +*/
  ierr = VecRestoreArray (localverts, &localvals); CHKERRQ (ierr);
  ierr = VecDestroy (localverts); CHKERRQ (ierr);
  ierr = VecDestroy (globalverts); CHKERRQ (ierr);

  return 0;
}
