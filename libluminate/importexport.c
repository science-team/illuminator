/***************************************
  $Header$

  This file contains functions for importing and exporting data to/from foreign
  file formats.  Thus far only .tiff images are supported.
***************************************/


#include "illuminator.h"     /* Also includes petscda.h */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) ierr = PetscPrintf (comm, "%s: " fmt, __FUNCT__, args); CHKERRQ (ierr)
#else
#define DPRINTF(fmt, args...)
#endif


#undef __FUNCT__
#define __FUNCT__ "IllImageWrite"


/*++++++++++++++++++++++++++++++++++++++
  This writes a single monochrome .tif image from a double array for IllImageSave().

  inline int IllImageWrite It returns zero or an error code.

  double *array Array of values to use as intensity values in the image,
  pointing to first red value.

  int xm Width of the array rows.

  int ym Number of rows in the array.

  int stride Number of doubles per point.

  int gxm Number of points between row starts.

  int goff Offest for green field.

  int boff Offest for blue field.

  char *basename Base file name; if the last four chars aren't ".tif" this
  appends that.

  PetscScalar minval Array value mapping to black.

  PetscScalar maxval Array value mapping to white.
  ++++++++++++++++++++++++++++++++++++++*/

static inline int IllImageWrite
(double *array, int xm,int ym,int stride,int gxm, int goff,int boff,
 char *basename, PetscScalar minval,PetscScalar maxval)
{
  /* Make the file name */
  /* Open the file */
  /* Make the header */
  /* Loop through and write the data */
  /* Close everything */
}


#undef __FUNCT__
#define __FUNCT__ "IllImageSave"

/*++++++++++++++++++++++++++++++++++++++
  This function stores one field as an image.  In 2-D, it stores the whole
  thing; in 3-D, it stores one image per layer.

  int IllImageSave It returns zero or an error code.

  MPI_COMM comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  DA theda Distributed array object controlling data saved.

  Vec X Vector whose data are actually being saved.

  char *basename Base file name.

  int field Field index.

  PetscScalar *rgbmin Field values to use for image R,G,B zero.

  PetscScalar *rgbmax Field values to use for image R,G,B max.

  int *coordrange Minimum and maximum x, y, z to store (PETSC_NULL for the
  whole thing).

  IllLayerStyle layer Layer style to use for 3-D DA.

  IllImageFormat format Image format.
  ++++++++++++++++++++++++++++++++++++++*/

int IllImageSave
(MPI_Comm comm, DA theda, Vec X, char *basename,
 int redfield,int greenfield,int bluefield,
 PetscScalar *rgbmin,PetscScalar *rgbmax, int *coordrange,
 IllLayerStyle layer, IllImageFormat format)
{

  /* Get size, if > 1 then SETERRQ only one processor for now */
  /* Get array */
  /* Get dimensions, if 2 then fall through to IllImageWrite */
  /* If layer isn't DEFAULT or Z_UP then SETERRQ only Z_UP for now */
  /* For 3-D: loop through planes, calling IllImageWrite for each */

  /* if (snprintf (filename, 999, "%s.cpu%.4d.data", basename, rank) > 999)
    SETERRQ1 (PETSC_ERR_ARG_SIZ,
	      "Base file name too long (%d chars, 960 max)",
	      strlen (basename));
  if (!(outfile = fopen (filename, "w")))
    SETERRQ4 (PETSC_ERR_FILE_OPEN,
	      "CPU %d: error %d (%s) opening output file \"%s\"",
	      rank, errno, strerror (errno), filename);

  switch (compressed & COMPRESS_INT_MASK)
    {
    case COMPRESS_INT_LONG:
      {
	guint32 *storage;
	if (!(storage = (guint32 *) malloc
	      (gridpoints * dof * sizeof (guint32))))
	  SETERRQ (PETSC_ERR_MEM, "Can't allocate data compression buffer");
	for (i=0; i<gridpoints*dof; i++)
	  storage [i] = (guint32)
	    (4294967295. * (globalarray [i] - fieldmin [i%dof]) /
	     (fieldmax [i%dof] - fieldmin [i%dof]));
	writeout = fwrite (storage, sizeof (guint32), gridpoints*dof, outfile);
	free (storage);
	break;
      }
    case COMPRESS_INT_SHORT:
      {
	guint16 *storage;
	if (!(storage = (guint16 *) malloc
	      (gridpoints * dof * sizeof (guint16))))
	  SETERRQ (PETSC_ERR_MEM, "Can't allocate data compression buffer");
	for (i=0; i<gridpoints*dof; i++)
	  storage [i] = (guint16)
	    (65535. * (globalarray [i] - fieldmin [i%dof]) /
	     (fieldmax [i%dof] - fieldmin [i%dof]));
	writeout = fwrite (storage, sizeof (guint16),  gridpoints*dof,outfile);
	free (storage);
	break;
      }
    case COMPRESS_INT_CHAR:
      {
	guint8 *storage;
	if (!(storage = (guint8 *) malloc
	      (gridpoints * dof * sizeof (guint8))))
	  SETERRQ (PETSC_ERR_MEM, "Can't allocate data compression buffer");
	for (i=0; i<gridpoints*dof; i++)
	  storage [i] = (guint8)
	    (255. * (globalarray [i] - fieldmin [i%dof]) /
	     (fieldmax [i%dof] - fieldmin [i%dof]));
	writeout = fwrite (storage, sizeof (guint8),  gridpoints*dof, outfile);
	free (storage);
	break;
      }
    default:
      writeout = fwrite (globalarray, sizeof (PetscScalar), gridpoints*dof,
			 outfile);
    }

  if (compressed & COMPRESS_GZIP_MASK)
    pclose (outfile);
  else
    fclose (outfile);

  if (writeout < gridpoints*dof)
    {
      int writerr = ferror (outfile);
      SETERRQ5 (PETSC_ERR_FILE_READ,
		"CPU %d: error %d (%s) during write, sent %d of %d items",
		rank, writerr, strerror (writerr), writeout, gridpoints*dof);
		} */

  return 0;
}
