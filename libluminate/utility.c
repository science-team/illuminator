/***************************************
  $Header$

  This file contains small utility functions for various aspects of
  visualization and storage.
  ***************************************/


#include "config.h" /* esp. for inline */
#include "illuminator.h" /* Just to make sure the interface is "right" */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) PetscPrintf (PETSC_COMM_WORLD, "%s: " fmt, __FUNCT__, args)
#else
#define DPRINTF(fmt, args...)
#endif


#undef __FUNCT__
#define __FUNCT__ "auto_scale"

/*++++++++++++++++++++++++++++++++++++++
  Determine a sensible scale for plotting, returned in *scale.  If a scalar
  field, returns the minimum and maximum; if a vector field, returns the
  minimum and maximum magnitudes (in 1-D, just plain minimum and maximum); if a
  ternary, returns the corners of the smallest equilateral triangle in ternary
  space in which all of the data fit.

  int auto_scale Returns zero or an error code.

  PetscScalar *global_array Array with values to scan for scale.

  int points Number of points in array to scan.

  int num_fields Number of fields in array.

  int display_field This display field (at least the start).

  field_plot_type fieldtype Type of field.

  int dimensions Number of dimensions.

  PetscScalar *scale Array in which to return the minimum/maximum values.
  ++++++++++++++++++++++++++++++++++++++*/

int auto_scale
(PetscScalar *global_array, int points, int num_fields, int display_field,
 field_plot_type fieldtype, int dimensions, PetscScalar *scale)
{
  int i;

  if (scale == NULL)
    SETERRQ (PETSC_ERR_ARG_BADPTR, "Invalid null pointer");

  if ((fieldtype == FIELD_VECTOR || fieldtype == FIELD_VECTOR+1) &&
      dimensions == 1)
    fieldtype = FIELD_SCALAR;

  switch (fieldtype)
    {
    case FIELD_SCALAR:
    case FIELD_SCALAR+1:
      {
	scale [0] = scale [1] = global_array [display_field];
	for (i=1; i<points; i++)
	  {
	    scale [0] = PetscMin
	      (scale [0], global_array [i*num_fields + display_field]);
	    scale [1] = PetscMax
	      (scale [1], global_array [i*num_fields + display_field]);
	  }
	return 0;
      }
    case FIELD_TERNARY:
      {
	/* Find the minimum x and y, and maximum sum, then fill in corners. */
	PetscScalar maxxpy =
	  global_array [display_field] + global_array [display_field+1];
	scale[0] = global_array [display_field];
	scale[1] = global_array [display_field+1];
	for (i=1; i<points; i++)
	  {
	    scale [0] = PetscMin
	      (scale[0], global_array [i*num_fields + display_field]);
	    scale [1] = PetscMin
	      (scale[1], global_array [i*num_fields + display_field+1]);
	    maxxpy = PetscMax
	      (maxxpy, global_array [i*num_fields + display_field] +
	       global_array [i*num_fields + display_field+1]);
	  }
	scale [2] = maxxpy - scale [1];
	scale [3] = scale [1];
	scale [4] = scale [0];
	scale [5] = maxxpy - scale [0];
	return 0;
      }
    case FIELD_TERNARY_SQUARE:
      {
	scale [0] = scale [1] = global_array [display_field];
	scale [2] = scale [3] = global_array [display_field+1];
	for (i=1; i<points; i++)
	  {
	    scale [0] = PetscMin
	      (scale [0], global_array [i*num_fields + display_field]);
	    scale [1] = PetscMax
	      (scale [1], global_array [i*num_fields + display_field]);
	    scale [2] = PetscMin
	      (scale [2], global_array [i*num_fields + display_field+1]);
	    scale [3] = PetscMax
	      (scale [3], global_array [i*num_fields + display_field+1]);
	  }
	return 0;
      }
    case FIELD_VECTOR:
    case FIELD_VECTOR+1:
      scale++;
    case FIELD_TENSOR_SHEAR:
      {
	/* Find the maximum square magnitude, then sqrt it. */
	scale[0] =
	  global_array [display_field] * global_array [display_field] +
	  global_array [display_field+1] * global_array [display_field+1] +
	  ((dimensions < 3) ? 0. :
	   global_array [display_field+2] * global_array [display_field+2]);
	for (i=1; i<points; i++)
	  {
	    scale[0] = PetscMax 
	      (scale [0], global_array [i*num_fields + display_field] *
	       global_array [i*num_fields + display_field] +
	       global_array [i*num_fields + display_field+1] *
	       global_array [i*num_fields + display_field+1] +
	       ((dimensions < 3) ? 0. :
		global_array [i*num_fields + display_field+2] *
		global_array [i*num_fields + display_field+2]));
	  }
	scale [0] = sqrt (scale [0]);
	return 0;
      }
    }
  SETERRQ (PETSC_ERR_ARG_OUTOFRANGE, "Field type not yet supported");
}


#undef __FUNCT__
#define __FUNCT__ "field_indices"

/*++++++++++++++++++++++++++++++++++++++
  Given an array of
  +latex+{\tt field\_plot\_type} enums, fill (part of) the {\tt indices}
  +html+ <tt>field_plot_type</tt> enums, fill (part of) the <tt>indices</tt>
  array with integers pointing to the true variable starts.  For example, in
  2-D with a vector field (two fields), a scalar field (one field), a symmetric
  tensor field (three fields) and a ternary composition field (two fields) for
  a total of 8 fields, this will fill the indices array with the values 0, 2,
  3, 6 and pad the rest of indices with -1, indicating when those true field
  variables start in the overall set of field variables.

  int nfields Total number of fields.

  int ds Dimensionality of the space (used to determine the number of fields
  used for a vector or tensor field).

  field_plot_type *plottypes Array of
  +latex+{\tt field\_plot\_type} enums with length {\tt nfields}.
  +html+ <tt>field_plot_type</tt> enums with length <tt>nfields</tt>.

  int *indices Array to hold the return values.
  ++++++++++++++++++++++++++++++++++++++*/

void field_indices (int nfields, int ds, field_plot_type *plottypes,
		    int *indices)
{
  int i, j;
  for (i=0, j=0; i<nfields; i++, j++)
    {
      indices [j] = i;
      if (plottypes [i] == FIELD_VECTOR ||
	  plottypes [i] == FIELD_VECTOR+1)
	i += ds-1;
      else if (plottypes [i] == FIELD_TERNARY ||
	       plottypes [i] == FIELD_TERNARY_SQUARE)
	i += 1;
      else if (plottypes [i] == FIELD_TENSOR_FULL)
	i += ds*ds-1;
      else if (plottypes [i] == FIELD_TENSOR_SYMMETRIC)
	i += ds*(ds+1)/2 -1;
      else if (plottypes [i] == FIELD_TENSOR_SHEAR)
	i += ds*(ds+1)/2 -2;
    }
  while (j<i)
    indices [j++] = -1;
}
