/***************************************
  $Header$

  This file includes functions controlling display objects, including
  constructors/destructors, I/O, and GTK+ display.
  ***************************************/


#include "illuminator.h"
#include "structures.h"
#include <stdlib.h>  /* For malloc() */
#include <stdio.h>   /* For snprintf() */
#include <gtk/gtk.h> /* For gdk_draw_rgb_[32_]image() */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) PetscPrintf (PETSC_COMM_WORLD, "%s: " fmt, __FUNCT__, args)
#else
#define DPRINTF(fmt, args...)
#endif

#define MAX_TRIANGLES 10000000 /*+ Maximum number of generated triangles per
				node. +*/


#undef __FUNCT__
#define __FUNCT__ "IDispCreate"

/*++++++++++++++++++++++++++++++++++++++
  Constructor for IDisplay object, including memory allocation for an RGB or
  zbuffer image.

  int IDispCreate Returns zero or an error code.

  IDisplay *newdisp Address in which to put the new IDisplay object.

  int width Width of the image.

  int height Height of the image.

  int rowskip Total width of the buffer including the image.

  int bpp If RGB: bytes per pixel, typically three or four; if zbuffer: depth
  of the buffer (number of layers).

  int zbuf Non-zero if creating a multi-zbuffer, zero for an RGB buffer.
  ++++++++++++++++++++++++++++++++++++++*/

int IDispCreate (IDisplay *newdisp, int width,int height,int rowskip,int bpp,
		 int zbuf)
{
  struct idisplay *thedisp;

  if (!(thedisp = (struct idisplay *) malloc (sizeof (struct idisplay))))
    SETERRQ (PETSC_ERR_MEM, "Unable to allocate memory for idisplay object");

  thedisp->to_geomview = NULL;

  if (zbuf)
    {
      thedisp->rgb = NULL;
      thedisp->rgb_width = thedisp->rgb_height = thedisp->rgb_rowskip =
	thedisp->rgb_bpp = 0;
      if (height==0 || rowskip==0 || bpp==0)
	{
	  thedisp->zbuf = NULL;
	  thedisp->zbuf_width = thedisp->zbuf_height = thedisp->zbuf_depth =
	    thedisp->zbuf_rowskip = 0;
	}
      else
	{
	  if (!(thedisp->zbuf = (zbuffer *) calloc
		(height*rowskip*bpp, sizeof (zbuffer))))
	    {
	      free (thedisp);
	      SETERRQ (PETSC_ERR_MEM,
		       "Unable to allocate memory for idisplay object");
	    }
	  DPRINTF ("Allocated %dx%dx%d zbuffer for zbuf at 0x%lx\n",
		   height, rowskip, bpp, thedisp->zbuf);
	  thedisp->zbuf_width = width;
	  thedisp->zbuf_height = height;
	  thedisp->zbuf_rowskip = rowskip;
	  thedisp->zbuf_depth = bpp;
	}
    }
  else
    {
      thedisp->zbuf = NULL;
      thedisp->zbuf_width = thedisp->zbuf_height = thedisp->zbuf_depth =
	thedisp->zbuf_rowskip = 0;
      if (height==0 | rowskip==0 || bpp==0)
	{
	  thedisp->rgb = NULL;
	  thedisp->rgb_width = thedisp->rgb_height = thedisp->rgb_rowskip =
	    thedisp->rgb_bpp = 0;
	}
      else
	{
	  if (!(thedisp->rgb = (guchar *) calloc
		(height*rowskip*bpp, sizeof (guchar))))
	    {
	      free (thedisp);
	      SETERRQ (PETSC_ERR_MEM,
		       "Unable to allocate memory for idisplay object");
	    }
	  DPRINTF ("Allocated %dx%dx%d guchar buffer for rgb at 0x%lx\n",
		   height, rowskip, bpp, thedisp->rgb);
	  thedisp->rgb_width = width;
	  thedisp->rgb_height = height;
	  thedisp->rgb_rowskip = rowskip;
	  thedisp->rgb_bpp = bpp;
	}
    }

  *newdisp = thedisp;
  return 0;  
}


#undef __FUNCT__
#define __FUNCT__ "IDispResize"

/*++++++++++++++++++++++++++++++++++++++
  IDisplay object resizer, including memory (re)allocation for an RGB or
  zbuffer image.  It either resizes what's there or creates a new buffer if
  there is not yet one.  KNOWN BUG: when complete, the old object is likely
  severely distorted by a realloc() call.

  int IDispResize Returns zero or an error code.

  IDisplay Disp IDisplay object to resize.

  int width New width of the image.

  int height New height of the image.

  int rowskip New total width of the buffer including the image.

  int bpp If RGB: bytes per pixel, typically three or four; if zbuffer: depth
  of the buffer (number of layers).

  int zbuf Non-zero to resize the multi-zbuffer, zero to resize the RGB buffer.
  ++++++++++++++++++++++++++++++++++++++*/

int IDispResize (IDisplay Disp, int width,int height,int rowskip,int bpp,
		 int zbuf)
{
  struct idisplay *thedisp = (struct idisplay *) Disp;

  /* Sanity checks */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Null display object");
  if (!(thedisp->rgb))
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "Display object has no RGB buffer to draw in");

  if (zbuf)
    {
      if (height != thedisp->zbuf_height || rowskip != thedisp->zbuf_rowskip ||
	  bpp != thedisp->zbuf_depth)
	{
	  if (height==0 || rowskip==0 || bpp==0)
	    {
	      if (thedisp->zbuf)
		free (thedisp->zbuf);
	      thedisp->zbuf = NULL;
	      thedisp->zbuf_width = thedisp->zbuf_height =
		thedisp->zbuf_rowskip = thedisp->zbuf_depth = 0;
	    }
	  else
	    {
	      int ierr;
	      DPRINTF ("Reallocated %dx%dx%d zbuffer for zbuf from 0x%lx ",
		       height, rowskip, bpp, thedisp->zbuf);
	      if (!(thedisp->zbuf = (zbuffer *) realloc
		    (thedisp->zbuf, height*rowskip*bpp * sizeof (zbuffer))))
		{
		  free (thedisp->zbuf);
		  thedisp->zbuf = NULL;
		  thedisp->zbuf_width = thedisp->zbuf_height =
		    thedisp->zbuf_rowskip = thedisp->zbuf_depth = 0;
		  SETERRQ (PETSC_ERR_MEM,
			   "Unable to allocate memory for IDisplay object");
		}
#ifdef DEBUG
	      ierr=PetscPrintf (PETSC_COMM_WORLD, "to 0x%lx\n", thedisp->zbuf);
	      CHKERRQ (ierr);
#endif
	      thedisp->zbuf_width = width;
	      thedisp->zbuf_height = height;
	      thedisp->zbuf_rowskip = rowskip;
	      thedisp->zbuf_depth = bpp;
	    }
	}
    }
  else if (height != thedisp->rgb_height || rowskip != thedisp->rgb_rowskip ||
	   bpp != thedisp->rgb_bpp)
    {
      if (height==0 || rowskip==0 || bpp==0)
	{
	  if (thedisp->rgb)
	    free (thedisp->rgb);
	  thedisp->rgb = NULL;
	  thedisp->rgb_width = thedisp->rgb_height = thedisp->rgb_rowskip =
	    thedisp->rgb_bpp = 0;	  
	}
      else
	{
	  int ierr;
	  DPRINTF ("Reallocated %dx%dx%d guchar buffer for rgb from 0x%lx ",
		   height, rowskip, bpp, thedisp->rgb);
	  if (!(thedisp->rgb = (guchar *) realloc
		(thedisp->rgb, height*rowskip*bpp * sizeof (guchar))))
	    {
	      free (thedisp->rgb);
	      thedisp->rgb = NULL;
	      thedisp->rgb_width = thedisp->rgb_height = thedisp->rgb_rowskip =
		thedisp->rgb_bpp = 0;
	      SETERRQ (PETSC_ERR_MEM,
		       "Unable to allocate memory for idisplay object");
	    }
#ifdef DEBUG
	  ierr=PetscPrintf (PETSC_COMM_WORLD, "to 0x%lx\n", thedisp->rgb);
	  CHKERRQ (ierr);
#endif
	  thedisp->rgb_width = width;
	  thedisp->rgb_height = height;
	  thedisp->rgb_rowskip = rowskip;
	  thedisp->rgb_bpp = bpp;
	}
    }

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IDispDestroy"

/*++++++++++++++++++++++++++++++++++++++
  Destructor for IDisplay data object.

  int IDispDestroy Returns zero or an error code.

  IDisplay Disp IDisplay object to destroy.
  ++++++++++++++++++++++++++++++++++++++*/

int IDispDestroy (IDisplay Disp)
{
  struct idisplay *thedisp = (struct idisplay *) Disp;

  /* Sanity check */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Trying to destroy null display object");

  if (thedisp->to_geomview)
    fclose (thedisp->to_geomview);
  if (thedisp->rgb)
    free (thedisp->rgb);
  if (thedisp->zbuf)
    free (thedisp->zbuf);
  free (thedisp);
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IDispFill"

/*++++++++++++++++++++++++++++++++++++++
  Fill the RGB image buffer in a IDisplay data object with a solid color.

  int IDispFill Returns zero or an error code.

  IDisplay Disp IDisplay object to fill.

  guchar *color Color to fill.
  ++++++++++++++++++++++++++++++++++++++*/

int IDispFill (IDisplay Disp, guchar *color)
{
  struct idisplay *thedisp = (struct idisplay *) Disp;
  int i,j,k;

  /* Sanity checks */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Null display object");
  if (!(thedisp->rgb))
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "Display object has no RGB buffer to draw in");

  for (i=0; i<thedisp->rgb_height; i++)
    for (j=0; j<thedisp->rgb_width; j++)
      for (k=0; k<thedisp->rgb_bpp; k++)
	thedisp->rgb [(i*thedisp->rgb_rowskip + j)* thedisp->rgb_bpp + k] =
	  color [k];

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IDispDrawGdk"

/*++++++++++++++++++++++++++++++++++++++
  Draw the IDisplay RGB image buffer into a Gtk drawing area.

  int IDispDrawGdk Returns zero or an error code.

  IDisplay Disp IDisplay object to draw.

  GtkWidget *dataview Gtk drawing area widget in which to draw.

  GdkRgbDither dith Dithering method, usually GDK_RGB_DITHER_MAX.
  ++++++++++++++++++++++++++++++++++++++*/

int IDispDrawGdk (IDisplay Disp, GtkWidget *dataview, GdkRgbDither dith)
{
  struct idisplay *thedisp = (struct idisplay *) Disp;

  /* Sanity checks */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Null display object");
  if (!(thedisp->rgb))
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "Display object has no RGB buffer to draw in");

  if (thedisp->rgb_bpp == 3)
    gdk_draw_rgb_image (dataview->window,
			dataview->style->fg_gc[GTK_STATE_NORMAL],
			0, 0, thedisp->rgb_width, thedisp->rgb_height,
			dith, thedisp->rgb, thedisp->rgb_rowskip*3);
  else if (thedisp->rgb_bpp == 4)
    gdk_draw_rgb_32_image (dataview->window,
			   dataview->style->fg_gc[GTK_STATE_NORMAL],
			   0, 0, thedisp->rgb_width, thedisp->rgb_height,
			   dith, thedisp->rgb, thedisp->rgb_rowskip*4);
  else
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "Must be 3 or 4 bytes per pixel for Gdk drawing");

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IDispWritePPM"

/*++++++++++++++++++++++++++++++++++++++
  Writes the RGB part of an IDisplay object, or a snapshot of the Geomview pipe
  World camera, to a file in PPM format.

  int IDispWritePPM Returns zero or an error code.

  IDisplay Disp IDisplay object whose RGB part will be written.

  char *filename File name to write the PPM to.
  ++++++++++++++++++++++++++++++++++++++*/

int IDispWritePPM (IDisplay Disp, char *filename)
{
  struct idisplay *thedisp = (struct idisplay *) Disp;
  FILE *outppm;
  int i,j;

  /* Sanity checks */
  if (!thedisp)
    SETERRQ (PETSC_ERR_ARG_CORRUPT, "Null display object");
  if (!(thedisp->to_geomview) && !(thedisp->rgb))
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,"Display object has no data to draw in");
  if (!(thedisp->to_geomview) && thedisp->rgb_bpp<3)
    SETERRQ (PETSC_ERR_ARG_WRONGSTATE,
	     "No PPM support for fewer than three bytes/pixel");

  if (thedisp->to_geomview)
    {
      fprintf (thedisp->to_geomview, "(snapshot c0 %s)\n", filename);
      return 0;
    }

  if (!(outppm = fopen (filename, "w")))
    SETERRQ1 (PETSC_ERR_FILE_OPEN,"Error opening PPM output file %s",filename);
  fprintf (outppm, "P6\n%d %d\n255\n", thedisp->rgb_width,thedisp->rgb_height);
  if (thedisp->rgb_bpp==3)
    {
      if (thedisp->rgb_width == thedisp->rgb_rowskip)
	fwrite (thedisp->rgb, sizeof (guchar),
		thedisp->rgb_width*thedisp->rgb_height*3, outppm);
      else
	for (i=0; i<thedisp->rgb_height; i++)
	  fwrite (thedisp->rgb + i*thedisp->rgb_rowskip*thedisp->rgb_bpp,
		  sizeof(guchar), thedisp->rgb_width*thedisp->rgb_bpp, outppm);
    }
  else
    {
      for (i=0; i<thedisp->rgb_height; i++)
	for (j=0; j<thedisp->rgb_width; j++)
	  fwrite (thedisp->rgb + (i*thedisp->rgb_rowskip+ j)* thedisp->rgb_bpp,
		  sizeof (guchar), thedisp->rgb_bpp, outppm);
    }

  fclose (outppm);
  return 0;
}
