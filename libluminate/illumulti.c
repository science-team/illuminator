/***************************************
  $Header$

  This file contains the functions
  +latex+{\tt IlluMultiSave()}, {\tt IlluMultiLoad()} and {\tt IlluMultiRead()}
  +html+ <tt>IlluMultiSave()</tt>, <tt>IlluMultiLoad()</tt> and
  +html+ <tt>IlluMultiRead()</tt>
  designed to handle distributed storage and retrieval of data on local drives
  of machines in a Beowulf cluster.  This should allow rapid loading of
  timestep data for "playback" from what is essentially a giant RAID-0 array of
  distributed disks, at enormously higher speeds than via NFS from a hard drive
  or RAID array on the head node.  The danger of course is that if one node's
  disk goes down, you don't have a valid data set any more, but that's the
  nature of RAID-0, right?
  +latex+\par
  +html+ <p>
  The filenames saved are:
  +latex+\begin{itemize} \item {\tt $<$basename$>$.cpu\#\#\#\#.meta}, where
  +latex+{\tt \#\#\#\#}
  +html+ <ul><li><tt>&lt;basename&gt.cpu####.meta</tt>, where <tt>####</tt>
  is replaced by the CPU number (more than four digits if there are more than
  9999 CPUs :-), which has the metadata for the whole thing in XML format
  (written by
  +latex+{\tt GNOME libxml}),
  +html+ <tt>GNOME libxml</tt>),
  as described in the notes on the
  +latex+{\tt IlluMultiStoreXML()} function.
  +html+ <tt>IlluMultiStoreXML()</tt> function.
  +latex+\item {\tt $<$basename$>$.cpu\#\#\#\#.data}
  +html+ </li><li><tt>&lt;basename&gt.cpu####.data</tt>
  which is simply a stream of the raw data, optionally compressed by
  +latex+{\tt gzip}. \end{itemize}
  +html+ <tt>gzip</tt>.</li></ul>
  If one were saving timesteps, one might include a timestep number in the
  basename, and also timestep and simulation time in the metadata.  The
  metadata can also hold simulation parameters, etc.
  +latex+\par
  +html+ <p>
  This supports 1-D, 2-D and 3-D distributed arrays.  As an extra feature, you
  can load a multi-CPU distributed array scattered over lots of files into a
  single CPU, to facilitate certain modes of data visualization.
***************************************/


#include "illuminator.h"     /* Also includes petscda.h */
#include <glib.h>            /* for guint*, GUINT*_SWAP_LE_BE */
#include <petscblaslapack.h> /* for BLAScopy_() */
#include <libxml/tree.h>     /* To build the XML tree to store */
#include <libxml/parser.h>   /* XML parsing */
#include <stdio.h>           /* FILE, etc. */
#include <errno.h>           /* errno */
#include <string.h>          /* strncpy(), etc. */
#include <stdlib.h>          /* malloc() and free() */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) ierr = PetscPrintf (comm, "%s: " fmt, __FUNCT__, args); CHKERRQ (ierr)
#else
#define DPRINTF(fmt, args...)
#endif


#undef __FUNCT__
#define __FUNCT__ "IlluMultiParseXML"

/*++++++++++++++++++++++++++++++++++++++
  This function reads in the XML metadata document and returns the various
  parameter values in the addresses pointed to by the arguments.  It is called
  by IlluMultiLoad() and IlluMultiRead().

  int IlluMultiParseXML It returns zero or an error code.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  char *basename Base file name.

  int rank CPU number to read data for.

  int *compressed Data compression: if zero then no compression (fastest), 1-9
  then gzip compression level, 10-15 then gzip --best.  If 16-31 then save
  guint32s representing relative values between min and max for each field,
  compressed according to this value minus 16.  Likewise for 32-47 and
  guint16s, and 48-63 for guint8s.  Yes, these alternative formats lose
  information and can't be used for accurate checkpointing, but they should
  retain enough data for visualization (except perhaps for the guint8s, which
  are possibly acceptable for vectors but likely not contours).

  int *wrongendian Tells whether the data are stored in the opposite endian
  format from this platform, and thus must be switched when the data are
  streamed in.

  int *dim Dimensionality of the space.

  int *px Number of processors in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  int *py Number of processors in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  int *pz Number of processors in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  int *nx Number of grid points over the entire array in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  int *ny Number of grid points over the entire array in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  int *nz Number of grid points over the entire array in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  PetscScalar *wx Physical overall width in the
  +latex+$x$-direction, {\tt PETSC\_NULL} if not needed.
  +html+ <i>x</i>-direction, <tt>PETSC_NULL</tt> if not needed.

  PetscScalar *wy Physical overall width in the
  +latex+$y$-direction, {\tt PETSC\_NULL} if not needed.
  +html+ <i>y</i>-direction, <tt>PETSC_NULL</tt> if not needed.

  PetscScalar *wz Physical overall width in the
  +latex+$z$-direction, {\tt PETSC\_NULL} if not needed.
  +html+ <i>z</i>-direction, <tt>PETSC_NULL</tt> if not needed.

  int *xm Number of grid points over the local part of the array in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  int *ym Number of grid points over the local part of the array in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  int *zm Number of grid points over the local part of the array in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  int *dof Degrees of freedom at each node, a.k.a. number of field variables.

  int *sw Stencil width.

  DAStencilType *st Stencil type, given by the
  +latex+{\tt PETSc enum} values.
  +html+ <tt>PETSc enum</tt> values.

  DAPeriodicType *wrap Periodic type, given by the
  +latex+{\tt PETSc enum} values.
  +html+ <tt>PETSc enum</tt> values.

  char ***fieldnames Names of the field variables.

  field_plot_type **fieldtypes Data (plot) types for field variables,
  +latex+{\tt PETSC\_NULL} if not needed.
  +html+ <tt>PETSC_NULL</tt> if not needed.

  PetscScalar **fieldmin Minimum value of each field variable.

  PetscScalar **fieldmax Maximum value of each field variable.

  int *usermetacount Number of user metadata parameters.

  char ***usermetanames User metadata parameter names.

  char ***usermetadata User metadata parameter strings.
  ++++++++++++++++++++++++++++++++++++++*/

static int IlluMultiParseXML
(MPI_Comm comm, char *basename, int rank, int *compressed, int *wrongendian,
 int *dim, int *px,int *py,int *pz, int *nx,int *ny,int *nz,
 PetscScalar *wx,PetscScalar *wy,PetscScalar *wz, int *xm,int *ym,int *zm,
 int *dof, int *sw, DAStencilType *st, DAPeriodicType *wrap,
 char ***fieldnames, field_plot_type **fieldtypes, PetscScalar **fieldmin,
 PetscScalar **fieldmax,
 int *usermetacount, char ***usermetanames, char ***usermetadata)
{
  xmlDocPtr thedoc;
  xmlNodePtr thenode;
  char filename [1000];
  xmlChar *buffer, *version;
  int field=0, ierr;

  if (snprintf (filename, 999, "%s.cpu%.4d.meta", basename, rank) > 999)
    SETERRQ1 (PETSC_ERR_ARG_SIZ, "Base file name too long (%d chars, 960 max)",
	      strlen (basename));

  DPRINTF ("Parsing XML file %s\n", filename);
  thedoc = xmlParseFile (filename);
  if (thedoc == NULL)
    SETERRQ (PETSC_ERR_FILE_OPEN, "xmlParseFile returned NULL\n");

  version = xmlGetProp (thedoc -> children, BAD_CAST "version");
  if (strncmp ((char *) version, "0.1", 3) &&
      strncmp ((char *) version, "0.2", 3))
    {
      free (version);
      SETERRQ (PETSC_ERR_FILE_UNEXPECTED,
	       "Version is neither 0.1 nor 0.2, can't parse\n");
    }

  *wrongendian = 0;
  buffer = xmlGetProp (thedoc -> children, BAD_CAST "endian");
#ifdef WORDS_BIGENDIAN
  if (strncasecmp ((char *) buffer, "big", 3))
    *wrongendian = 1;
#else
  if (!strncasecmp ((char *) buffer, "big", 3))
    *wrongendian = 1;
#endif
  free (buffer);

  buffer = xmlGetProp (thedoc -> children, BAD_CAST "compression");
  /* Have to handle all of the various compressed string values */
  /* TODO: make this deal with "float" and "complex" */
  if (buffer[0] == 'l' || buffer[0] == 'L')
    {
      if (buffer[4] == 'n' || buffer[4] == 'N')
	*compressed = COMPRESS_INT_LONG | COMPRESS_GZIP_NONE;
      else if (buffer[4] >= '1' && buffer[4] <= '9')
	{
	  sscanf ((char *) buffer+4, "%d", compressed);
	  *compressed |= COMPRESS_INT_LONG;
	}
      else if (buffer[4] == 'b' || buffer[4] == 'B')
	*compressed = COMPRESS_INT_LONG | COMPRESS_GZIP_BEST;
      else
	SETERRQ1 (PETSC_ERR_ARG_OUTOFRANGE,
		 "Unrecognized compression type %s in .meta file", buffer);
    }
  else if (buffer[0] == 's' || buffer[0] == 'S')
    {
      if (buffer[5] == 'n' || buffer[5] == 'N')
	*compressed = COMPRESS_INT_SHORT | COMPRESS_GZIP_NONE;
      else if (buffer[5] >= '1' && buffer[5] <= '9')
	{
	  sscanf ((char *) buffer+5, "%d", compressed);
	  *compressed |= COMPRESS_INT_SHORT;
	}
      else if (buffer[5] == 'b' || buffer[5] == 'B')
	*compressed = COMPRESS_INT_SHORT | COMPRESS_GZIP_BEST;
      else
	SETERRQ1 (PETSC_ERR_ARG_OUTOFRANGE,
		 "Unrecognized compression type %s in .meta file", buffer);
    }
  else if (buffer[0] == 'c' || buffer[0] == 'C')
    {
      if (buffer[4] == 'n' || buffer[4] == 'N')
	*compressed = COMPRESS_INT_CHAR | COMPRESS_GZIP_NONE;
      else if (buffer[4] >= '1' && buffer[4] <= '9')
	{
	  sscanf ((char *) buffer+4, "%d", compressed);
	  *compressed |= COMPRESS_INT_CHAR;
	}
      else if (buffer[4] == 'b' || buffer[4] == 'B')
	*compressed = COMPRESS_INT_CHAR | COMPRESS_GZIP_BEST;
      else
	SETERRQ1 (PETSC_ERR_ARG_OUTOFRANGE,
		 "Unrecognized compression type %s in .meta file", buffer);
    }
  else
    {
      if (buffer[0] == 'n' || buffer[0] == 'N')
	*compressed = COMPRESS_INT_NONE | COMPRESS_GZIP_NONE;
      else if (buffer[0] >= '1' && buffer[0] <= '9')
	{
	  sscanf ((char *) buffer, "%d", compressed);
	  *compressed |= COMPRESS_INT_NONE;
	}
      else if (buffer[0] == 'b' || buffer[0] == 'B')
	*compressed = COMPRESS_INT_NONE | COMPRESS_GZIP_BEST;
      else
	SETERRQ1 (PETSC_ERR_ARG_OUTOFRANGE,
		 "Unrecognized compression type %s in .meta file", buffer);
    }
  free (buffer);
  DPRINTF ("Root node: version %s, %s endian, compression %d|%d\n",
	   version, *wrongendian ? "switched" : "native",
	   *compressed & COMPRESS_INT_MASK, *compressed & COMPRESS_GZIP_MASK);

  *fieldnames = PETSC_NULL;
  if (fieldtypes != PETSC_NULL)
    *fieldtypes = PETSC_NULL;
  *fieldmin = *fieldmax = PETSC_NULL;
  *usermetacount = 0;
  *usermetanames = *usermetadata = PETSC_NULL;

  /* The main parsing loop; because xmlStringGetNodeList() doesn't work. */
  for (thenode = thedoc->children->xmlChildrenNode;
       thenode;
       thenode = thenode->next)
    {
      DPRINTF ("Looping through nodes, this is %s, fieldnames 0x%lx\n",
	       thenode->name, *fieldnames);
      if (!strncasecmp ((char *) thenode->name, "GlobalCPUs", 10))
	{
	  buffer = xmlGetProp (thenode, BAD_CAST "dimensions");
	  sscanf ((char *) buffer, "%d", dim);
	  free (buffer);

	  buffer = xmlGetProp (thenode, BAD_CAST "xwidth");
	  sscanf ((char *) buffer, "%d", px);
	  free (buffer);

	  if (*dim > 1)
	    {
	      buffer = xmlGetProp (thenode, BAD_CAST "ywidth");
	      sscanf ((char *) buffer, "%d", py);
	      free (buffer);

	      if (*dim > 2)
		{
		  buffer = xmlGetProp (thenode, BAD_CAST "zwidth");
		  sscanf ((char *) buffer, "%d", pz);
		  free (buffer);
		}
	      else
		*pz = 1;
	    }
	  else
	    *py = 1;
	}

      else if (!strncasecmp ((char *) thenode->name, "GlobalSize", 10))
	{
	  buffer = xmlGetProp (thenode, BAD_CAST "xwidth");
	  sscanf ((char *) buffer, "%d", nx);
	  free (buffer);
	  /*+ For GlobalSize, since there's no *size attribute (for an 0.1
	    version document), assume 1. +*/
	  buffer = xmlGetProp (thenode, BAD_CAST "xsize");
	  if (wx != PETSC_NULL)
	    {
	      if (buffer)
#ifdef PETSC_USE_SINGLE
		sscanf ((char *) buffer, "%f", wx);
#else
		sscanf ((char *) buffer, "%lf", wx);
#endif
	      else
		*wx = 1.;
	    }
	  if (buffer)
	    free (buffer);

	  if (*dim > 1)
	    {
	      buffer = xmlGetProp (thenode, BAD_CAST "ywidth");
	      sscanf ((char *) buffer, "%d", ny);
	      free (buffer);
	      buffer = xmlGetProp (thenode, BAD_CAST "ysize");
	      if (wy != PETSC_NULL)
		{
		  if (buffer)
#ifdef PETSC_USE_SINGLE
		    sscanf ((char *) buffer, "%f", wy);
#else
		    sscanf ((char *) buffer, "%lf", wy);
#endif
		  else
		    *wy = 1.;
		}
	      if (buffer)
		free (buffer);

	      if (*dim > 2)
		{
		  buffer = xmlGetProp (thenode, BAD_CAST "zwidth");
		  sscanf ((char *) buffer, "%d", nz);
		  free (buffer);
		  buffer = xmlGetProp (thenode, BAD_CAST "zsize");
		  if (wz != PETSC_NULL)
		    {
		      if (buffer)
#ifdef PETSC_USE_SINGLE
			sscanf ((char *) buffer, "%f", wz);
#else
			sscanf ((char *) buffer, "%lf", wz);
#endif
		      else
			*wz = 1.;
		    }
		  if (buffer)
		    free (buffer);
		}
	      else
		*nz = 1;
	    }
	  else
	    *ny = *nz = 1;

	  buffer = xmlGetProp (thenode, BAD_CAST "fields");
	  sscanf ((char *) buffer, "%d", dof);
	  free (buffer);

	  if (*dof > field)
	    {
	      if (!(*fieldnames = (char **) realloc
		    (*fieldnames, *dof * sizeof (char *))))
		SETERRQ (PETSC_ERR_MEM, "Can't allocate field names");
	      if (fieldtypes != PETSC_NULL)
		if (!(*fieldtypes = (field_plot_type *) realloc
		      (*fieldtypes, *dof * sizeof(field_plot_type))))
		SETERRQ (PETSC_ERR_MEM, "Can't allocate field types");
	      if (!(*fieldmin = (PetscScalar *) realloc
		    (*fieldmin, *dof * sizeof(PetscScalar))))
		SETERRQ (PETSC_ERR_MEM, "Can't allocate field params");
	      if (!(*fieldmax = (PetscScalar *) realloc
		    (*fieldmax, *dof * sizeof(PetscScalar))))
		SETERRQ (PETSC_ERR_MEM, "Can't allocate field params");
	    }
	}

      else if (!strncasecmp ((char *) thenode->name, "LocalSize", 9))
	{
	  buffer = xmlGetProp (thenode, BAD_CAST "xwidth");
	  sscanf ((char *) buffer, "%d", xm);
	  free (buffer);

	  if (*dim > 1)
	    {
	      buffer = xmlGetProp (thenode, BAD_CAST "ywidth");
	      sscanf ((char *) buffer, "%d", ym);
	      free (buffer);

	      if (*dim > 2)
		{
		  buffer = xmlGetProp (thenode, BAD_CAST "zwidth");
		  sscanf ((char *) buffer, "%d", zm);
		  free (buffer);
		}
	      else
		*zm = 1;
	    }
	  else
	    *ym = 1;
	}

      else if (!strncasecmp ((char *) thenode->name, "Stencil", 7))
	{
	  buffer = xmlGetProp (thenode, BAD_CAST "width");
	  sscanf ((char *) buffer, "%d", sw);
	  free (buffer);

	  buffer = xmlGetProp (thenode, BAD_CAST "type");
	  sscanf ((char *) buffer, "%d", st);
	  free (buffer);

	  buffer = xmlGetProp (thenode, BAD_CAST "periodic");
	  sscanf ((char *) buffer, "%d", wrap);
	  free (buffer);
	}

      else if (!strncasecmp ((char *) thenode->name, "Field", 5))
	{
	  if (!*fieldnames || !*fieldmax || !*fieldmin)
	    {
	      printf ("Warning: reading a Field, but the number of them is unknown!\n");
	    }

	  if (field >= *dof)
	    {
	      printf ("Warning: more <Field> tags than declared degrees of freedom.\nThis may be because tags are out of order.\n");
	      *fieldnames = (char **) realloc
		(*fieldnames, (field+1) * sizeof (char *));
	      if (fieldtypes != PETSC_NULL)
		*fieldtypes = (field_plot_type *) realloc
		  (*fieldtypes, (field+1) * sizeof (field_plot_type));
	      *fieldmin = (PetscScalar *) realloc
		(*fieldmin, (field+1) * sizeof (PetscScalar));
	      *fieldmax = (PetscScalar *) realloc
		(*fieldmax, (field+1) * sizeof (PetscScalar));
	    }

	  (*fieldnames) [field] =
	    (char *) xmlGetProp (thenode, BAD_CAST "name");
	  /*+ If the type attribute is missing from the Field node (as it is in
	    version 0.1 documents), assume
	    +latex+{\tt FIELD\_SCALAR}.
	    +html+ <tt>FIELD_SCALAR</tt>. +*/
	  buffer = xmlGetProp (thenode, BAD_CAST "type");
	  if (fieldtypes)
	    {
	      if (buffer)
		sscanf ((char *) buffer, "0x%x", *fieldtypes + field);
	      else
		(*fieldtypes) [field] = FIELD_SCALAR;
	    }
	  if (buffer)
	    free(buffer);
	  buffer = xmlGetProp (thenode, BAD_CAST "min");
#ifdef PETSC_USE_SINGLE
	  sscanf ((char *) buffer, "%f", *fieldmin + field);
#else
	  sscanf ((char *) buffer, "%lf", *fieldmin + field);
#endif
	  free (buffer);
	  buffer = xmlGetProp (thenode, BAD_CAST "max");
#ifdef PETSC_USE_SINGLE
	  sscanf ((char *) buffer, "%f", *fieldmax + field);
#else
	  sscanf ((char *) buffer, "%lf", *fieldmax + field);
#endif
	  free (buffer);
	  field++;
	}

      else if (!strncasecmp ((char *) thenode->name, "User", 4))
	{
	  (*usermetacount)++;
	  (*usermetanames) = (char **) realloc
	    (*usermetanames, (*usermetacount) * sizeof (char *));
	  (*usermetadata) = (char **) realloc
	    (*usermetadata, (*usermetacount) * sizeof (char *));
	  (*usermetanames) [(*usermetacount)-1] =
	    (char *) xmlGetProp (thenode, BAD_CAST "name");
	  (*usermetadata) [(*usermetacount)-1] =
	    (char *) xmlGetProp (thenode, BAD_CAST "value");
	}
    }

  /* This is another way to do things, which would be a whole lot easier, if it
     worked... (See Debian's libxml bug list.)
  thenode = xmlStringGetNodeList (thedoc, "GlobalCPUs");
  printf ("thenode = 0x%lx\n", thenode);
  if (!thenode)
    return 1;
  thenode = xmlStringGetNodeList (thedoc, "GlobalSize");
  if (!thenode)
    return 1;
  thenode = xmlStringGetNodeList (thedoc, "LocalSize");
  if (!thenode)
    return 1;
  thenode = xmlStringGetNodeList (thedoc, "Stencil");
  if (!thenode)
    return 1;

  i = 0;
  thenode = xmlStringGetNodeList (thedoc, "Field");
  if (!thenode)
    return 1;
  while (thenode)
    {
      i++;
      thenode = thenode -> next;
    }

  thenode = xmlStringGetNodeList (thedoc, "User");
  while (thenode)
    {
      thenode = thenode -> next;
      } */

  free (version);
  xmlFreeDoc (thedoc);

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IlluMultiParseData"

/*++++++++++++++++++++++++++++++++++++++
  This function reads in the data stored by IlluMultiStoreData(), complete with
  int/gzip compression.

  int IlluMultiParseData It returns zero or an error code.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  PetscScalar *globalarray Array into which to load the (local) data.

  char *basename Base file name.

  int rank CPU number to read data for.

  int compressed Data compression: if zero then no compression (fastest), 1-9
  then gzip compression level, 10-15 then gzip --best.  If 16-31 then save
  guint32s representing relative values between min and max for each field,
  compressed according to this value minus 16.  Likewise for 32-47 and
  guint16s, and 48-63 for guint8s.  Yes, these alternative formats lose
  information and can't be used for accurate checkpointing, but they should
  retain enough data for visualization (except perhaps for the guint8s, which
  are possibly acceptable for vectors but likely not contours).

  int gridpoints Number of gridpoints to read data for.

  int dof Degrees of freedom at each node, a.k.a. number of field variables.

  int wrongendian Tells whether the data are stored in the opposite endian
  format from this platform, and thus must be switched when the data are
  streamed in.

  PetscScalar *fieldmin Minimum value of each field variable.

  PetscScalar *fieldmax Maximum value of each field variable.
  ++++++++++++++++++++++++++++++++++++++*/

static int IlluMultiParseData
(MPI_Comm comm, PetscScalar *globalarray, char *basename, int rank,
 int compressed, int gridpoints, int dof, int wrongendian,
 PetscScalar *fieldmin, PetscScalar *fieldmax)
{
  int i;
  char filename [1000];
  FILE *infile;
  size_t readoutput;

  if (compressed & COMPRESS_GZIP_MASK)
    {
      if (snprintf (filename, 999, "gunzip -c < %s.cpu%.4d.data",
		    basename, rank) > 999)
	SETERRQ1 (PETSC_ERR_ARG_SIZ,
		  "Base file name too long (%d chars, 960 max)",
		  strlen (basename));
      if (!(infile = popen (filename, "r")))
	SETERRQ4 (PETSC_ERR_FILE_OPEN,
		  "CPU %d: error %d (%s) opening input pipe \"%s\"",
		  rank, errno, strerror (errno), filename);
    }
  else
    {
      if (snprintf (filename, 999, "%s.cpu%.4d.data", basename, rank) > 999)
	SETERRQ1 (PETSC_ERR_ARG_SIZ,
		  "Base file name too long (%d chars, 960 max)",
		  strlen (basename));
      if (!(infile = fopen (filename, "r")))
	SETERRQ4 (PETSC_ERR_FILE_OPEN,
		  "CPU %d: error %d (%s) opening input file \"%s\"",
		  rank, errno, strerror (errno), filename);
    }

  /* Read and store the data */
  /* TODO: make this deal with float and complex */
  switch (compressed & COMPRESS_INT_MASK)
    {
      /* Yes, this way of doing it is a bit redundant, but it seems better than
	 the multitude of subconditionals which would be required if I did it
	 the other way. */
    case COMPRESS_INT_LONG:
      {
	guint32 *storage;
	if (!(storage = (guint32 *) malloc
	      (gridpoints * dof * sizeof (guint32))))
	  SETERRQ (PETSC_ERR_MEM,
		   "Can't allocate data decompression buffer");
	readoutput = fread (storage, sizeof (guint32), gridpoints*dof, infile);

	if (wrongendian)
	  {
	    for (i=0; i<gridpoints*dof; i++)
	      storage[i] = GUINT32_SWAP_LE_BE_CONSTANT (storage[i]);
	  }
	for (i=0; i<gridpoints*dof; i++)
	  globalarray[i] = fieldmin [i%dof] +
	    ((fieldmax [i%dof] - fieldmin [i%dof]) * storage[i]) / 4294967295.;
	free (storage);
	break;
      }
    case COMPRESS_INT_SHORT:
      {
	guint16 *storage;
	if (!(storage = (guint16 *) malloc
	      (gridpoints * dof * sizeof (guint16))))
	  SETERRQ (PETSC_ERR_MEM,
		   "Can't allocate data decompression buffer");
	readoutput = fread (storage, sizeof (guint16), gridpoints*dof, infile);

	if (wrongendian)
	  {
	    for (i=0; i<gridpoints*dof; i++)
	      storage[i] = GUINT16_SWAP_LE_BE_CONSTANT (storage[i]);
	  }
	for (i=0; i<gridpoints*dof; i++)
	  globalarray[i] = fieldmin [i%dof] +
	    ((fieldmax [i%dof] - fieldmin [i%dof]) * storage[i]) / 65535.;
	free (storage);
	break;
      }
    case COMPRESS_INT_CHAR:
      {
	guint8 *storage;
	if (!(storage = (guint8 *) malloc
	      (gridpoints * dof * sizeof (guint8))))
	  SETERRQ (PETSC_ERR_MEM,
		   "Can't allocate data decompression buffer");
	readoutput = fread (storage, sizeof (guint8), gridpoints*dof, infile);

	for (i=0; i<gridpoints*dof; i++)
	  globalarray[i] = fieldmin [i%dof] +
	    ((fieldmax [i%dof] - fieldmin [i%dof]) * storage[i]) / 255.;
	free (storage);
	break;
      }
    default:
      {
	/* TODO: check for different size data, complex */
	readoutput = fread (globalarray, sizeof (PetscScalar), gridpoints*dof,
			    infile);

	if (wrongendian)
	  {
	    if (sizeof (PetscScalar) == 8)
	      {
		for (i=0; i<gridpoints*dof; i++)
		  globalarray[i]= GUINT64_SWAP_LE_BE_CONSTANT (globalarray[i]);
	      }
	    else if (sizeof (PetscScalar) == 4)
	      {
		for (i=0; i<gridpoints*dof; i++)
		  globalarray[i]= GUINT32_SWAP_LE_BE_CONSTANT (globalarray[i]);
	      }
	    else
	      SETERRQ1 (PETSC_ERR_ARG_UNKNOWN_TYPE,
			"Unknown PetscScalar size %d (4-8 supported)",
			sizeof (PetscScalar));
	  }

      }
    }

  if (compressed & COMPRESS_GZIP_MASK)
    pclose (infile);
  else
    fclose (infile);

  if (readoutput < gridpoints*dof)
    {
      int readerr = ferror (infile);
      SETERRQ5 (PETSC_ERR_FILE_READ,
		"CPU %d: error %d (%s) during read, got %d of %d items",
		rank, readerr, strerror (readerr), readoutput, gridpoints*dof);
    }

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IlluMultiStoreXML"

/*++++++++++++++++++++++++++++++++++++++
  This function opens, stores and closes the XML metadata file for IlluMulti
  format storage.  It is called by IlluMultiSave().

  int IlluMultiStoreXML It returns zero or an error code.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  char *basename Base file name.

  int rank CPU number to store data for.

  int compressed Data compression: if zero then no compression (fastest), 1-9
  then gzip compression level, 10-15 then gzip --best.  If 16-31 then save
  guint32s representing relative values between min and max for each field,
  compressed according to this value minus 16.  Likewise for 32-47 and
  guint16s, and 48-63 for guint8s.  Yes, these alternative formats lose
  information and can't be used for accurate checkpointing, but they should
  retain enough data for visualization (except perhaps for the guint8s, which
  are possibly acceptable for vectors but certainly not contours).

  int dim Dimensionality of the space.

  int px Number of processors in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  int py Number of processors in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  int pz Number of processors in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  int nx Number of grid points over the entire array in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  int ny Number of grid points over the entire array in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  int nz Number of grid points over the entire array in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  PetscScalar wx Physical overall width in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  PetscScalar wy Physical overall width in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  PetscScalar wz Physical overall width in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  int xm Number of grid points over the local part of the array in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  int ym Number of grid points over the local part of the array in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  int zm Number of grid points over the local part of the array in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  int dof Degrees of freedom at each node, a.k.a. number of field variables.

  int sw Stencil width.

  int st Stencil type, given by the
  +latex+{\tt PETSc enum} values.
  +html+ <tt>PETSc enum</tt> values.

  int wrap Periodic type, given by the
  +latex+{\tt PETSc enum} values.
  +html+ <tt>PETSc enum</tt> values.

  char **fieldnames Names of the field variables.

  field_plot_type *fieldtypes Data (plot) types for field variables.

  PetscReal *fieldmin Minimum value of each field variable.

  PetscReal *fieldmax Maximum value of each field variable.

  int usermetacount Number of user metadata parameters.

  char **usermetanames User metadata parameter names.

  char **usermetadata User metadata parameter strings.
  ++++++++++++++++++++++++++++++++++++++*/

static int IlluMultiStoreXML
(MPI_Comm comm, char *basename, int rank, int compressed,
 int dim, int px,int py,int pz, int nx,int ny,int nz,
 PetscScalar wx,PetscScalar wy,PetscScalar wz, int xm,int ym,int zm,
 int dof, int sw, int st, int wrap, char **fieldnames,
 field_plot_type *fieldtypes, PetscReal *fieldmin, PetscReal *fieldmax,
 int usermetacount, char **usermetanames, char **usermetadata)
{
  xmlDocPtr thedoc;
  xmlNodePtr thenode;
  FILE *outfile;
  char filename [1000];
  xmlChar buffer [1000];
  int i;

  /*+The XML tags in the .meta file consist of:
    +latex+\begin{center} \begin{tabular}{|l|l|} \hline
    +html+ <center><table BORDER>
    +latex+{\tt IlluMulti} & Primary tag, with attributes {\tt version},
    +latex+\\ & {\tt endian} ({\tt big} or {\tt little}) and
    +latex+{\tt compression} \\ & (none, 1-9, best, float*, long*, short* or
    +latex+char*\footnote{longnone, long1-long9 or longbest compresses each
    +latex+double to a 32-bit unsigned int, with 0 representing the minimum for
    +latex+that field and 2$^{32}-1$ representing the maximum; likewise
    +latex+shortnone, short1-short9, shortbest uses 16-bit unsigned ints, and
    +latex+char* uses 8-bit unsigned ints.  float is there to indicate that
    +latex+PetscScalar is 4 bytes at save time, loading should adjust
    +latex+accordingly; that's not fully supported just yet.  At some point
    +latex+I'll have to figure out how to represent complex.}). \\ \hline
    +html+ <tr><td><tt>IlluMulti</tt></td><td>Primary tag, with attributes
    +html+ <tt>version</tt>,<br><tt>endian</tt> (<tt>big</tt> or
    +html+ <tt>little</tt>) and <tt>compression</tt><br>(none, 1-9, best;
    +html+ long*, short* or char*)</td></tr>
    +*/

  thedoc = xmlNewDoc (BAD_CAST "1.0");
  thedoc->children = xmlNewDocNode (thedoc,NULL, BAD_CAST"IlluMulti", NULL);
  xmlSetProp (thedoc->children, BAD_CAST "version", BAD_CAST "0.2");
#ifdef WORDS_BIGENDIAN
  xmlSetProp (thedoc->children, BAD_CAST "endian", BAD_CAST "big");
#else
  xmlSetProp (thedoc->children, BAD_CAST "endian", BAD_CAST "little");
#endif
  if ((compressed & COMPRESS_INT_MASK) == COMPRESS_INT_LONG)
    strcpy ((char *) buffer, "long");
  else if ((compressed & COMPRESS_INT_MASK) == COMPRESS_INT_SHORT)
    strcpy ((char *) buffer, "short");
  else if ((compressed & COMPRESS_INT_MASK) == COMPRESS_INT_CHAR)
    strcpy ((char *) buffer, "char");
  /* Note: this doesn't really support complex types yet... */
  else if (sizeof (PetscScalar) == 4)
    strcpy ((char *) buffer, "float");
  else
    *buffer = '\0';
  if ((compressed & COMPRESS_GZIP_MASK) == 0)
    strcat ((char *) buffer, "none");
  else if ((compressed & COMPRESS_GZIP_MASK) >= 1 &&
	   (compressed & COMPRESS_GZIP_MASK) <= 9)
    sprintf ((char *) buffer + strlen ((char *) buffer), "%d",
	     compressed & COMPRESS_GZIP_MASK);
  else
    strcat ((char *) buffer, "best");
  xmlSetProp (thedoc->children, BAD_CAST "compression", buffer);

  /*+
    +latex+{\tt GlobalCPUs} & Number of CPUs in each direction, with
    +latex+\\ & attributes {\tt dimensions}, {\tt xwidth}, {\tt ywidth} and
    +latex+{\tt zwidth} \\ \hline
    +html+ <tr><td><tt>GlobalCPUs</tt></td><td>Number of CPUs in each
    +html+ direction, with<br>attributes <tt>dimensions</tt>, <tt>xwidth</tt>,
    +html+ <tt>ywidth</tt> and <tt>zwidth</tt></td></tr>
    +*/

  thenode = xmlNewChild (thedoc->children, NULL, BAD_CAST "GlobalCPUs", NULL);
  snprintf ((char *) buffer, 999, "%d", dim);
  xmlSetProp (thenode, BAD_CAST "dimensions", buffer);
  snprintf ((char *) buffer, 999, "%d", px);
  xmlSetProp (thenode, BAD_CAST "xwidth", buffer);
  if (dim > 1)
    {
      snprintf ((char *) buffer, 999, "%d", py);
      xmlSetProp (thenode, BAD_CAST "ywidth", buffer);
      if (dim > 2)
	{
	  snprintf ((char *) buffer, 999, "%d", pz);
	  xmlSetProp (thenode, BAD_CAST "zwidth", buffer);
	}
    }

  /*+
    +latex+{\tt GlobalSize} & Size of the entire distributed array, with
    +latex+\\ & attributes {\tt xwidth}, {\tt ywidth},
    +latex+{\tt zwidth}, {\tt xsize}**, {\tt ysize}**, {\tt zsize}** and
    +latex+{\tt fields} \\ \hline
    +html+ <tr><td><tt>GlobalSize</tt></td><td>Size of the entire distributed
    +html+ array, with<br>attributes <tt>xwidth</tt>, <tt>ywidth</tt>,
    +html+ <tt>zwidth</tt>, <tt>xsize</tt>**, <tt>ysize</tt>**,
    +html+ <tt>zsize</tt>** and <tt>fields</tt></td></tr>
    +*/

  thenode = xmlNewChild (thedoc->children, NULL, BAD_CAST "GlobalSize", NULL);
  snprintf ((char *) buffer, 999, "%d", nx);
  xmlSetProp (thenode, BAD_CAST "xwidth", buffer);
  snprintf ((char *) buffer, 999, "%g", wx);
  xmlSetProp (thenode, BAD_CAST "xsize", buffer);
  if (dim > 1)
    {
      snprintf ((char *) buffer, 999, "%d", ny);
      xmlSetProp (thenode, BAD_CAST "ywidth", buffer);
      snprintf ((char *) buffer, 999, "%g", wy);
      xmlSetProp (thenode, BAD_CAST "ysize", buffer);
      if (dim > 2)
	{
	  snprintf ((char *) buffer, 999, "%d", nz);
	  xmlSetProp (thenode, BAD_CAST "zwidth", buffer);
	  snprintf ((char *) buffer, 999, "%g", wz);
	  xmlSetProp (thenode, BAD_CAST "zsize", buffer);
	}
    }
  snprintf ((char *) buffer, 999, "%d", dof);
  xmlSetProp (thenode, BAD_CAST "fields", buffer);

  /*+
    +latex+{\tt LocalSize} & Size of the entire local part of the array,
    +latex+\\ & with attributes {\tt xwidth}, {\tt ywidth} and {\tt zwidth}
    +latex+\\ \hline
    +html+ <tr><td><tt>LocalSize</tt></td><td>Size of the local part of the
    +html+ array, with<br>attributes <tt>xwidth</tt>, <tt>ywidth</tt> and
    +html+ <tt>zwidth</tt></td></tr>
    +*/

  thenode = xmlNewChild (thedoc->children, NULL, BAD_CAST "LocalSize", NULL);
  snprintf ((char *) buffer, 999, "%d", xm);
  xmlSetProp (thenode, BAD_CAST "xwidth", buffer);
  if (dim > 1)
    {
      snprintf ((char *) buffer, 999, "%d", ym);
      xmlSetProp (thenode, BAD_CAST "ywidth", buffer);
      if (dim > 2)
	{
	  snprintf ((char *) buffer, 999, "%d", zm);
	  xmlSetProp (thenode, BAD_CAST "zwidth", buffer);
	}
    }

  /*+
    +latex+{\tt Stencil} & Stencil and periodic data, with attributes
    +latex+\\ & {\tt width}, {\tt type} and {\tt periodic} (using {\tt PETSc
    +latex+enum} values) \\ \hline
    +html+ <tr><td><tt>Stencil</tt></td><td>Stencil and periodic data, with
    +html+ attributes<br><tt>width</tt>, <tt>type</tt> and <tt>periodic</tt>
    +html+ (using <tt>PETSc enum</tt> values)</td></tr>
    +*/

  thenode = xmlNewChild (thedoc->children, NULL, BAD_CAST "Stencil", NULL);
  snprintf ((char *) buffer, 999, "%d", sw);
  xmlSetProp (thenode, BAD_CAST "width", buffer);
  snprintf ((char *) buffer, 999, "%d", (int) st);
  xmlSetProp (thenode, BAD_CAST "type", buffer);
  snprintf ((char *) buffer, 999, "%d", (int) wrap);
  xmlSetProp (thenode, BAD_CAST "periodic", buffer);

  /*+
    +latex+{\tt Field} & Data on each field, attributes {\tt name},
    +latex+{\tt type}**, {\tt min} and {\tt max} \\ \hline
    +html+ <tr><td><tt>Field</tt></td><td>Data on each field, attributes
    +html+ <tt>name</tt>, <tt>type</tt>*, <tt>min</tt> and
    +html+ <tt>max</tt></td></tr>
    +*/

  for (i=0; i<dof; i++)
    {
      thenode = xmlNewChild (thedoc->children, NULL, BAD_CAST "Field", NULL);
      xmlSetProp (thenode, BAD_CAST "name", BAD_CAST fieldnames [i]);
      snprintf ((char *) buffer, 999, "0x%.2x", fieldtypes [i]);
      xmlSetProp (thenode, BAD_CAST "type", buffer);
      snprintf ((char *) buffer, 999, "%g", fieldmin [i]);
      xmlSetProp (thenode, BAD_CAST "min", buffer);
      snprintf ((char *) buffer, 999, "%g", fieldmax [i]);
      xmlSetProp (thenode, BAD_CAST "max", buffer);
    }

  /*+
    +latex+{\tt User} & User parameters, attributes {\tt name} and {\tt value}
    +latex+\\ \hline \end{tabular} \end{center}
    +html+ <tr><td><tt>User</tt></td><td>User parameters, attributes
    +html+ <tt>name</tt> and <tt>value</tt></td></tr></table></center>
    *Lossy compression to smaller data types.
    +latex+\par
    +html+ <br>
    **Represents new attribute for IlluMulti 0.2 file format.
    +*/

  for (i=0; i<usermetacount; i++)
    {
      thenode = xmlNewChild (thedoc->children, NULL, BAD_CAST "User", NULL);
      xmlSetProp (thenode, BAD_CAST "name", BAD_CAST usermetanames [i]);
      xmlSetProp (thenode, BAD_CAST "value", BAD_CAST usermetadata [i]);
    }

  if (snprintf ((char *)filename, 999, "%s.cpu%.4d.meta", basename, rank) >999)
    SETERRQ1 (PETSC_ERR_ARG_SIZ, "Base file name too long (%d chars, 960 max)",
	      strlen (basename));
  if (!(outfile = fopen (filename, "w")))
    SETERRQ4 (PETSC_ERR_FILE_OPEN,
	      "CPU %d: error %d (%s) opening output file \"%s\"",
	      rank, errno, strerror (errno), filename);
  xmlDocDump (outfile, thedoc);
  xmlFreeDoc (thedoc);
  fclose(outfile);

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IlluMultiStoreData"

/*++++++++++++++++++++++++++++++++++++++
  This function stores the data file.

  int IlluMultiStoreData It returns zero or an error code.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  PetscScalar *globalarray Array from which to save the (local) data.

  char *basename Base file name.

  int rank CPU number to read data for.

  int compressed Data compression: if zero then no compression (fastest), 1-9
  then gzip compression level, 10-15 then gzip --best.  If 16-31 then save
  guint32s representing relative values between min and max for each field,
  compressed according to this value minus 16.  Likewise for 32-47 and
  guint16s, and 48-63 for guint8s.  Yes, these alternative formats lose
  information and can't be used for accurate checkpointing, but they should
  retain enough data for visualization (except perhaps for the guint8s, which
  are possibly acceptable for vectors but likely not contours).

  int gridpoints Number of gridpoints to store data for.

  int dof Degrees of freedom at each node, a.k.a. number of field variables.

  PetscScalar *fieldmin Minimum value of each field variable.

  PetscScalar *fieldmax Maximum value of each field variable.
  ++++++++++++++++++++++++++++++++++++++*/

static int IlluMultiStoreData
(MPI_Comm comm, PetscScalar *globalarray, char *basename, int rank,
 int compressed, int gridpoints, int dof, PetscScalar *fieldmin,
 PetscScalar *fieldmax)
{
  int i;
  char filename [1000];
  FILE *outfile;
  size_t writeout;

  if (compressed & COMPRESS_GZIP_MASK)
    {
      if ((compressed & COMPRESS_GZIP_MASK) >= 1 &&
	  (compressed & COMPRESS_GZIP_MASK) <= 9)
	{
	  if (snprintf (filename, 999, "gzip -c -%d > %s.cpu%.4d.data",
			compressed & COMPRESS_GZIP_MASK, basename, rank) > 999)
	    SETERRQ1 (PETSC_ERR_ARG_SIZ,
		      "Base file name too long (%d chars, 960 max)",
		      strlen (basename));
	}
      else
	{
	  if (snprintf (filename,999, "gzip -c --best > %s.cpu%.4d.data",
			basename,rank) > 999)
	    SETERRQ1 (PETSC_ERR_ARG_SIZ,
		      "Base file name too long (%d chars, 960 max)",
		      strlen (basename));
	}
      if (!(outfile = popen (filename, "w")))
	SETERRQ4 (PETSC_ERR_FILE_OPEN,
		  "CPU %d: error %d (%s) opening output pipe \"%s\"",
		  rank, errno, strerror (errno), filename);
    }
  else
    {
      if (snprintf (filename, 999, "%s.cpu%.4d.data", basename, rank) > 999)
	SETERRQ1 (PETSC_ERR_ARG_SIZ,
		  "Base file name too long (%d chars, 960 max)",
		  strlen (basename));
      if (!(outfile = fopen (filename, "w")))
	SETERRQ4 (PETSC_ERR_FILE_OPEN,
		  "CPU %d: error %d (%s) opening output file \"%s\"",
		  rank, errno, strerror (errno), filename);
    }

  switch (compressed & COMPRESS_INT_MASK)
    {
    case COMPRESS_INT_LONG:
      {
	guint32 *storage;
	if (!(storage = (guint32 *) malloc
	      (gridpoints * dof * sizeof (guint32))))
	  SETERRQ (PETSC_ERR_MEM, "Can't allocate data compression buffer");
	for (i=0; i<gridpoints*dof; i++)
	  storage [i] = (guint32)
	    (4294967295. * (globalarray [i] - fieldmin [i%dof]) /
	     (fieldmax [i%dof] - fieldmin [i%dof]));
	writeout = fwrite (storage, sizeof (guint32), gridpoints*dof, outfile);
	free (storage);
	break;
      }
    case COMPRESS_INT_SHORT:
      {
	guint16 *storage;
	if (!(storage = (guint16 *) malloc
	      (gridpoints * dof * sizeof (guint16))))
	  SETERRQ (PETSC_ERR_MEM, "Can't allocate data compression buffer");
	for (i=0; i<gridpoints*dof; i++)
	  storage [i] = (guint16)
	    (65535. * (globalarray [i] - fieldmin [i%dof]) /
	     (fieldmax [i%dof] - fieldmin [i%dof]));
	writeout = fwrite (storage, sizeof (guint16),  gridpoints*dof,outfile);
	free (storage);
	break;
      }
    case COMPRESS_INT_CHAR:
      {
	guint8 *storage;
	if (!(storage = (guint8 *) malloc
	      (gridpoints * dof * sizeof (guint8))))
	  SETERRQ (PETSC_ERR_MEM, "Can't allocate data compression buffer");
	for (i=0; i<gridpoints*dof; i++)
	  storage [i] = (guint8)
	    (255. * (globalarray [i] - fieldmin [i%dof]) /
	     (fieldmax [i%dof] - fieldmin [i%dof]));
	writeout = fwrite (storage, sizeof (guint8),  gridpoints*dof, outfile);
	free (storage);
	break;
      }
    default:
      writeout = fwrite (globalarray, sizeof (PetscScalar), gridpoints*dof,
			 outfile);
    }

  if (compressed & COMPRESS_GZIP_MASK)
    pclose (outfile);
  else
    fclose (outfile);

  if (writeout < gridpoints*dof)
    {
      int writerr = ferror (outfile);
      SETERRQ5 (PETSC_ERR_FILE_READ,
		"CPU %d: error %d (%s) during write, sent %d of %d items",
		rank, writerr, strerror (writerr), writeout, gridpoints*dof);
    }

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "checkagree"

/*++++++++++++++++++++++++++++++++++++++
  Ancillary routine for
  +latex+{\tt IlluMultiRead()}:
  +html+ <tt>IlluMultiRead()</tt>:
  checks agreement of parameters and reports disagreement if necessary.

  int checkagree Returns 0 if they agree, 1 otherwise.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  int da Integer parameter from the existing DA.

  int file Integer parameter read from the file.

  char *parameter Parameter name for reporting.
  ++++++++++++++++++++++++++++++++++++++*/

static inline int checkagree (MPI_Comm comm, int da, int file, char *parameter)
{
  int ierr;

  if (da == file)
    return 0;
  SETERRQ3 (PETSC_ERR_ARG_OUTOFRANGE,
	    "Disagreement in parameter %s: da has %d, file %d\n",
	    parameter, da, file);
}


#undef __FUNCT__
#define __FUNCT__ "IlluMultiRead"

/*++++++++++++++++++++++++++++++++++++++
  This reads the data into an existing distributed array and vector, checking
  that the sizes are right etc.

  int IlluMultiRead It returns zero or an error code.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  DA theda Distributed array object controlling the data to read.

  Vec X Vector into which to read the data.

  char *basename Base file name.

  int *usermetacount Pointer to an int where we put the number of user metadata
  parameters loaded.

  char ***usermetanames Pointer to a char ** where the loaded parameter names
  are stored.  This is
  +latex+{\tt malloc}ed by this function, so a call to {\tt free()}
  +html+ <tt>malloc</tt>ed by this function, so a call to <tt>free()</tt>
  is needed to free up its data.

  char ***usermetadata Pointer to a char ** where the loaded parameter strings
  are stored.  This is
  +latex+{\tt malloc}ed by this function, so a call to {\tt free()}
  +html+ <tt>malloc</tt>ed by this function, so a call to <tt>free()</tt>
  is needed to free up its data.
  ++++++++++++++++++++++++++++++++++++++*/

int IlluMultiRead
(MPI_Comm comm, DA theda, Vec X, char *basename,
 int *usermetacount, char ***usermetanames, char ***usermetadata)
{
  int dim, px,py,pz, nx,ny,nz, xm,ym,zm, dof, sw, size, rank, ierr;
  DAPeriodicType wrap, fwrap;
  DAStencilType st, fst;
  int fdim, fpx,fpy,fpz, fnx,fny,fnz, fxm,fym,fzm, fdof, fsw, fsize = 1;
  int compressed, wrongendian;
  char filename[1000], **fieldnames;
  FILE *infile;
  PetscScalar *globalarray, *fieldmin, *fieldmax;

  if (!comm)
    comm = PETSC_COMM_WORLD;

  /*+ First it gets the properties of the distributed array for comparison with
    the metadata. +*/
  ierr = DAGetInfo (theda, &dim, &nx,&ny,&nz, &px,&py,&pz, &dof, &sw, &wrap,
		    &st); CHKERRQ (ierr);
  ierr = DAGetCorners (theda, PETSC_NULL,PETSC_NULL,PETSC_NULL, &xm,&ym,&zm);
  CHKERRQ (ierr);

  /*+ Next it parses the XML metadata file into the document tree, and reads
    its content into the appropriate structures, comparing parameters with
    those of the existing distributed array structure. +*/
  ierr = MPI_Comm_size (comm, &size); CHKERRQ (ierr);
  ierr = MPI_Comm_rank (comm, &rank); CHKERRQ (ierr);

  DPRINTF ("Parsing XML metadata file from basename %s\n", basename);
  if (ierr = IlluMultiParseXML
      (comm, basename, rank, &compressed, &wrongendian, &fdim, &fpx,&fpy,&fpz,
       &fnx,&fny,&fnz, PETSC_NULL,PETSC_NULL,PETSC_NULL, &fxm,&fym,&fzm, &fdof,
       &fsw, &fst, &fwrap, &fieldnames, PETSC_NULL, &fieldmin, &fieldmax,
       usermetacount, usermetanames, usermetadata))
    return ierr;

  /* The size>1 checks are because we support loading multiple CPUs' data into
     a 1-CPU distributed array, as long as the global dimensions are right. */
  DPRINTF ("Checking size agreement between DA and data to read\n",0);
  fsize = fpx * ((dim > 1) ? fpy : 1) * ((dim > 2) ? fpz : 1);
  if (ierr = checkagree (comm, dim, fdim, "dimensions")) return ierr;
  if (size > 1 || fsize == 1) {
    if (ierr = checkagree (comm, px, fpx, "cpu xwidth")) return ierr;
    if (dim > 1) {
      if (ierr = checkagree (comm, py, fpy, "cpu ywidth")) return ierr;
      if (dim > 2) {
	if (ierr = checkagree (comm, pz, fpz, "cpu zwidth")) return ierr; }}}
  if (ierr = checkagree (comm, nx, fnx, "global xwidth")) return ierr;
  if (dim > 1) {
    if (ierr = checkagree (comm, ny, fny, "global ywidth")) return ierr;
    if (dim > 2) {
      if (ierr = checkagree (comm, nz, fnz, "global zwidth")) return ierr; }}
  if (size > 1 || fsize == 1) {
    if (ierr = checkagree (comm, xm, fxm, "local xwidth")) return ierr;
    if (dim > 1) {
      if (ierr = checkagree (comm, ym, fym, "local ywidth")) return ierr;
      if (dim > 2) {
	if (ierr = checkagree (comm, zm, fzm, "local zwidth")) return ierr; }}}
  if (ierr = checkagree (comm, dof, fdof, "degrees of freedom")) return ierr;
  if (ierr = checkagree (comm, sw, fsw, "stencil width")) return ierr;
  if (ierr = checkagree (comm, (int)st, (int)fst, "stencil type")) return ierr;
  if (ierr = checkagree (comm, (int)wrap, (int)fwrap, "periodic type"))
    return ierr;

  /*+ Then it streams in the data in one big slurp. +*/
  ierr = VecGetArray (X, &globalarray); CHKERRQ (ierr);
  if (size > 1 || fsize == 1) /* Default condition */
    {
      DPRINTF ("Reading data\n",0);
      ierr = IlluMultiParseData
	(comm, globalarray, basename, rank, compressed,
	 xm * ((dim>1)?ym:1) * ((dim>2)?zm:1), dof, wrongendian, fieldmin,
	 fieldmax);
      if (ierr)
	{
	  xm = VecRestoreArray (X, &globalarray); CHKERRQ (xm);
	  return ierr;
	}
    }
  else /* Getting distributed data into a single array */
    {
      int i,j,k, xs,ys,zs;
      PetscScalar *storage;

      /* Incrementally read in data to storage, store it in globalarray */
      /* First loop through the stored CPUs */
      for     (pz=0, zs=0; pz<((dim>2)?fpz:1); pz++, zs+=fzm)
	for   (py=0, ys=0; py<((dim>1)?fpy:1); py++, ys+=fym)
	  for (px=0, xs=0; px<fpx;            px++, xs+=fxm, rank++)
	    {
	      int gridpoints;

	      DPRINTF ("Parsing XML metadata file for CPU %d\n", rank);
	      if (ierr = IlluMultiParseXML
		  (comm, basename, rank, &compressed, &wrongendian, &fdim,
		   &fpx,&fpy,&fpz, &fnx,&fny,&fnz,
		   PETSC_NULL,PETSC_NULL,PETSC_NULL,
		   &fxm,&fym,&fzm, &fdof, &fsw, &fst, &fwrap,
		   &fieldnames, PETSC_NULL, &fieldmin, &fieldmax,
		   usermetacount, usermetanames, usermetadata))
		return ierr;
	      gridpoints = fxm * ((dim>1)?fym:1) * ((dim>2)?fzm:1);

	      /* This allocate/read/copy/free storage business is annoying.
		 Replacing it with "zero-copy" would involve changing the
		 IlluMultiParseData() API to allow loading into part of the
		 local array.  But I'll leave that for future expansion, when
		 this will be able to do arbitrary m->n CPU loading as long as
		 the global sizes match. */
	      storage = (PetscScalar *) malloc
		(gridpoints * dof * sizeof (PetscScalar));
	      DPRINTF ("Reading data for CPU %d\n", rank);
	      ierr = IlluMultiParseData
		(comm, storage, basename, rank, compressed, gridpoints, dof,
		 wrongendian, fieldmin, fieldmax);

	      fxm *= dof; /* so fxm is the number of PetscScalars to copy */
	      i=1;
	      /* Now distribute */
	      for (k=0; k<((dim>2)?fzm:1); k++)
		for (j=0; j<((dim>1)?fym:1); j++)
		  BLAScopy_ (&fxm, storage + (k*fym + j)*fxm /* *dof */, &i,
			   globalarray + (((zs+k)*ny + ys+j)*nx + xs)*dof, &i);

	      free (storage);
	      fxm /= dof;
	    }
    }

  ierr = VecRestoreArray (X, &globalarray); CHKERRQ (ierr);

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IlluMultiLoad"

/*++++++++++++++++++++++++++++++++++++++
  This creates a new distributed array of the appropriate size and loads the
  data into the vector contained in it (as retrieved by
  +latex+{\tt DAGetVector()}).
  +html+ <tt>DAGetVector()</tt>).
  It also reads the user metadata parameters into arrays stored at the supplied
  pointers.

  int IlluMultiLoad It returns zero or an error code.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  char *basename Base file name.

  DA *theda Pointer to a DA object (to be created by this function).

  PetscScalar *wx Physical overall width in the
  +latex+$x$-direction.
  +html+ <i>x</i>-direction.

  PetscScalar *wy Physical overall width in the
  +latex+$y$-direction.
  +html+ <i>y</i>-direction.

  PetscScalar *wz Physical overall width in the
  +latex+$z$-direction.
  +html+ <i>z</i>-direction.

  field_plot_type **fieldtypes Data (plot) types for field variables.

  int *usermetacount Pointer to an int where we put the number of user metadata
  parameters loaded.

  char ***usermetanames Pointer to a char ** where the loaded parameter names
  are stored.  This is
  +latex+{\tt malloc}ed by this function, so a call to {\tt free()}
  +html+ <tt>malloc</tt>ed by this function, so a call to <tt>free()</tt>
  is needed to free up its data.

  char ***usermetadata Pointer to a char ** where the loaded parameter strings
  are stored.  This is
  +latex+{\tt malloc}ed by this function, so a call to {\tt free()}
  +html+ <tt>malloc</tt>ed by this function, so a call to <tt>free()</tt>
  is needed to free up its data.
  ++++++++++++++++++++++++++++++++++++++*/

int IlluMultiLoad
(MPI_Comm comm, char *basename, DA *theda, PetscScalar *wx,PetscScalar *wy,
 PetscScalar *wz, field_plot_type **fieldtypes, int *usermetacount,
 char ***usermetanames, char ***usermetadata)
{
  int dim, px,py,pz, nx,ny,nz, dof, sw, fxm,fym,fzm, rank,size, ierr;
  int compressed, wrongendian, fpx,fpy,fpz, fsize, xm,ym,zm;
  DAPeriodicType wrap;
  DAStencilType st;
  char filename[1000], **fieldnames;
  xmlChar *buffer;
  FILE *infile;
  xmlDocPtr thedoc;
  xmlNodePtr thenode;
  PetscScalar *globalarray, *fieldmin, *fieldmax;
  Vec X;

  if (!comm)
    comm = PETSC_COMM_WORLD;

  ierr = MPI_Comm_rank (comm, &rank); CHKERRQ (ierr);
  ierr = MPI_Comm_size (comm, &size); CHKERRQ (ierr);

  /*+ First it gets the parameters from the XML file. +*/
  DPRINTF ("Parsing XML metadata file from basename %s\n", basename);
  if (ierr = IlluMultiParseXML
      (comm, basename, rank, &compressed, &wrongendian, &dim, &fpx,&fpy,&fpz,
       &nx,&ny,&nz, wx,wy,wz, &fxm,&fym,&fzm, &dof, &sw, &st, &wrap,
       &fieldnames, fieldtypes, &fieldmin, &fieldmax,
       usermetacount, usermetanames, usermetadata))
    return ierr;

  fsize = fpx * ((dim > 1) ? fpy : 1) * ((dim > 2) ? fpz : 1);
  if (size == fsize) { /* Default condition: n->n */
    px = fpx; py = fpy; pz = fpz; }
  else if (size == 1)  /* Special condition: n->1 */
    px = py = pz = 1;
  else                 /* Error: incorrect number of CPUs */
    SETERRQ5 (PETSC_ERR_ARG_SIZ,
	      "Incorrect CPU count: current %d, stored %dx%dx%d=%d\n",
	      size, fpx,fpy,fpz, fsize);

  /*+ Next it creates a distributed array based on those parameters, and sets
    the names of its fields. +*/
  switch (dim)
    {
    case 1:
      {
	DPRINTF ("Creating %d-%d 1-D DA\n", nx, dof);
	ierr = DACreate1d (comm, wrap, nx, dof, sw, PETSC_NULL,
			   theda); CHKERRQ (ierr);
	break;
      }
    case 2:
      {
	DPRINTF ("Creating %dx%d-%d 2-D DA\n", nx,ny, dof);
	ierr = DACreate2d (comm, wrap, st, nx,ny, px,py, dof, sw,
			   PETSC_NULL, PETSC_NULL, theda); CHKERRQ (ierr);
	break;
      }
    case 3:
      {
	DPRINTF ("Creating %dx%dx%d-%d 3-D DA\n", nx,ny,nz, dof);
	ierr = DACreate3d (comm, wrap, st, nx,ny,nz, px,py,pz, dof,
			   sw, PETSC_NULL, PETSC_NULL, PETSC_NULL, theda);
	CHKERRQ (ierr);
	break;
      }
    default:
      SETERRQ1 (PETSC_ERR_ARG_OUTOFRANGE,
		"Unsupported number of dimensions %d\n", dim);
    }

  ierr = DAGetCorners (*theda, PETSC_NULL, PETSC_NULL, PETSC_NULL,
		       &xm,&ym,&zm); CHKERRQ (ierr);
  if(size==fsize && (xm != fxm || ym != fym || zm != fzm))
    SETERRQ6 (PETSC_ERR_ARG_SIZ,
	      "Local array size mismatch: DA %dx%dx%d, stored %dx%dx%d\n",
	      xm,ym,zm, fxm,fym,fzm);

  for (px=0; px<dof; px++)
    DASetFieldName (*theda, px, fieldnames [px]);

  /*+ Then it streams the data into the distributed array's vector in one big
    slurp. +*/
  DPRINTF ("Getting global vector and array\n",0);
  ierr = DAGetGlobalVector (*theda, &X); CHKERRQ (ierr);
  ierr = VecGetArray (X, &globalarray); CHKERRQ (ierr);
  if (size > 1 || fsize == 1) /* Default condition */
    {
      DPRINTF ("Loading data in parallel\n",0);
      ierr = IlluMultiParseData
	(comm, globalarray, basename, rank, compressed,
	 fxm * ((dim>1)?fym:1) * ((dim>2)?fzm:1), dof, wrongendian, fieldmin,
	 fieldmax);
      if (ierr)
	{
	  fxm = VecRestoreArray (X, &globalarray); CHKERRQ (fxm);
	  return ierr;
	}
    }
  else /* Getting distributed data into a single array */
    {
      int i,j,k, xs,ys,zs;
      PetscScalar *storage;

      /* Incrementally read in data to storage, store it in globalarray */
      /* First loop through the stored CPUs */
      DPRINTF ("Loading data into a single array on 1 CPU\n",0);
      for     (pz=0, zs=0; pz<((dim>2)?fpz:1); pz++, zs+=fzm)
	for   (py=0, ys=0; py<((dim>1)?fpy:1); py++, ys+=fym)
	  for (px=0, xs=0; px<fpx;             px++, xs+=fxm, rank++)
	    {
	      int gridpoints, fdim, fnx,fny,fnz, fdof, fsw;
	      DAPeriodicType fwrap;
	      DAStencilType fst;

	      if (ierr = IlluMultiParseXML
		  (comm, basename, rank, &compressed, &wrongendian, &fdim,
		   &fpx,&fpy,&fpz, &fnx,&fny,&fnz,
		   PETSC_NULL,PETSC_NULL,PETSC_NULL,
		   &fxm,&fym,&fzm, &fdof, &fsw, &fst, &fwrap,
		   &fieldnames, PETSC_NULL, &fieldmin, &fieldmax,
		   usermetacount, usermetanames, usermetadata))
		return ierr;
	      gridpoints = fxm * ((dim>1)?fym:1) * ((dim>2)?fzm:1);

	      /* This allocate/read/copy/free storage business is annoying.
		 Replacing it with "zero-copy" would involve changing the
		 IlluMultiParseData() API to allow loading into part of the
		 local array.  But I'll leave that for future expansion, when
		 this will be able to do arbitrary m->n CPU loading as long as
		 the global sizes match. */
	      storage = (PetscScalar *) malloc
		(gridpoints * dof * sizeof (PetscScalar));
	      ierr = IlluMultiParseData
		(comm, storage, basename, rank, compressed, gridpoints, dof,
		 wrongendian, fieldmin, fieldmax);

	      fxm *= dof; /* so fxm is the number of PetscScalars to copy */
	      i=1;
	      /* Now distribute */
	      for (k=0; k<((dim>2)?fzm:1); k++)
		for (j=0; j<((dim>1)?fym:1); j++)
		  BLAScopy_ (&fxm, storage + (k*fym + j)*fxm /* *dof */, &i,
			   globalarray + (((zs+k)*ny + ys+j)*nx + xs)*dof, &i);

	      free (storage);
	      fxm /= dof;
	    }
    }

  DPRINTF ("Done.\n",0);
  ierr = VecRestoreArray (X, &globalarray); CHKERRQ (ierr);
  ierr = DARestoreGlobalVector (*theda, &X); CHKERRQ (ierr);

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "IlluMultiSave"

/*++++++++++++++++++++++++++++++++++++++
  This saves the vector X in multiple files, two per process.

  int IlluMultiSave it returns zero or an error code.

  MPI_Comm comm MPI communicator, if NULL it uses PETSC_COMM_WORLD.

  DA theda Distributed array object controlling data saved.

  Vec X Vector whose data are actually being saved.

  char *basename Base file name.

  int usermetacount Number of user metadata parameters.

  char **usermetanames User metadata parameter names.

  char **usermetadata User metadata parameter strings.

  int compressed Data compression: if zero then no compression (fastest), 1-9
  then gzip compression level, 10-15 then gzip --best.  If 16-31 then save
  guint32s representing relative values between min and max for each field,
  compressed according to this value minus 16.  Likewise for 32-47 and
  guint16s, and 48-63 for guint8s.  Yes, these alternative formats lose
  information and can't be used for accurate checkpointing, but they should
  retain enough data for visualization (except perhaps for the guint8s, which
  are possibly acceptable for vectors but certainly not contours).
  ++++++++++++++++++++++++++++++++++++++*/

int IlluMultiSave
(MPI_Comm comm, DA theda, Vec X, char *basename, PetscScalar wx,PetscScalar wy,
 PetscScalar wz, field_plot_type *fieldtypes, int usermetacount,
 char **usermetanames, char **usermetadata, int compressed)
{
  int dim, nx,ny,nz, px,py,pz, dof, sw, xs,ys,zs, xm,ym,zm, rank, i, ierr;
  DAPeriodicType wrap;
  DAStencilType st;
  FILE *outfile;
  char filename[1000], **fieldnames;
  PetscReal *fieldmin, *fieldmax;
  PetscScalar *globalarray;
  guint64 *array64;
  guint32 *array32;
  guint16 *array16;
  guint8 *array8;

  if (!comm)
    comm = PETSC_COMM_WORLD;

  /*+First a check to verify a supported value of
    +latex+{\tt compressed},
    +html+ <tt>compressed</tt>,
    but no fancy guint* compression for complex!
    +*/
#ifdef PETSC_USE_COMPLEX
  if (compressed < 0 || compressed > COMPRESS_GZIP_MASK)
    SETERRQ1(PETSC_ERR_ARG_OUTOFRANGE,"Unsupported compression type code 0x%x",
	     compressed);
#else
  if (compressed < 0 || compressed > (COMPRESS_GZIP_MASK | COMPRESS_INT_MASK))
    SETERRQ1(PETSC_ERR_ARG_OUTOFRANGE,"Unsupported compression type code 0x%x",
	     compressed);
#endif

  /*+Then get the distributed array parameters and processor number, and store
    all this data in the XML .meta file. +*/
  ierr = DAGetInfo (theda, &dim, &nx,&ny,&nz, &px,&py,&pz, &dof, &sw, &wrap,
		    &st); CHKERRQ (ierr);
  ierr = DAGetCorners (theda, &xs,&ys,&zs, &xm,&ym,&zm); CHKERRQ (ierr);
  ierr = MPI_Comm_rank (comm, &rank); CHKERRQ (ierr);

  if (!(fieldnames = (char **) malloc (dof * sizeof (char *))))
    SETERRQ (PETSC_ERR_MEM, "Can't allocate field names");
  if (!(fieldmin = (PetscScalar *) malloc (2 * dof * sizeof (PetscScalar))))
    SETERRQ (PETSC_ERR_MEM, "Can't allocate field params");
  fieldmax = fieldmin + dof;
  for (i=0; i<dof; i++)
    {
      ierr = DAGetFieldName (theda, i, fieldnames + i); CHKERRQ (ierr);
      ierr = VecStrideMin (X, i, PETSC_NULL, fieldmin + i); CHKERRQ (ierr);
      ierr = VecStrideMax (X, i, PETSC_NULL, fieldmax + i); CHKERRQ (ierr);
    }

  if (ierr = IlluMultiStoreXML
      (comm, basename, rank, compressed, dim, px,py,pz, nx,ny,nz, wx,wy,wz,
       xm,ym,zm, dof, sw, (int) st, (int) wrap, fieldnames,fieldtypes,
       fieldmin,fieldmax, usermetacount, usermetanames, usermetadata))
    return ierr;

  /*+ Finally, the data just stream out to the data file or gzip pipe in one
    big lump. +*/
  ierr = VecGetArray (X, &globalarray); CHKERRQ (ierr);
  IlluMultiStoreData (comm, globalarray, basename, rank, compressed,
		      xm * ((dim>1)?ym:1) * ((dim>2)?zm:1), dof,
		      fieldmin, fieldmax);
  ierr = VecRestoreArray (X, &globalarray); CHKERRQ (ierr);

  free (fieldmin);
  free (fieldnames);

  return 0;
}
