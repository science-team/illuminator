illuminator (0.10.1-1) unstable; urgency=low

  * New upstream: bug fixes and one new feature (.tif layer save for 3D).
  * Update to new PETSc 2.3.3.
  * Included math-blaslapack.m4 dropped by petsc package (closes: #443033).
  * Dropped Alpha dependency on orphaned libffm-dev.
  * Changed tetex dependencies to texlive (closes: #419564).

 --

illuminator (0.10.0-4) unstable; urgency=low

  * Put debhelper in Build-Depends (closes: #408457).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 26 Jan 2007 16:32:51 -0500

illuminator (0.10.0-3) unstable; urgency=low

  * Rebuilding against new PETSc 2.3.2 (closes: #376890).
  * Fixed chts.c for new PetscRandom API (closes: #402784).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 17 Jan 2007 17:33:19 -0500

illuminator (0.10.0-2etch1) unstable; urgency=low

  * Rebuild against PETSc 2.3.1.
  * A couple of upstream fixes for future 0.10.1.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Thu, 13 Apr 2006 13:10:43 -0400

illuminator (0.10.0-2) unstable; urgency=low

  * Important packaging fixes, including libffm-dev build-dep on alpha,
    petsc-dev substvar fix (making libluminate-dev installable), etc.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Thu, 23 Feb 2006 10:28:27 -0500

illuminator (0.10.0-1etch1) unstable; urgency=low

  * Brown-bag packaging fixes, rebuilt for etch, will be rebuilt and uploaded
    to unstable when gnome-vfs transitions.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Thu,  9 Feb 2006 16:58:06 -0500

illuminator (0.10.0-1) unstable; urgency=low

  * New upstream, removes dvips from Build-Depends-Indep (closes: #337378).
  * One tiny fix to petsc-dev substvar calculation.
  * Updated DH_COMPAT to 4.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue,  7 Feb 2006 19:23:30 -0500

illuminator (0.9.1-3) unstable; urgency=low

  * Changed libffm1-dev to libffm-dev in Build-Depends so it builds on alpha.
  * Slight downgrade to petsc deps for sarge backport compatibility.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Thu,  8 Sep 2005 19:26:30 -0400

illuminator (0.9.1-2) unstable; urgency=low

  * Rebuild using new mpich/petsc stack.
  * Hand-removed -lslog from aclocal.m4 and configure to make this work.
  * Versioned petsc dependencies marked in control and rules.
  * Updated readline build-dependency (closes: #326342).
  * Moved end-line double-backslashes in illumulti.c and render.c to next lines
    to work around cxref-related toolchain change somewhere (not cxref...).
  * Updated FSF address and date in AUTHORS and debian/copyright files.
  * Updated standards version in control.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed,  7 Sep 2005 13:06:52 -0400

illuminator (0.9.1-1sarge1) unstable; urgency=low

  * Rebuilt against petsc 2.2.0-4sarge1 and mpich 1.2.7-0sarge2.
  * Hand-removed -lslog from aclocal.m4 and configure to make this work.
  * Versioned petsc dependencies marked in control and rules.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 16 Aug 2005 14:52:18 -0400

illuminator (0.9.1-1) unstable; urgency=low

  * New upstream.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Sun,  1 May 2005 10:47:27 -0400

illuminator (0.9.0-1) unstable; urgency=low

  * New upstream with libluminate version bump.
  * Removed @build_cpu@ from illuminator.tex.in to avoid trouble with x86_64
    architecture (closes: #255908).
  * Switched to lmodern fonts in documentation.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 30 Nov 2004 17:38:51 -0500

illuminator (0.8.9-2) unstable; urgency=low

  * Brown bag packaging error: moved libluminate4.files to libluminate5.files!

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 18 Aug 2004 13:58:09 -0400

illuminator (0.8.9-1) unstable; urgency=low

  * New upstream with libluminate version bump.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 17 Aug 2004 10:57:41 -0400

illuminator (0.8.0-2) unstable; urgency=low

  * Workaround for cxref issue with glibconfig.h.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 23 Jun 2004 19:32:15 -0400

illuminator (0.8.0-1) unstable; urgency=low

  * New upstream with new gtk+-2, gnome 2, glade 2, libxml2 dependencies.
  * Removed menu entries in favor of new .desktop files.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 26 May 2004 12:23:31 -0400

illuminator (0.7.0-0pre0.woody.0) unstable; urgency=low

  * New upstream pre-release with added readline dependencies in CVS, and
    libluminate version bump.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue,  9 Mar 2004 21:35:15 -0500

illuminator (0.6.9-2) unstable; urgency=low

  * Changed build-depend from libffm-dev to libffm1-dev (closes: #225190).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 30 Dec 2003 13:57:47 -0500

illuminator (0.6.9-1) unstable; urgency=low

  * Build-depends on petsc-dev, auto-detects relevant petsc version for
    libluminate-dev dependency (closes: #221400).
  * Build-depends on hevea instead of latex2html (closes: #221337).
  * libluminate-dev conflicts with and replaces illuminator-dev, and depends on
    same version of libluminate3 (closes: #204394).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 18 Nov 2003 19:48:22 -0500

illuminator (0.6.9-0.woody.1) unstable; urgency=low

  * First preview release of new upstream.
  * Leaving out tsview-ng because it's not ready for prime-time.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 18 Nov 2003 19:48:15 -0500

illuminator (0.6.2-1) unstable; urgency=low

  * First unstable release of new upstream.
  * Depends on newest 2.1.6 PETSc.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Thu,  4 Sep 2003 15:48:28 -0400

illuminator (0.6.2-0.woody.1) unstable; urgency=low

  * New upstream with minor fixes.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri,  1 Aug 2003 15:45:22 -0400

illuminator (0.6.1-1) unstable; urgency=low

  * First unstable release/upload of new upstream.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 25 Jul 2003 13:26:45 -0400

illuminator (0.6.1-0.woody.1) unstable; urgency=low

  * New upstream with minor fixes.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 25 Jul 2003 12:58:49 -0400

illuminator (0.6.0-0.woody.1) unstable; urgency=low

  * Another new upstream, another bumped shlib version.
  * Updates to control file including some package name changes.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Thu, 24 Jul 2003 18:09:36 -0400

illuminator (0.5.0-1) unstable; urgency=low

  * First unstable release/upload of new upstream (closes: #185374).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Sun, 20 Jul 2003 09:41:57 -0400

illuminator (0.5.0-0.woody.1) unstable; urgency=low

  * New upstream, with bumped shlib version.
  * Fixes bug 185374.
  * Packaging workaround to dh_installdocs "feature" which had used
    .doc-base.in instead of constructed .doc-base.
  * Changed illuminator-dev section to libdevel, even for this woody package
    (since I don't plan to actually upload it).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 21 May 2003 18:38:35 -0400

illuminator (0.4.4-2.woody.1) stable; urgency=low

  * Backport to woody for new PETSc version 2.1.5, and with tsview.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Sun, 20 Apr 2003 11:44:26 -0400

illuminator (0.4.4-2) unstable; urgency=low

  * D'oh!  Forgot to include tsview in the package.  Here it is, in
    illuminator-demo.  I'll probably rename that to illuminator-bin at some
    point, since that's what it's becoming (tsview isn't really a -demo
    program).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 25 Mar 2003 17:22:25 -0500

illuminator (0.4.4-1) unstable; urgency=low

  * First unstable release of new upstream, updated PETSc dependency versions.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 18 Mar 2003 10:27:03 -0500

illuminator (0.4.4-0.woody.1) unstable; urgency=low

  * New upstream release, one bugfix and two new features.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 14 Mar 2003 18:01:41 -0500

illuminator (0.4.3-4) unstable; urgency=low

  * Rebuild now that gcc is 3.2 and link with shared mpich (closes: #170573).

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 15 Jan 2003 12:14:15 -0500

illuminator (0.4.3-3) unstable; urgency=low

  * Updated chui.c so DPRINTF macro works with cxref.
  * Removed -wait option from 3dgf, so it's done by default.
  * Cleanups for documentation generation.
  * Updated Standards-Version.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Sun, 22 Dec 2002 19:33:28 -0500

illuminator (0.4.3-2) unstable; urgency=low

  * Oops!  Forgot to update the debian/ stuff.  Changed to @VERSION@ so this
    won't happen again.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Sun,  1 Dec 2002 14:31:20 -0500

illuminator (0.4.3-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Sat, 30 Nov 2002 19:50:52 -0500

illuminator (0.4.2-2) unstable; urgency=low

  * Oops, omission in -dev.files left illuminator.m4 out of the package.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 12 Nov 2002 12:12:32 -0500

illuminator (0.4.2-1) unstable; urgency=low

  * New upstream.
  * Merged packaging in with "upstream" to make future releases smoother.
  * Finally removed old petscgraphics Conflicts/Replaces because it's ancient.
  * Build-Conflicts with cpml* (alpha) since that confuses math-blaslapack.m4.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Mon, 11 Nov 2002 20:01:06 -0500

illuminator (0.3.2-2) unstable; urgency=low

  * New build with unstable libs, including the new shared mpich.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Fri, 21 Jun 2002 12:24:13 +0000

illuminator (0.3.2-1.woody.2) testing; urgency=low

  * D'OH!  Forgot to set the distribution to "testing" before upload...

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 18 Jun 2002 23:13:56 -0400

illuminator (0.3.2-1.woody.1) unstable; urgency=low

  * Special upload to get the new version into woody, because of build problems
    and dependency on specific PETSc version.
  * Removed cruft comments from rules file.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 18 Jun 2002 19:07:30 -0400

illuminator (0.3.2-1) unstable; urgency=low

  * New upstream bugfix release. (closes: #149498)

 -- Adam C. Powell, IV <hazelsct@debian.org>  Mon, 10 Jun 2002 18:59:19 -0400

illuminator (0.3.1-1) unstable; urgency=low

  * New upstream bugfix release.
  * Menu entry for chui.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed,  5 Jun 2002 13:16:25 -0400

illuminator (0.3.0-1) unstable; urgency=low

  * New upstream release.
  * New "chui" front-end to chts.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue,  4 Jun 2002 22:57:35 -0400

illuminator (0.2.5-1) unstable; urgency=low

  * New upstream release.
  * Includes fix to work with multi-field PETSc vectors/distributed arrays.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Thu, 21 Mar 2002 16:19:32 -0500

illuminator (0.2.4-2) unstable; urgency=low

  * Minor upstream fix to make it work for non-periodic DAs.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Mon, 11 Feb 2002 19:50:18 -0500

illuminator (0.2.4-1) unstable; urgency=low

  * New upstream release for compatibility with PETSc 2.1.1.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue, 15 Jan 2002 18:41:13 -0500

illuminator (0.2.3-1) unstable; urgency=low

  * Name change!  Replaces and conflicts with old petscgraphics* packages.
  * Added tetex-extra to Build-Depends-Indep for fullpage.sty (closes: #119598)
  * Removed c-compiler from Build-Depends since petsc2.1.0-dev depends on the
    right compiler.
  * Changed section to graphics, since that's what it is.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Tue,  4 Dec 2001 22:23:58 -0500

petscgraphics (0.2.2-2) unstable; urgency=low

  * Changed shlib's Recommends: geomview to Suggests: because it is likely to
    be dragged in as a dependency on a compute node, where geomview would be
    useless.  Changed -demo's Depends: to Recommends: for the same reason.
  * Built against new PETSc packages which depend on atlas and not lapack, to
    make sure this uses atlas.
  * Updated cahnhill.h, cahnhill.c and chts.c from CVS so the documentation is
    complete.
  * Replaced index.html hard link with a symbolic one, so lintian doesn't
    complain about a zero-length file.
  * Made Build-Depends-Indep and put appropriate things there.
  * Added a new chts manpage.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 22 Aug 2001 09:14:57 -0400

petscgraphics (0.2.2-1) unstable; urgency=low

  * New upstream release.
  * Changed priority from optional to extra.
  * Dropped the cruft at the bottom of this file.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Mon,  2 Jul 2001 20:32:20 -0400

petscgraphics (0.2.1-1) unstable; urgency=low

  * Initial release (closes: #100724).
  * Had to re-autogen/make mydist to get shared libs to work at all, resulting
    in a large patch with changes to aclocal.m4, ltmain.sh, etc. vs. official
    release.
  * Slight modifications to Makefile.am.
  * Corrected URL in petscgraphics.html.in.
  * Added some documentation to help people understand the basics of using the
    library.

 -- Adam C. Powell, IV <hazelsct@debian.org>  Wed, 13 Jun 2001 18:10:28 -0400
