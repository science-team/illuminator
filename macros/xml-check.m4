dnl Check for libxml by Adam Powell last modified 2002-09-07
dnl Copyright 2002 Adam Powell, redistributable under the terms of the GNU
dnl General Public License version 2 or later.

dnl For libxml2, just change xml to xml2, version numbers.

dnl "Bugs": doesn't actually try linking to the library as gnome-xml-check
dnl does, no option to not die on failure.

AC_DEFUN([XML_CHECK],[
	dispvers="$1"
	AC_PATH_PROG(XML_CONFIG, xml-config,
		AC_MSG_ERROR(You need libxml $dispvers or later.))
	AC_MSG_CHECKING(for libxml >= $dispvers)
	cmpvers=`echo $dispvers | awk -F. '{ print $[]1 * 1000000 + $[]2 * 10000 + $[]3 * 100 + $[]4;}'`
	if $XML_CONFIG --libs > /dev/null 2>&1; then
		pkgvers=`$XML_CONFIG --version | sed -e 's/^[[^0-9]]*//'`
	else
		pkgvers=not
	fi
	AC_MSG_RESULT($pkgvers found)

	pkgvers=`echo $pkgvers | awk -F. '{ print $[]1 * 1000000 + $[]2 * 10000 + $[]3 * 100 + $[]4;}'`
	if test $pkgvers -lt $cmpvers; then
		AC_MSG_ERROR(You need libxml $dispvers or later.)
	fi
	LIBXML_CFLAGS="`$XML_CONFIG --cflags`"
	LIBXML_LIBS="`$XML_CONFIG --libs`"
	AC_SUBST(LIBXML_CFLAGS)
	AC_SUBST(LIBXML_LIBS)
])
