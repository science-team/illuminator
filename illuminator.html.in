<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <meta name="Author" content="Adam C. Powell, IV">
   <meta name="GENERATOR" content="Mozilla/4.7 [en] (X11; I; Linux 2.2.18pre21 ppc) [Netscape]">
   <meta name="Description" content="Documentation home for Illuminator visualization software, Note @VERSION@ in the .tar.gz link!">
   <title>Illuminator Distributed Visualization Library</title>
</head>
<body text="#330000" bgcolor="#66FF99" link="#000099" vlink="#551A8B" alink="#FFFF00">

<center><h1><tt>Illuminator</tt> Distributed Visualization
Library</h1></center>

<center><h3>by <a href="http://lyre.mit.edu/~powell/">Adam
Powell</a></h3></center>

<center><h3>Version @VERSION@ released @RELEASE_DATE@, built for a
<tt>@build_vendor@ @build_cpu@</tt> running <tt>@build_os@</tt></h3></center>

<hr WIDTH="100%">
<table ALIGN=RIGHT BORDER NOSAVE >
<tr NOSAVE>
<td BGCOLOR="#FFCC33" NOSAVE>Documentation Formats:</td>
<td BGCOLOR="#FFCC33" NOSAVE>Source code reference:</td>
</tr>

<tr BGCOLOR="#66FFFF" NOSAVE>
<td NOSAVE>
<!--UNCOMMENT IF HAVE_PLAIN_LATEX
<a href="@PACKAGE@.dvi">dvi</a>
UNCOMMENT IF HAVE_PLAIN_LATEX-->
<!--UNCOMMENT IF HAVE_PDFLATEX
<br><a href="@PACKAGE@.pdf">pdf</a>
UNCOMMENT IF HAVE_PDFLATEX-->
<!--UNCOMMENT IF HAVE_DVIPS
<br><a href="@PACKAGE@.ps.gz">gzipped PostScript</a>
<br><a href="@PACKAGE@.ps">PostScript</a>
UNCOMMENT IF HAVE_DVIPS-->
<!--UNCOMMENT IF HAVE_HEVEA
<br><a href="@PACKAGE@-tex.html">HTML</a> (<a href="http://pauillac.inria.fr/~maranget/hevea/">HeVeA</a>)
UNCOMMENT IF HAVE_HEVEA-->
<!--UNCOMMENT IF HAVE_LATEX2HTML
<br><a href="@PACKAGE@.html">HTML</a> (<a href="http://www.latex2html.org/">LaTeX2HTML</a>)
<br><a href="@PACKAGE@-html-@VERSION@.tar.gz">.tar.gz HTML directory</a>
UNCOMMENT IF HAVE_LATEX2HTML-->
</td>
<td>
<!-- Begin-Of-Source-Files -->

<!-- End-Of-Source-Files -->
</td></tr>

<tr NOSAVE>
<td BGCOLOR="#FFFF00" COLSPAN=2 NOSAVE>If you need a PostScript previewer, try
<a href="http://www.cs.wisc.edu/~ghost/">GSview</a></td>
</tr>
</table>
This is the documentation starting point for
<a href="http://lyre.mit.edu/~powell/illuminator.html">Illuminator</a>, a
distributed visualization library. It is based on
<a href="http://www-unix.mcs.anl.gov/petsc/petsc-2/">PETSc</a>, and the current
implementation uses <a href="http://geomview.org/">Geomview</a> to render
isoquant surfaces.<p>

In addition to visualization, Illuminator 0.4+ includes distributed storage
functions, to permit rapid saving and loading of data to local hard drives on a
Beowulf cluster using various types of compression.<p>

Copyright (C) 2001, 2002 Adam Powell;
Copyright (C) 2003 Adam Powell, Jorge Vieyra, Bo Zhou;
Copyright (C) 2004 Adam Powell, Ojonimi Ocholi, Jorge Vieyra, Bo Zhou.<p>

The <tt>libluminate</tt> library is free software; you can redistribute it
and/or modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 2.1 of the License,
or (at your option) any later version.<p>

Note that it uses some chunks of code from Ken Brakke's public domain Surface
Evolver, those chunks are attributed in the source code.<p>

The other source code and documentation in this Illuminator package are free
software; you can distribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.<p>

This software is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU (Lesser) General Public License for more
details.<p>

You should have received a copy of the GNU (Lesser) General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA<p>

You may contact the corresponding author by email at
<tt><a href="mailto:hazelsct@mit.edu">hazelsct@mit.edu</a></tt>.<p>

Share and enjoy!

<address><a href="http://lyre.mit.edu/~powell/">Adam Powell</a></address>
</body>
</html>
