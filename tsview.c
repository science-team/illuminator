/***************************************
  $Header$

  This program views the output of a time series saved using
  +latex+{\tt IlluMultiSave()}.
  +html+ <tt>IlluMultiSave()</tt>.
  It basically just switches between timesteps; future versions may be more
  interesting.  The neat part of it is that it loads multiprocessor data and
  displays it on a single CPU.
  ***************************************/

static char help[] = "Displays the output of of a timestep series saved using IlluMultiSave().\n\
Usage:\n\
\n\
  tsview <basename> [-no_transparency]\n\
\n\
Then interactively flip through the timesteps (h or ? lists commands).\n";

#define HELP_STRING "tsview commands:\n\
  <enter>       Display next timestep\n\
  b             Display previous timestep\n\
  i increment   Set the next timestep increment\n\
  ###           Jump to timestep ###\n\
  t             Toggle Geomview transparency (3-D only)\n\
  v             Change field displayed (3-D only)\n\
  d             Dump geomview to picture (3-D only), creates basename-f%d.ppm\n\
  a             Dump all timesteps to pictures (3-D only)\n\
  p [v1 v2 ...] Set contour values for plotting or \"auto\" (3-D only)\n\
  r             Reloads entries in a directory\n\
  s size        Set maximum dimension of PETSc viewer windows (2-D only)\n\
  cx, cy, cz    Toggle xcut, ycut, zcut (cut last row/plane of periodic DA)\n\
  gx30-90, y,z  Set plot x range to 30-90, same for y and z\n\
  h/?           Print this information\n\
  q/x           Quit tsview\n"

#include "illuminator.h"
#include <sys/dir.h> /* For scandir(), alphasort, struct dirent */
#include <libgen.h>  /* For dirname(), basename() */
#include <string.h>  /* For strdup() */
#include <stdlib.h>            /* Needed for readline stuff below */
#include <term.h>              /* ncurses header for readline */
#include <readline/readline.h> /* For command line editing */
#include <readline/history.h>  /* For command line history */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) PetscPrintf (PETSC_COMM_WORLD, "%s: " fmt, __FUNCT__, args)
#else
#define DPRINTF(fmt, args...)
#endif

char *basefilename;


#undef __FUNCT__
#define __FUNCT__ "myfilter"

/*++++++++++++++++++++++++++++++++++++++
  This function returns non-zero for "qualifying" file names which start with
  the stored files' basename and end with
  +latex+{\tt .cpu0000.meta}.
  +html+ <tt>.cpu0000.meta</tt>.
  It is used as the
  +latex+{\tt select()} function for {\tt scandir()} in {\tt main()}.
  +html+ <tt>select()</tt> function for <tt>scandir()</tt> in <tt>main()</tt>.

  int myfilter Returns non-zero for qualifying filenames.

  const struct dirent *direntry Directory entry with filename to test.
  ++++++++++++++++++++++++++++++++++++++*/

int myfilter (const struct dirent *direntry)
{
  if ((!strncmp (direntry->d_name, basefilename, strlen(basefilename))))
    return (!strncmp (direntry->d_name + strlen(direntry->d_name) - 13,
		      ".cpu0000.meta", 13));
  return 0;
}


/*+++++++++++++++++++++++++++++++++++++

Functions for reading the command line
and avoiding reading empty lines

Probably this function is not Petsc
safe, but we'll see.

+++++++++++++++++++++++++++++++++++++*/

/* A static variable for holding the line. */
static char *line_read = (char *)NULL;

/* Read a string, and return a pointer to it.
   Returns NULL on EOF. */



char* rl_gets (char* message)
{
  /* If the buffer has already been allocated,
     return the memory to the free pool. */
  if (line_read)
    {
      free (line_read);
      line_read = (char *)NULL;
    }

  /* Get a line from the user. */
  line_read = readline (message);

  /* If the line has any text in it,
     save it on the history. */
  if (line_read && *line_read)
    add_history (line_read);

  return (line_read);
}


/*
  Failed attempt to make a Petsc safe readline
  Lefted here for reference

  It is based on PetscSynchronizedFGets, but instead of using
  fgets() it uses rl_gets()

*/

int PetscSynchronizedFReadline(MPI_Comm comm,char* message,char* string)
{
  int ierr,rank, len;
  PetscFunctionBegin;
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

  /* First processor prints immediately to stdin*/
  if (!rank) {
    string = rl_gets(message);
  }
  
  len = strlen(string);

  ierr = MPI_Bcast(string,len,MPI_BYTE,0,comm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "main"

/*++++++++++++++++++++++++++++++++++++++
  This is
  +latex+{\tt main()}.
  +html+ <tt>main()</tt>.

  int main It returns an int to the OS.

  int argc Argument count.

  char *argv[] Arguments.
  ++++++++++++++++++++++++++++++++++++++*/

int main (int argc, char *argv[])
{
  int total_entries, current_entry, dims, i, ierr, windowsize=300, plots=0,
    increment=1, xmin=0,xmax=-1, ymin=0,ymax=-1, zmin=0,zmax=-1;
  struct dirent **namelist;
  char **files, *thefilename, *filec, *dirc, *basedirname;
  PetscViewer theviewer;
  PetscTruth loaded = PETSC_FALSE, transp=PETSC_TRUE;

  if (argc<2)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Usage: tsview basename\n");
      CHKERRQ (ierr);
      return 1;
    }

  /*+ After
    +latex+{\tt PETSc}
    +html+ <tt>PETSc</tt>
    initialization, it gets the list of files matching the basename. +*/
  ierr = PetscInitialize (&argc, &argv, (char *)0, help); CHKERRQ (ierr);

  DPRINTF ("Command line:",0); CHKERRQ (ierr);
#ifdef DEBUG
  for (i=0; i<argc; i++)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, " %s", argv[i]); CHKERRQ (ierr);
    }
  ierr = PetscPrintf (PETSC_COMM_WORLD, "\n"); CHKERRQ (ierr);
#endif

  filec = strdup (argv[1]);
  dirc = strdup (argv[1]);
  basefilename = basename (filec);
  basedirname = dirname (dirc);

  ierr = PetscOptionsHasName (PETSC_NULL, "-no_transparency", &transp);
  CHKERRQ (ierr);
  transp = !transp;

  total_entries = scandir (basedirname, &namelist, myfilter, alphasort);
  if (!total_entries)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "No such files, exiting\n");
      CHKERRQ (ierr);
      exit (1);
    }
  if (total_entries < 0)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Error scanning directory %s\n",
			  basedirname); CHKERRQ (ierr);
      ierr = PetscFinalize (); CHKERRQ(ierr);
      return 1;
    }
  ierr = PetscPrintf (PETSC_COMM_WORLD, "%d eligible files:\n", total_entries);
  CHKERRQ (ierr);

  if (!(files = (char **) malloc (total_entries * sizeof (char *))))
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Error allocating memory\n");
      CHKERRQ (ierr);
      ierr = PetscFinalize (); CHKERRQ(ierr);
      return 1;
    }
  for (i=0; i<total_entries; i++)
    {
      int filength = strlen(namelist[i]->d_name);

      files [i] = (char *) malloc ((filength-12)*sizeof(char));
      strncpy (files [i], namelist[i]->d_name, filength-13);
      files [i] [filength-13] = '\0';
      free (namelist[i]);
      ierr = PetscPrintf (PETSC_COMM_WORLD, "[%d] %s\n", i, files [i]);
      CHKERRQ (ierr);
    }
  free (namelist);

  /*+In the main loop, the various timesteps are displayed, with options:
    +latex+\begin{itemize} \item
    +html+ <ul><li>
    A number jumps to that entry in the files table.
    +latex+\item {\stt <return>}
    +html+ <li><tt>&lt;return&gt;</tt>
    loads the next file.
    +latex+\item {\tt b}
    +html+ <li><tt>b</tt>
    goes back one file.
    +latex+\item {\tt q}
    +html+ <li><tt>q</tt>
    quits the program.
    +latex+\end{itemize}
    +html+ </ul>
    +*/
  current_entry=0;
  while (1)
    {
      DA theda;
      Vec global;
      int usermetacount=0, fields, display_field;
      char basis [strlen(argv[1]) + 20], **usermetanames, **usermetadata,
	*instring;
      PetscScalar minmax[6], plot_vals[6], plot_colors[24] =
	{ 1.,0.,0.,.5, 1.,1.,0.,.5, 0.,1.,0.,.5, 0.,1.,1.,.5, 0.,0.,1.,.5,
	  1.,0.,1.,.5 };
      field_plot_type *fieldtypes;
      ISurface Surf;
      IDisplay Disp;

      /* Load the vector */
      strcpy (basis, basedirname);
      strcat (basis, "/");
      strcat (basis, files[current_entry]);
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Loading entry %d, basename %s\n",
			  current_entry, basis);
      if (loaded)
	{
	  ierr = IlluMultiRead (PETSC_COMM_WORLD, theda, global, basis,
				&usermetacount, &usermetanames, &usermetadata);
	  CHKERRQ (ierr);
	}
      else
	{
	  DPRINTF ("Loading first timestep, creating distributed array\n",0);
	  display_field = 0;
	  minmax [0] = minmax [2] = minmax [4] = 0.;
	  minmax [1] = minmax [3] = minmax [5] = 1.;
	  ierr = IlluMultiLoad
	    (PETSC_COMM_WORLD, basis, &theda, minmax+1, minmax+3, minmax+5,
	     &fieldtypes, &usermetacount, &usermetanames, &usermetadata);
	  CHKERRQ (ierr);
	  ierr = DAGetGlobalVector (theda, &global); CHKERRQ (ierr);
	  loaded = PETSC_TRUE;

	  ierr = DAGetInfo (theda, &dims, PETSC_NULL,PETSC_NULL,PETSC_NULL,
			    PETSC_NULL,PETSC_NULL,PETSC_NULL, &fields,
			    PETSC_NULL,PETSC_NULL,PETSC_NULL); CHKERRQ (ierr);

	  /* Usermetadata xwidth, ywidth, zwidth override minmax in case
	     version is 0.1. */
	  for (i=0; i<usermetacount; i++)
	    {
	      if (!strncmp (usermetanames [i], "xwidth", 6))
		sscanf (usermetadata [i], "%lf", minmax+1);
	      else if (!strncmp (usermetanames [i], "ywidth", 6))
		sscanf (usermetadata [i], "%lf", minmax+3);
	      else if (!strncmp (usermetanames [i], "zwidth", 6))
		sscanf (usermetadata [i], "%lf", minmax+5);
	    }

	  if (dims<3)
	    {
	      int width=windowsize, height=windowsize;

	      ierr = PetscPrintf (PETSC_COMM_WORLD,
				  "For viewing 2-D data, try tsview-ng!\n");
	      CHKERRQ (ierr);

	      if (minmax[1]<minmax[3])
		width  *= minmax[1]/minmax[3];
	      else
		height *= minmax[3]/minmax[1];

	      ierr = PetscViewerDrawOpen
		(PETSC_COMM_WORLD, 0, "", PETSC_DECIDE, PETSC_DECIDE,
		 width, height, &theviewer); CHKERRQ (ierr);
	    }
	  else
	    {
#ifdef GEOMVIEW
	      ierr = SurfCreate (&Surf); CHKERRQ (ierr);
	      ierr = GeomviewBegin (PETSC_COMM_WORLD, &Disp); CHKERRQ (ierr);
#else
	      SETERRQ (PETSC_ERR_SUP,
		       "Built without Geomview, which is needed for 3-D");
#endif
	    }
	}

      /* Print user data */
      ierr = PetscPrintf (PETSC_COMM_WORLD, "User data:\n"); CHKERRQ (ierr);
      for (i=0; i<usermetacount; i++)
	{
	  ierr = PetscPrintf (PETSC_COMM_WORLD, "%s = %s\n", usermetanames [i],
			      usermetadata [i]); CHKERRQ (ierr);
	}

      /* View the vector */
      if (dims<3)
	{
	  ierr = VecView (global, theviewer); CHKERRQ (ierr);
	}
      else
	{
	  /*+ The Illuminator-based 3-D viewer can only display one field at a
	    time.  At the beginning, that is field 0, and is cycled using the
	    +latex+{\tt v}
	    +html+ <tt>v</tt>
	    command. +*/
	  PetscScalar minval, maxval;
	  char *fieldname;

	  ierr = VecStrideMin (global, display_field, PETSC_NULL, &minval);
	  CHKERRQ (ierr);
	  ierr = VecStrideMax (global, display_field, PETSC_NULL, &maxval);
	  CHKERRQ (ierr);
	  ierr = DAGetFieldName (theda, display_field, &fieldname);
	  CHKERRQ (ierr);
	  ierr = PetscPrintf (PETSC_COMM_WORLD,
			      "Displaying field %d [%g-%g]: %s\n",
			      display_field, minval, maxval, fieldname);
	  CHKERRQ (ierr);

	  DPRINTF ("Calculating triangle locations\n",0);
	  if (plots)
	    {
	      ierr = DATriangulateRange (Surf, theda, global, display_field,
					 minmax, plots, plot_vals, plot_colors,
					 xmin,xmax, ymin,ymax, zmin,zmax);
	      CHKERRQ (ierr);
	    }
	  else
	    {
	      ierr = DATriangulateRange (Surf, theda, global, display_field,
					 minmax, PETSC_DECIDE, PETSC_NULL,
					 PETSC_NULL, xmin,xmax, ymin,ymax,
					 zmin,zmax);
	      CHKERRQ (ierr);
	    }
#ifdef GEOMVIEW
	  DPRINTF ("Consolidating triangles on head node and visualizing\n",0);
	  ierr = GeomviewDisplayTriangulation
	    (PETSC_COMM_WORLD, Surf, Disp, minmax, fieldname, transp);
	  CHKERRQ (ierr);
#endif
	  ierr = SurfClear (Surf); CHKERRQ (ierr);
	}

      /* Free user data */
      for (i=0; i<usermetacount; i++)
	{
	  free (usermetanames [i]);
	  free (usermetadata [i]);
	}
      free (usermetanames);
      free (usermetadata);

      /* Get user input */
      /* ierr = PetscPrintf (PETSC_COMM_WORLD, "What to do? (h for options) ");
      CHKERRQ (ierr);
      ierr = PetscSynchronizedFGets (PETSC_COMM_WORLD, stdin, 99, instring);
      CHKERRQ (ierr); */
      /* This is probably not PETSc-safe */
      instring = rl_gets("What to do? (h for options)> ");

      switch (instring [0])
	{
	case 'q':
	case 'Q':
	case 'x':
	case 'X':
	  {
	    if (dims < 3)
	      {
		ierr = PetscViewerDestroy (theviewer); CHKERRQ (ierr);
	      }
	    else
	      {
#ifdef GEOMVIEW
		ierr = GeomviewEnd (PETSC_COMM_WORLD, Disp); CHKERRQ (ierr);
#endif
		ierr = ISurfDestroy (Surf); CHKERRQ (ierr);
	      }
	    ierr = PetscFinalize(); CHKERRQ (ierr);
	    return 0;
	  }
	case 't':
	case 'T':
	  {
	    transp=!transp;
	    break;
	  }
	case 'h':
	case 'H':
	case '?':
	  {
	    ierr = PetscPrintf (PETSC_COMM_WORLD, HELP_STRING);
	    break;
	  }
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	  {
	    current_entry = atoi (instring);
	    break;
	  }
	case 'b':
	case 'B':
	  {
	    current_entry--;
	    break;
	  }
	case 'i':
	case 'I':
	  {
	    /* printf ("instring=\"%s\"\n",instring); */
	    if (instring[1] && instring[2])
	      {
		sscanf (instring, "i %d", &increment);
	      }
	    else
	      {
		ierr=PetscPrintf (PETSC_COMM_WORLD,
				  "Increment: %d\n",increment);
		CHKERRQ (ierr);
	      }
	    break;
	  }
	case 'v':
	case 'V':
	  {
	    if (dims == 3)
	      display_field = (display_field+1) % fields;
	    break;
	  }
#ifdef GEOMVIEW
	case 'd':
	case 'D':
	  {
	    char filename [200], number[10];

	    if (dims<3)
	      {
		ierr=PetscPrintf (PETSC_COMM_WORLD,
				  "The 'd' command is for 3-D only.\n");
		CHKERRQ (ierr);
		break;
	      }

	    strncpy (filename, basedirname, 198);
	    strcat (filename, "/");
	    strncat (filename, files [current_entry], 198 - strlen (filename));
	    snprintf (number, 9, "-f%d", display_field);
	    strncat (filename, number, 198 - strlen (filename));
	    strncat (filename, ".ppm", 198 - strlen (filename));

	    DPRINTF ("Saving image with filename %s\n", filename);
	    IDispWritePPM (Disp, filename);
	    break;
	  }
	case 'a':
	case 'A':
	  {
	    char filename [200], number[10], *fieldname;
	    int entry;

	    if (dims<3)
	      {
		ierr=PetscPrintf (PETSC_COMM_WORLD,
				  "The 'a' command is for 3-D only.\n");
		CHKERRQ (ierr);
		break;
	      }

	    for (entry=(instring[1]?atoi(instring+1):0); entry<total_entries;
		 entry++)
	      {
		ierr=PetscPrintf
		  (PETSC_COMM_WORLD, "Loading   file %d/%d\r", entry, total_entries);
		strcpy (basis, basedirname);
		strcat (basis, "/");
		strcat (basis, files[entry]);
		ierr = IlluMultiRead (PETSC_COMM_WORLD, theda, global, basis,
				      &usermetacount, &usermetanames,
				      &usermetadata); CHKERRQ (ierr);

		ierr=PetscPrintf
		  (PETSC_COMM_WORLD, "Rendering file %d/%d\r", entry, total_entries);
		ierr = DATriangulateRange
		  (Surf, theda, global, display_field, minmax,
		   plots?plots:PETSC_DECIDE, plots?plot_vals:PETSC_NULL,
		   plots?plot_colors:PETSC_NULL, xmin,xmax, ymin,ymax,
		   zmin,zmax); CHKERRQ (ierr);
		ierr = DAGetFieldName (theda, display_field, &fieldname);
		CHKERRQ (ierr);
		ierr = GeomviewDisplayTriangulation
		  (PETSC_COMM_WORLD, Surf, Disp, minmax, fieldname, transp);
		CHKERRQ (ierr);
		ierr = SurfClear (Surf); CHKERRQ (ierr);

		ierr=PetscPrintf
		  (PETSC_COMM_WORLD, "Dumping   file %d/%d\r", entry, total_entries);
		strncpy (filename, basedirname, 198);
		strcat (filename, "/");
		strncat (filename, files [entry], 198 - strlen (filename));
		snprintf (number, 9, "-f%d", display_field);
		strncat (filename, number, 198 - strlen (filename));
		strncat (filename, ".ppm", 198 - strlen (filename));
		IDispWritePPM (Disp, filename);
	      }
	    DPRINTF ("\n",0);
	    break;
	  }
#endif
	case 'r':
	case 'R':
	  {
	    total_entries = scandir (basedirname, &namelist, myfilter,
				     alphasort);

	    if (!(files = (char **) realloc (files,total_entries * sizeof (char *))))
	      {
		ierr = PetscPrintf (PETSC_COMM_WORLD, "Error allocating memory\n");
		CHKERRQ (ierr);
		ierr = PetscFinalize (); CHKERRQ(ierr);
		return 1;
	      }
	    for (i=0; i<total_entries; i++)
	      {
		int filength = strlen(namelist[i]->d_name);
	
		files [i] = (char *) malloc ((filength-12)*sizeof(char));
		strncpy (files [i], namelist[i]->d_name, filength-13);
		files [i] [filength-13] = '\0';
		free (namelist[i]);
		ierr = PetscPrintf (PETSC_COMM_WORLD, "[%d] %s\n", i, files [i]);
		CHKERRQ (ierr);
	      }
	    free (namelist);
	    
	    ierr = PetscPrintf (PETSC_COMM_WORLD, "Total Entries: %d\n",
				total_entries);
	    CHKERRQ (ierr);
	    break;
	  }
	case 's':
	case 'S':
	  {
	    if (instring[1] && instring[2] && dims<3)
	      {
		sscanf (instring+2, "%d", &windowsize);

		if (windowsize)
		  {
		    int width=windowsize, height=windowsize;

		    ierr = PetscViewerDestroy (theviewer); CHKERRQ (ierr);

		    if (minmax[1]<minmax[3])
		      width  *= minmax[1]/minmax[3];
		    else
		      height *= minmax[3]/minmax[1];

		    ierr = PetscViewerDrawOpen
		      (PETSC_COMM_WORLD, 0, "", PETSC_DECIDE, PETSC_DECIDE,
		       width, height, &theviewer); CHKERRQ (ierr);
		  }
		else
		  {
		    ierr=PetscPrintf (PETSC_COMM_WORLD,
				      "Usage: \"s ###\" (2-D only)\n");
		    CHKERRQ (ierr);
		  }
	      }
	    else
	      {
		ierr=PetscPrintf (PETSC_COMM_WORLD,
				  "Usage: \"s ###\" (2-D only)\n");
		CHKERRQ (ierr);
	      }
	    break;
	  }
	case 'p':
	case 'P':
	  {
	    int count=0, newplots=0;

	    if (dims<3)
	      {
		ierr=PetscPrintf (PETSC_COMM_WORLD,
				  "The 'p' command is for 3-D only.\n");
		CHKERRQ (ierr);
		break;
	      }

	    if (instring[1]=='\0' || instring[2]=='\0')
	      {
		ierr = PetscPrintf (PETSC_COMM_WORLD,
				    "Current plot contour isoquants:");
		CHKERRQ (ierr);
		if (plots == 0)
		  {
		    ierr = PetscPrintf (PETSC_COMM_WORLD,
					" auto (20%%, 40%%, 60%%, 80%%)");
		    CHKERRQ (ierr);
		  }
		for (count=0; count<plots; count++)
		  {
		    ierr = PetscPrintf (PETSC_COMM_WORLD, " %g",
					plot_vals[count]); CHKERRQ (ierr);
		  }
		ierr = PetscPrintf (PETSC_COMM_WORLD, "\n"); CHKERRQ (ierr);
		break;
	      }

	    while (newplots<6 && instring[count] != '\0')
	      {
		while ((instring[count] < '0' || instring[count] > '9') &&
		       instring[count] != '-' && instring[count] != '.' &&
		       instring[count] != '\0')
		  count++;

		if (instring[count])
		  {
#if defined(PETSC_USE_SINGLE)
		    sscanf (instring+count, "%f", plot_vals+newplots);
#else
		    sscanf (instring+count, "%lf", plot_vals+newplots);
#endif
		    newplots++;
		    while ((instring[count] >= '0' && instring[count] <= '9')||
			   instring[count] == '-' || instring[count] == '.')
		      count++;
		  }
	      }
	    plots = newplots;
	    break;
	  }
	case 'c':
	case 'C':
	  {
	    if (instring[1] == 'x' || instring[1] == 'X')
	      xmax = (xmax == -2) ? -1 : -2;
	    if (instring[1] == 'y' || instring[1] == 'Y')
	      ymax = (ymax == -2) ? -1 : -2;
	    if (instring[1] == 'z' || instring[1] == 'Z')
	      zmax = (zmax == -2) ? -1 : -2;
	    DPRINTF ("x %d-%d, y %d-%d, z %d-%d\n", xmin, xmax, ymin, ymax,
		     zmin, zmax);
	    break;
	  }
	case 'g':
	case 'G':
	  {
	    int mingrid, maxgrid;
	    sscanf (instring+2, "%d-%d", &mingrid, &maxgrid);
	    if (instring[1] == 'x' || instring[1] == 'X')
	      {
		xmin = mingrid;
		xmax = maxgrid;
	      }
	    if (instring[1] == 'y' || instring[1] == 'Y')
	      {
		ymin = mingrid;
		ymax = maxgrid;
	      }
	    if (instring[1] == 'z' || instring[1] == 'Z')
	      {
		zmin = mingrid;
		zmax = maxgrid;
	      }
	    DPRINTF ("x %d-%d, y %d-%d, z %d-%d\n", xmin, xmax, ymin, ymax,
		     zmin, zmax);
	    break;
	  }
	default:
	  current_entry+=increment;
	}
      if (current_entry < 0)
	current_entry = total_entries-1;
      if (current_entry >= total_entries)
	current_entry = 0;
    }

  free (filec);
  free (dirc);
}
