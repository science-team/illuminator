/***************************************
  $Header$

  This program views the output of a time series saved using
  +latex+{\tt IlluMultiSave()}.
  +html+ <tt>IlluMultiSave()</tt>.
  It basically just switches between timesteps; future versions may be more
  interesting.  The neat part of it is that it loads multiprocessor data and
  displays it on a single CPU.
  ***************************************/

static char help[] = "Displays the output of of a timestep series saved using IlluMultiSave().\n\
Usage:\n\
\n\
  tsview <basename> [-no_transparency]\n\
\n\
Then interactively flip through the timesteps (h or ? lists commands).\n";

#include "illuminator.h"
#include <glade/glade.h>
#include <gnome.h>
#include <libgnomeui/libgnomeui.h>
#include <sys/dir.h> /* For scandir(), alphasort, struct dirent */
#include <libgen.h>  /* For dirname(), basename() */
#include <string.h>  /* For strdup() */

/* Build with -DDEBUG for debugging output */
#undef DPRINTF
#ifdef DEBUG
#define DPRINTF(fmt, args...) PetscPrintf (PETSC_COMM_WORLD, "%s: " fmt, __FUNCT__, args)
#else
#define DPRINTF(fmt, args...)
#endif

/*+ Declared in illuminator.c, these give the current number of triangles on
  this node and corner coordinates and color information for each triangle. +*/

GladeXML *xml;
ISurface Surf;
IDisplay Disp [1] = { NULL };

/* Filename list and time/log info */
int entrynum=0, total_entries=0, current_timestep;
char *the_basename, *basedirname, **stepnames=NULL;
double current_time;
char *log_text=NULL;

/* Ternary diffusion path color, and supplementary diffusion path data: there
   are dp_supp_colors colors, each corresponding to a color and a number of
   points in the big A and B arrays. */
PetscScalar ternary_dp_color[4] = { 0.,0.,0.,1. };
int dp_supp_colors=0, *dp_supp_color_points=NULL;
PetscScalar *dp_supp_red=NULL, *dp_supp_green=NULL, *dp_supp_blue=NULL,
  *dp_supp_alpha=NULL;
PetscScalar *dp_supp_AB=NULL;

/* Window parameters and drawables */
#define DEFAULT_RENDER_SIZE 300
#define SCALE_BPP 3
int width=0, height=0, bpp=3, scale_size, nx, ny, transform, dataview_count=1;
GtkWidget *dataviews [1];
IDisplay scalar_disp, ternary_square_disp, ternary_triangle_disp, ternary_disp,
  vector_disp, shear_disp;
gboolean scalar_auto_set=TRUE, ternary_auto_set=TRUE, vector_auto_set=TRUE,
  shear_auto_set=TRUE;
gdouble sizemag;
PetscTruth transp;

typedef enum {
  GEN_TRI=0, UNIT_TRI, EQ_TRI, GEN_RECT, UNIT_SQ, SQUARE
} terntype;
terntype lastern, thistern;

/* PETSc structures etc. */
DA theda;
Vec global;
PetscScalar minmax[6] = { 0.,1., 0.,1., 0.,1. };
PetscScalar scales[15]; /* 2 scalar, 6 tritern, 4 sqtern, 2 vector, 1 tensor */
field_plot_type *fieldtypes;
int dimensions, num_fields, current_field, *field_index, num_variables[1],
  **variable_indices;

/* First some primary functions which do stuff, then callbacks, then main(). */

#undef __FUNCT__
#define __FUNCT__ "render_dataviews"

/* Width and height are reversed if rotated, so use these macros with render */
#define RENDER_WIDTH ((transform & ROTATE_LEFT) ? height : width)
#define RENDER_HEIGHT ((transform & ROTATE_LEFT) ? width : height)

int render_dataviews ()
{
  int viewnum, nx,ny,nz, ierr;

  DPRINTF ("Rendering dataviews\n",0);
  if (dataview_count != 1)
    {
      printf ("dataview_count != 1 is not yet supported\n");
      exit(0);
    }
  for (viewnum=0; viewnum<dataview_count; viewnum++)
    {
      int nx,ny, xs,ys, xm,ym, i;
      PetscScalar minval, maxval, refmag, *global_array;
      GtkType type;
      char thestatus [100];

      /* (Re)allocate buffer */
      if (!Disp[0])
	{
	  DPRINTF ("Allocating IDisplay RGB buffer: %dx%dx%d\n",
		   width,height,bpp);
	  if (IDispCreate (Disp, width, height, width, bpp, 0)) {
	    printf ("ERROR: can't allocate RGB buffer\n");
	    exit (1); }
	}
      else
	{
	  DPRINTF ("Reallocating IDisplay RGB buffer: %dx%dx%d\n",
		   width,height,bpp);
	  if (IDispResize (Disp[0], width, height, width, bpp, 0)) {
	    printf ("ERROR: can't reallocate RGB buffer\n");
	    exit (1); }
	}

      /* Make sure the drawing area is the right size */
      DPRINTF ("(Re)sizing drawing area to %dx%d\n",width,height);
      if (!dataviews [viewnum])
	dataviews [viewnum] = glade_xml_get_widget (xml, "plot_area");
      gtk_drawing_area_size (GTK_DRAWING_AREA (dataviews[viewnum]),
			     width, height);

      /* Render into Disp [viewnum] */
      ierr = DAGetInfo (theda, PETSC_NULL, &nx,&ny,PETSC_NULL,
			PETSC_NULL,PETSC_NULL,PETSC_NULL, PETSC_NULL,
			PETSC_NULL,PETSC_NULL,PETSC_NULL); CHKERRQ (ierr);
      ierr = DAGetCorners (theda, &xs,&ys,PETSC_NULL, &xm,&ym,PETSC_NULL);
      CHKERRQ (ierr);

      DPRINTF ("Rendering %dx%d buffer with transform %d\n", RENDER_WIDTH,
	       RENDER_HEIGHT, transform);
      if (dimensions == 2)
	{
	  char maxes [6][20];

	  ierr = VecGetArray (global, &global_array); CHKERRQ (ierr);

	  /* Plot with automatic or manual scaling, as appropriate */
	  switch (fieldtypes [current_field])
	    {
	    case FIELD_SCALAR:
	      if (scalar_auto_set)
		{
		  auto_scale (global_array, xm*ym, num_fields, current_field,
			      FIELD_SCALAR, 2, scales);
		  snprintf (maxes[0], 19, "%g", scales[0]);
		  snprintf (maxes[1], 19, "%g", scales[1]);
		  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						 (xml, "scalar_min_entry")),
				      maxes[0]);
		  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						 (xml, "scalar_max_entry")),
				      maxes[1]);
		}
	      ierr = render_rgb_local_2d
		(Disp[0], global_array, num_fields,current_field, FIELD_SCALAR,
		 scales, nx,ny, xs,ys, xm,ym, transform, NULL,0,0,0,0);
	      CHKERRQ (ierr);
	      break;

	    case FIELD_TERNARY:
	    case FIELD_TERNARY_SQUARE:
	      if (thistern==GEN_TRI || thistern==UNIT_TRI || thistern==EQ_TRI)
		{
		  GtkWidget *ternary_scale_area = glade_xml_get_widget
		    (xml, "ternary_scale_area");
		  int color, point, i;
		  gboolean update_ternary_entries=FALSE;

		  if (ternary_auto_set)
		    {
		      auto_scale (global_array, xm*ym, num_fields,
				  current_field, FIELD_TERNARY, 2, scales+2);
		      update_ternary_entries=TRUE;
		    }
		  if (thistern==UNIT_TRI)
		    {
		      scales[2] = scales[3] = scales[5] = scales[6] = 0.;
		      scales[4] = scales[7] = 1.;
		      update_ternary_entries=TRUE;
		    }
		  if (thistern==EQ_TRI)
		    {
		      scales[5] = scales[3];
		      scales[6] = scales[2];
		      scales[7] = scales[3] + scales[4]-scales[2];
		      update_ternary_entries=TRUE;
		    }
		  if (update_ternary_entries)
		    {
		      snprintf (maxes[0], 19, "%g", scales[2]);
		      snprintf (maxes[1], 19, "%g", scales[3]);
		      snprintf (maxes[2], 19, "%g", scales[4]);
		      snprintf (maxes[3], 19, "%g", scales[5]);
		      snprintf (maxes[4], 19, "%g", scales[6]);
		      snprintf (maxes[5], 19, "%g", scales[7]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_1A_entry")),
					  maxes[0]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_1B_entry")),
					  maxes[1]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_2A_entry")),
					  maxes[2]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget 
						     (xml,"ternary_2B_entry")),
					  maxes[3]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_3A_entry")),
					  maxes[4]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_3B_entry")),
					  maxes[5]);
		    }

		  /* Start with blank ternary scale */
		  render_scale_2d (ternary_triangle_disp, FIELD_TERNARY, 1);
		  /* Overlay diffusion path supplements on scale */
		  for (color=0, i=0; color<dp_supp_colors;
		       i+=dp_supp_color_points[color++])
		    render_composition_path
		      (ternary_triangle_disp, dp_supp_AB+i*2,
		       dp_supp_color_points[color], 2, FIELD_TERNARY, scales+2,
		       dp_supp_red[color], dp_supp_green[color],
		       dp_supp_blue[color], dp_supp_alpha[color]);
		  /* Render diagram, add diffusion path to scale */
		  ierr = render_rgb_local_2d
		    (Disp[0], global_array, num_fields, current_field,
		     FIELD_TERNARY, scales+2, nx,ny, xs,ys, xm,ym, transform,
		     ternary_triangle_disp,
		     ternary_dp_color[0],ternary_dp_color[1],
		     ternary_dp_color[2],ternary_dp_color[3]);
		  CHKERRQ (ierr);
		  IDispDrawGdk (ternary_triangle_disp, ternary_scale_area,
				GDK_RGB_DITHER_MAX);
		}
	      else /* Ternary square */
		{
		  GtkWidget *ternary_scale_area = glade_xml_get_widget
		    (xml, "ternary_scale_area");
		  gboolean update_ternary_entries=FALSE;

		  if (ternary_auto_set)
		    {
		      auto_scale (global_array, xm*ym, num_fields,
				  current_field, FIELD_TERNARY_SQUARE, 2,
				  scales+8);
		      update_ternary_entries=TRUE;
		    }
		  if (thistern==UNIT_SQ)
		    {
		      scales[8] = scales[10] = 0.;
		      scales[9] = scales[11] = 1.;
		      update_ternary_entries=TRUE;
		    }
		  if (thistern==SQUARE)
		    {
		      scales[11] = scales[10] + scales[9]-scales[8];
		      update_ternary_entries=TRUE;
		    }
		  if (update_ternary_entries)
		    {		  
		      snprintf (maxes[0], 19, "%g", scales[8]);
		      snprintf (maxes[1], 19, "%g", scales[9]);
		      snprintf (maxes[2], 19, "%g", scales[10]);
		      snprintf (maxes[3], 19, "%g", scales[11]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_1A_entry")),
					  maxes[0]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_1B_entry")),
					  maxes[1]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						     (xml,"ternary_2A_entry")),
					  maxes[2]);
		      gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget 
						     (xml,"ternary_2B_entry")),
					  maxes[3]);
		    }

		  render_scale_2d (ternary_square_disp,FIELD_TERNARY_SQUARE,1);
		  ierr = render_rgb_local_2d
		    (Disp[0], global_array, num_fields, current_field,
		     FIELD_TERNARY_SQUARE, scales+8, nx,ny, xs,ys, xm,ym,
		     transform, ternary_square_disp,
		     ternary_dp_color[0], ternary_dp_color[1],
		     ternary_dp_color[2], ternary_dp_color[3]); CHKERRQ (ierr);
		  IDispDrawGdk (ternary_square_disp, ternary_scale_area,
				GDK_RGB_DITHER_MAX);
		}
	      break;

	    case FIELD_VECTOR:
	      if (vector_auto_set)
		{
		  auto_scale (global_array, xm*ym, num_fields, current_field,
			      FIELD_VECTOR, 2, scales+12);
		  snprintf (maxes[0], 19, "%g", scales[13]);
		  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						 (xml, "vector_max_entry")),
				      maxes[0]);
		}
	      ierr = render_rgb_local_2d
		(Disp[0], global_array, num_fields, current_field,
		 FIELD_VECTOR, scales+12, nx,ny, xs,ys, xm,ym, transform,
		 NULL,0,0,0,0); CHKERRQ (ierr);
	      break;

	    case FIELD_TENSOR_SHEAR:
	      if (shear_auto_set)
		{
		  auto_scale (global_array, xm*ym, num_fields, current_field,
			      fieldtypes [current_field], 2, scales+14);
		  snprintf (maxes[0], 19, "%g", scales[14]);
		  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
						 (xml, "shear_max_entry")),
				      maxes[0]);
		}
	      ierr = render_rgb_local_2d
		(Disp[0], global_array, num_fields, current_field,
		 FIELD_TENSOR_SHEAR,scales+14, nx,ny, xs,ys, xm,ym, transform,
		 NULL,0,0,0,0); CHKERRQ (ierr);
	      break;
	    }

	  ierr = VecRestoreArray (global, &global_array); CHKERRQ (ierr);
	}
      else /* Three dimensions */
	{
	  /* Fixed negative z-direction for now */
	  PetscScalar eye[3], dir[3] = {0, 0, -1.}, right[3] = {.5, 0., 0.};
	  guchar bgcolor[4] = { 0x50, 0x50, 0x50, 0xFF };
	  eye[0] = 0.5*(minmax[0]+minmax[1]);
	  eye[1] = 0.5*(minmax[2]+minmax[3]);
	  eye[2] = minmax[5] + minmax[1]-minmax[0];

	  if (!Surf)
	    ierr = SurfCreate (&Surf); CHKERRQ (ierr);

	  ierr = DATriangulate
	    (Surf, theda, global, current_field, minmax, PETSC_DECIDE,
	     PETSC_NULL, PETSC_NULL, PETSC_FALSE, PETSC_FALSE, PETSC_FALSE);
	  CHKERRQ(ierr);

	  IDispFill (Disp[0], bgcolor);
	  ierr = render_rgb_local_3d (Disp, Surf, eye, dir, right);
	  ierr = SurfClear (Surf); CHKERRQ (ierr);
	}

      /* Draw IDisplay object onto window */
      DPRINTF ("Painting %dx%d rgb%s buffer onto window\n",
	       RENDER_WIDTH, RENDER_HEIGHT, (bpp==4) ? "_32" : "");
      IDispDrawGdk (Disp[0], dataviews [viewnum], GDK_RGB_DITHER_MAX);
    }
}


/*+ Little variable for my_*_filter() and refresh_stepnames(). +*/
static char *basefilename;

#undef __FUNCT__
#define __FUNCT__ "my_time_filter"

/*++++++++++++++++++++++++++++++++++++++
  This function returns non-zero for "qualifying" file names which start with
  the stored files' basename.time and end with
  +latex+{\tt .cpu0000.meta}.
  +html+ <tt>.cpu0000.meta</tt>.
  It is used as the
  +latex+{\tt select()} function for {\tt scandir()} in {\tt main()}.
  +html+ <tt>select()</tt> function for <tt>scandir()</tt> in <tt>main()</tt>.

  int my_time_filter Returns non-zero for qualifying filenames.

  const struct dirent *direntry Directory entry with filename to test.
  ++++++++++++++++++++++++++++++++++++++*/

int my_time_filter (const struct dirent *direntry)
{
  if ((!strncmp (direntry->d_name, basefilename, strlen(basefilename))) &&
      (!strncmp (direntry->d_name + strlen(basefilename), ".time", 5)))
    return (!strncmp (direntry->d_name + strlen(direntry->d_name) - 13,
		      ".cpu0000.meta", 13));
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "my_time_filter"

/*++++++++++++++++++++++++++++++++++++++
  This function returns non-zero for "qualifying" file names which start with
  the stored files' basename and end with
  +latex+{\tt .cpu0000.meta}.
  +html+ <tt>.cpu0000.meta</tt>.
  It is used as the
  +latex+{\tt select()} function for {\tt scandir()} in {\tt main()}.
  +html+ <tt>select()</tt> function for <tt>scandir()</tt> in <tt>main()</tt>.

  int my_notime_filter Returns non-zero for qualifying filenames.

  const struct dirent *direntry Directory entry with filename to test.
  ++++++++++++++++++++++++++++++++++++++*/

int my_notime_filter (const struct dirent *direntry)
{
  if ((!strncmp (direntry->d_name, basefilename, strlen(basefilename))))
    return (!strncmp (direntry->d_name + strlen(direntry->d_name) - 13,
		      ".cpu0000.meta", 13));
  return 0;
}


/*++++++++++++++++++++++++++++++++++++++
  This loads the names of the files into a long list.
  ++++++++++++++++++++++++++++++++++++++*/

int refresh_stepnames ()
{
  struct dirent **namelist;
  char *filec, *dirc, totalsteps[14];
  int i, ierr;

  filec = strdup (the_basename);
  dirc = strdup (the_basename);
  basefilename = basename (filec);
  basedirname = dirname (dirc);

  /* Default: scan for a timestep sequence */
  total_entries = scandir (basedirname, &namelist, my_time_filter, alphasort);
  if (total_entries < 0)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Error scanning directory %s\n",
			  basedirname); CHKERRQ (ierr);
      return 1;
    }
  if (!total_entries)
    {
      /* Perhaps this is not a timestep sequence */
      total_entries = scandir (basedirname, &namelist, my_notime_filter,
			       alphasort);
      if (total_entries < 0)
	{
	  ierr= PetscPrintf (PETSC_COMM_WORLD, "Error scanning directory %s\n",
			      basedirname); CHKERRQ (ierr);
	  return 1;
	}
      if (!total_entries)
	{
	  ierr = PetscPrintf (PETSC_COMM_WORLD, "No such files\n");
	  CHKERRQ (ierr);
	  return 1;
	}
    }
  DPRINTF ("%d eligible files:\n", total_entries);
  snprintf (totalsteps, 14, "/%d", total_entries-1);
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "total_label")), totalsteps);

  if (!(stepnames = (char **) realloc
	(stepnames, total_entries*sizeof (char *))))
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Error allocating memory\n");
      CHKERRQ (ierr);
      return 1;
    }
  for (i=0; i<total_entries; i++)
    {
      int filength = strlen(namelist[i]->d_name);

      stepnames [i] = (char *) malloc ((filength-12)*sizeof(char));
      strncpy (stepnames [i], namelist[i]->d_name, filength-13);
      stepnames [i] [filength-13] = '\0';
      free (namelist[i]);
      DPRINTF ("[%d] %s\n", i, stepnames [i]);
      if (ierr)
	printf ("refresh_stepnames() Error: %d\n", ierr);
      /* CHKERRQ (ierr); */
    }

  free (namelist);
  return 0;
}

void on_plot_area_expose_event
(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{ IDispDrawGdk (Disp[0], widget, GDK_RGB_DITHER_MAX); }

void on_scalar_scale_area_expose_event
(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{ IDispDrawGdk (scalar_disp, widget, GDK_RGB_DITHER_MAX); }

void on_ternary_scale_area_expose_event
(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{ IDispDrawGdk (ternary_disp, widget, GDK_RGB_DITHER_MAX); }

void on_vector_scale_area_expose_event
(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{ IDispDrawGdk (vector_disp, widget, GDK_RGB_DITHER_MAX); }

void on_shear_scale_area_expose_event
(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{ IDispDrawGdk (shear_disp, widget, GDK_RGB_DITHER_MAX); }

void on_scale_size_entry_activate (GtkWidget *theentry, gpointer user) {
  G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  guchar black [4] = { 0, 0, 0, 1 };
  GtkWidget *scalar_scale_area, *ternary_scale_area, *vector_scale_area,
    *shear_scale_area;

  sscanf (entrytext, "%d", &scale_size);

  if (IDispResize (scalar_disp, scale_size,scale_size,scale_size,SCALE_BPP, 0))
    { printf ("ERROR: can't reallocate scale RGB buffer\n"); exit (1); }
  if (IDispResize (ternary_triangle_disp, scale_size,scale_size,scale_size,
		   SCALE_BPP,0))
    { printf ("ERROR: can't reallocate scale RGB buffer\n"); exit (1); }
  if (IDispResize (ternary_square_disp, scale_size,scale_size,scale_size,
		   SCALE_BPP,0))
    { printf ("ERROR: can't reallocate scale RGB buffer\n"); exit (1); }
  if (IDispResize (vector_disp, scale_size,scale_size,scale_size,SCALE_BPP, 0))
    { printf ("ERROR: can't reallocate scale RGB buffer\n"); exit (1); }
  if (IDispResize (shear_disp, scale_size,scale_size,scale_size,SCALE_BPP, 0))
    { printf ("ERROR: can't reallocate scale RGB buffer\n"); exit (1); }

  IDispFill (scalar_disp, black);
  IDispFill (ternary_disp, black);
  IDispFill (vector_disp, black);
  IDispFill (shear_disp, black);
  render_scale_2d (scalar_disp, FIELD_SCALAR, 1);
  render_scale_2d (ternary_disp, FIELD_TERNARY, 1);
  render_scale_2d (vector_disp, FIELD_VECTOR, 1);
  render_scale_2d (shear_disp, FIELD_TENSOR_SHEAR, 1);

  gtk_drawing_area_size
    (GTK_DRAWING_AREA (glade_xml_get_widget (xml, "scalar_scale_area")),
     scale_size, scale_size);
  gtk_drawing_area_size
    (GTK_DRAWING_AREA (glade_xml_get_widget (xml, "ternary_scale_area")),
     scale_size, scale_size);
  gtk_drawing_area_size
    (GTK_DRAWING_AREA (glade_xml_get_widget (xml, "vector_scale_area")),
     scale_size, scale_size);
  gtk_drawing_area_size
    (GTK_DRAWING_AREA (glade_xml_get_widget (xml, "shear_scale_area")),
     scale_size, scale_size);

  render_dataviews ();
}

void on_scalar_auto_checkbutton_toggled
(GtkWidget *thebutton, gpointer user_data)
{
  scalar_auto_set = gtk_toggle_button_get_active
    (GTK_TOGGLE_BUTTON (thebutton));
  if (scalar_auto_set)
    {
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_min_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_max_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_min_entry"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_max_entry"), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_min_label"), TRUE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_max_label"), TRUE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_min_entry"), TRUE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "scalar_max_entry"), TRUE);
    }
}

void on_ternary_auto_checkbutton_toggled
(GtkWidget *thebutton, gpointer user_data)
{
  ternary_auto_set = gtk_toggle_button_get_active
    (GTK_TOGGLE_BUTTON (thebutton));
  if (ternary_auto_set)
    {
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_1A_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_1B_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_2A_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_2B_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_3A_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_3B_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_1A_entry"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_1B_entry"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_2A_entry"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_2B_entry"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_3A_entry"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "ternary_3B_entry"), FALSE);
    }
  else
    {
      if (thistern==GEN_TRI || thistern==EQ_TRI || thistern==GEN_RECT ||
	  thistern==SQUARE)
	{
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1A_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1B_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2A_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1A_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1B_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2A_entry"), TRUE);
	}
      if (thistern==GEN_TRI || thistern==GEN_RECT)
	{
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2B_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3A_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3B_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2B_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3A_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3B_entry"), TRUE);
	}
    }
}

void triangle_to_square ()
{
  char maxes [6][20];

  gtk_widget_hide (glade_xml_get_widget (xml, "ternary_3A_label"));
  gtk_widget_hide (glade_xml_get_widget (xml, "ternary_3A_entry"));
  gtk_widget_hide (glade_xml_get_widget (xml, "ternary_3B_label"));
  gtk_widget_hide (glade_xml_get_widget (xml, "ternary_3B_entry"));
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_1A_label")), "Min A");
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_1B_label")), "Max A");
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_2A_label")), "Min B");
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_2B_label")), "Max B");
  snprintf (maxes[0], 19, "%g", scales[8]);
  snprintf (maxes[1], 19, "%g", scales[9]);
  snprintf (maxes[2], 19, "%g", scales[10]);
  snprintf (maxes[3], 19, "%g", scales[11]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_1A_entry")), maxes[0]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_1B_entry")), maxes[1]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_2A_entry")), maxes[2]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget 
				 (xml, "ternary_2B_entry")), maxes[3]);

  ternary_disp = ternary_square_disp;
}

void square_to_triangle ()
{
  char maxes [6][20];

  gtk_widget_show (glade_xml_get_widget (xml, "ternary_3A_label"));
  gtk_widget_show (glade_xml_get_widget (xml, "ternary_3A_entry"));
  gtk_widget_show (glade_xml_get_widget (xml, "ternary_3B_label"));
  gtk_widget_show (glade_xml_get_widget (xml, "ternary_3B_entry"));
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_1A_label")), "Red A");
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_1B_label")), "Red B");
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_2A_label")), "Green A");
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget
				 (xml, "ternary_2B_label")), "Green B");
  snprintf (maxes[0], 19, "%g", scales[2]);
  snprintf (maxes[1], 19, "%g", scales[3]);
  snprintf (maxes[2], 19, "%g", scales[4]);
  snprintf (maxes[3], 19, "%g", scales[5]);
  snprintf (maxes[4], 19, "%g", scales[6]);
  snprintf (maxes[5], 19, "%g", scales[7]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_1A_entry")), maxes[0]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_1B_entry")), maxes[1]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_2A_entry")), maxes[2]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget 
				 (xml, "ternary_2B_entry")), maxes[3]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_3A_entry")), maxes[4]);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget
				 (xml, "ternary_3B_entry")), maxes[5]);

  ternary_disp = ternary_triangle_disp;
}

void change_ternary (GtkWidget *widget, gpointer user_data)
{
  GtkWidget *ternary_options, *ternary_menu, *ternary_item, *ternary_items [6];
  int i, ierr;

  /* Determine the new scale type setting */
  ternary_options = glade_xml_get_widget (xml, "ternary_scale_menu");
  ternary_menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (ternary_options));
  ternary_item = gtk_menu_get_active (GTK_MENU (ternary_menu));
  ternary_items [0] = glade_xml_get_widget (xml, "general_triangle");
  ternary_items [1] = glade_xml_get_widget (xml, "unit_triangle");
  ternary_items [2] = glade_xml_get_widget (xml, "equil_triangle");
  ternary_items [3] = glade_xml_get_widget (xml, "general_rect");
  ternary_items [4] = glade_xml_get_widget (xml, "unit_square");
  ternary_items [5] = glade_xml_get_widget (xml, "square");
  for (i=0; i<6 && ternary_item != ternary_items[i]; i++) ;
  if (i==6)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Unrecognized ternary scale type");
      exit (1);
    }
  thistern = (terntype) i;
  DPRINTF ("Changing ternary scale type from %d to %d\n", (int) lastern, i);

  if ((lastern==GEN_TRI || lastern==UNIT_TRI || lastern==EQ_TRI) &&
      (thistern==GEN_RECT || thistern==UNIT_SQ || thistern==SQUARE))
    triangle_to_square ();
  if ((lastern==GEN_RECT || lastern==UNIT_SQ || lastern==SQUARE) &&
      (thistern==GEN_TRI || thistern==UNIT_TRI || thistern==EQ_TRI))
    square_to_triangle ();
  on_ternary_scale_area_expose_event
    (glade_xml_get_widget (xml, "ternary_scale_area"), NULL, NULL);

  /* Sensitize/desensitize the appropriate boxes */
  if (!ternary_auto_set)
    {
      if (thistern==UNIT_TRI || thistern==UNIT_SQ)
	{
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1A_label"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1B_label"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2A_label"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1A_entry"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1B_entry"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2A_entry"), FALSE);
	}
      else
	{
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1A_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1B_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2A_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1A_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_1B_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2A_entry"), TRUE);
	}
      if (thistern==GEN_TRI || thistern==GEN_RECT)
	{
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2B_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3A_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3B_label"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2B_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3A_entry"), TRUE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3B_entry"), TRUE);
	}
      else
	{
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2B_label"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3A_label"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3B_label"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_2B_entry"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3A_entry"), FALSE);
	  gtk_widget_set_sensitive
	    (glade_xml_get_widget (xml, "ternary_3B_entry"), FALSE);
	}
    }

  /* Render the composition field and scale/diffusion path */
  for (i=0; i<num_fields; i++)
    if (fieldtypes [i] == FIELD_TERNARY ||
	fieldtypes [i] == FIELD_TERNARY_SQUARE)
      fieldtypes [i] =
	(thistern==GEN_RECT || thistern==UNIT_SQ || thistern==SQUARE) ?
	FIELD_TERNARY_SQUARE : FIELD_TERNARY;
  render_dataviews ();

  lastern = thistern;
}

void on_vector_auto_checkbutton_toggled
(GtkWidget *thebutton, gpointer user_data)
{
  vector_auto_set = gtk_toggle_button_get_active
    (GTK_TOGGLE_BUTTON (thebutton));
  if (vector_auto_set)
    {
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_max_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_symm_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_max_entry"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_symm_spinbutton"), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_max_label"), TRUE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_symm_label"), TRUE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_max_entry"), TRUE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "vector_symm_spinbutton"), TRUE);
    }
}

void on_shear_auto_checkbutton_toggled
(GtkWidget *thebutton, gpointer user_data)
{
  shear_auto_set = gtk_toggle_button_get_active
    (GTK_TOGGLE_BUTTON (thebutton));
  if (shear_auto_set)
    {
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "shear_max_label"), FALSE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "shear_max_entry"), FALSE);
    }
  else
    {
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "shear_max_label"), TRUE);
      gtk_widget_set_sensitive
	(glade_xml_get_widget (xml, "shear_max_entry"), TRUE);
    }
}

void on_scalar_min_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", scales); }

void on_scalar_max_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", scales+1); }

void on_ternary_1A_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf",
	  (thistern==GEN_RECT || thistern==UNIT_SQ || thistern==SQUARE) ?
	  scales+8 : scales+2); }

void on_ternary_1B_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf",
	  (thistern==GEN_RECT || thistern==UNIT_SQ || thistern==SQUARE) ?
	  scales+9 : scales+3); }

void on_ternary_2A_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf",
	  (thistern==GEN_RECT || thistern==UNIT_SQ || thistern==SQUARE) ?
	  scales+10 : scales+4); }

void on_ternary_2B_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf",
	  (thistern==GEN_RECT || thistern==UNIT_SQ || thistern==SQUARE) ?
	  scales+11 : scales+5); }

void on_ternary_3A_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", scales+6); }

void on_ternary_3B_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", scales+7); }

void on_ternary_dp_red_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", ternary_dp_color); }

void on_ternary_dp_green_entry_changed(GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", ternary_dp_color+1); }

void on_ternary_dp_blue_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", ternary_dp_color+2); }

void on_ternary_dp_alpha_entry_changed(GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", ternary_dp_color+3); }

void on_path_filename_entry_activate (GtkWidget *theentry, gpointer user) {
  G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  FILE *path_file;
  gchar linebuf[200];
  int color=-1, point=0, point_array_size=0, ierr;
  DPRINTF ("Filename entered: %s\n", entrytext);

  if (!(path_file=fopen(entrytext, "r"))) {
    ierr=PetscPrintf (PETSC_COMM_WORLD, "Unable to open: %s\n", entrytext);
    CHKERRQ (ierr);
    return; }

  dp_supp_colors=0;
  while (fgets (linebuf, 199, path_file))
    {
      if (linebuf[0] == 'c' || linebuf[0] == 'C')
	{
	  /* Expand the color buffers */
	  dp_supp_colors++;
	  dp_supp_color_points = realloc
	    (dp_supp_color_points, dp_supp_colors*sizeof(int));
	  dp_supp_red = realloc
	    (dp_supp_red, dp_supp_colors*sizeof(PetscScalar));
	  dp_supp_green = realloc
	    (dp_supp_green, dp_supp_colors*sizeof(PetscScalar));
	  dp_supp_blue = realloc
	    (dp_supp_blue, dp_supp_colors*sizeof(PetscScalar));
	  dp_supp_alpha = realloc
	    (dp_supp_alpha, dp_supp_colors*sizeof(PetscScalar));

	  /* Properties of the new color entry */
	  color = dp_supp_colors-1;
	  dp_supp_color_points [color] = 0;
	  sscanf (linebuf, "%*s %lf %lf %lf %lf", dp_supp_red+color,
		  dp_supp_green+color,dp_supp_blue+color,dp_supp_alpha+color);
	}
      if (((linebuf[0] >= '0' && linebuf[0] <= '9') || linebuf[0] == '.') &&
	  color > -1)
	{
	  if (point >= point_array_size)
	    dp_supp_AB = realloc
	      (dp_supp_AB,
	       (point_array_size=(point_array_size?point_array_size*2:100))*
	       2*sizeof(PetscScalar));
	  sscanf (linebuf, "%lf %lf", dp_supp_AB+2*point,
		  dp_supp_AB+2*point+1);
	  dp_supp_color_points[color]++;
	  point++;
	}
    }
  fclose (path_file);

#ifdef DEBUG
  ierr=PetscPrintf (PETSC_COMM_WORLD, "Loaded path supplement:\n");
  CHKERRQ (ierr);
  for (color=0, point=0; color<dp_supp_colors; color++)
    {
      ierr=PetscPrintf (PETSC_COMM_WORLD, "%d: %d points, color %g %g %g %g\n",
			color, dp_supp_color_points[color], dp_supp_red[color],
			dp_supp_green[color], dp_supp_blue[color],
			dp_supp_alpha[color]);
      CHKERRQ (ierr);
      for (ierr=0; ierr<dp_supp_color_points[color]; ierr++, point++)
	PetscPrintf (PETSC_COMM_WORLD, " %g %g\n", dp_supp_AB [point*2],
		     dp_supp_AB [point*2+1]);
    }
#endif

  render_dataviews();
}

void on_vector_max_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", scales+13); }

void on_vector_symm_spinbutton_changed (GtkWidget *theentry,gpointer user_data)
{ int symmetry;
  G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%d", &symmetry);
  /* Because this sometimes sends events with outrageous symmetry numbers */
  if (symmetry <= 10) {
    render_scale_2d (vector_disp, FIELD_VECTOR, symmetry);
    on_vector_scale_area_expose_event
      (glade_xml_get_widget (xml, "vector_scale_area"), NULL, user_data); }}

void on_shear_max_entry_changed (GtkWidget *theentry, gpointer user_data)
{ G_CONST_RETURN gchar *entrytext = gtk_entry_get_text (GTK_ENTRY (theentry));
  sscanf (entrytext, "%lf", scales+14); }

void change_variable (GtkWidget *widget, gpointer user_data)
{
  current_field = GPOINTER_TO_INT (widget);
  DPRINTF ("Switching to variable %d\n", current_field);
  gtk_notebook_set_current_page
    (GTK_NOTEBOOK (glade_xml_get_widget (xml, "scale_notebook")),
     fieldtypes [current_field] >> 4);
  render_dataviews();
}

void on_mag_spin_value_changed (GtkWidget *mag_spin, gpointer user_data) {
  G_CONST_RETURN gchar *entrytext;
  entrytext = gtk_entry_get_text (GTK_ENTRY (mag_spin));
  sscanf (entrytext, "%lf", &sizemag);
  width = (int) (minmax [1] * sizemag);
  height = (int) (minmax [3] * sizemag);

  if (width == 0)
    width = DEFAULT_RENDER_SIZE;
  if (height == 0)
    height = DEFAULT_RENDER_SIZE;

  render_dataviews();
}


void display_timestep (int usermetacount, char **usermetanames,
		       char **usermetadata)
{
  int i;
  static char step_buffer [20], time_buffer [20];

  for (i=0; i<usermetacount; i++)
    {
      if (!strncmp (usermetanames [i], "timestep", 8))
	sscanf (usermetadata [i], "%d", &current_timestep);
      else if (!strncmp (usermetanames [i], "time", 4))
	sscanf (usermetadata [i], "%lf", &current_time);
    }
  snprintf (step_buffer, 19, "Timestep: %d", current_timestep);
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (xml, "timestep_label")),
		      step_buffer);
  snprintf (time_buffer, 19, "Time: %g", current_time);
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (xml, "time_label")),
		      time_buffer);

  on_mag_spin_value_changed (glade_xml_get_widget (xml, "mag_spin"), NULL);
}


/*++++++++++++++++++++++++++++++++++++++
  This function saves both the current field image and also the scale image.

  GtkWidget *widget Standard GTK+ callback argument, ignored here.

  gpointer user_data Standard GTK+ callback argument, ignored here.
  ++++++++++++++++++++++++++++++++++++++*/

void on_save_activate (GtkWidget *widget, gpointer user_data)
{
  int i, ierr;
  char filename [200], number[10];

  strncpy (filename, basedirname, 198);
  strcat (filename, "/");
  strncat (filename, stepnames [entrynum], 198 - strlen (filename));
  snprintf (number, 9, "-f%d", current_field);
  strncat (filename, number, 198 - strlen (filename));
  strncat (filename, ".ppm", 198 - strlen (filename));
  DPRINTF ("Saving field image with filename %s\n", filename);
  IDispWritePPM (Disp[0], filename);

  if (fieldtypes [current_field] == FIELD_TERNARY_SQUARE ||
      fieldtypes [current_field] == FIELD_TERNARY)
    {
      strncpy (filename, basedirname, 198);
      strcat (filename, "/");
      strncat (filename, stepnames [entrynum], 198 - strlen (filename));
      snprintf (number, 9, "-s%d", current_field);
      strncat (filename, number, 198 - strlen (filename));
      strncat (filename, ".ppm", 198 - strlen (filename));
      DPRINTF ("Saving scale image with filename %s\n", filename);
      IDispWritePPM (ternary_disp, filename);
    }
}


void on_timestep_spin_value_changed (GtkWidget *timestep_spin, gpointer user_data) {
  int usermetacount, ierr;
  G_CONST_RETURN gchar *entrytext;
  char **usermetanames, **usermetadata, filename [200], **field_name;
  GtkWidget *variable_options, *variable_menu, **variable_item;

  entrytext = gtk_entry_get_text (GTK_ENTRY (timestep_spin));
  sscanf (entrytext, "%d", &entrynum);

  /* Bound the entrynum between 0 and total_entries-1; -11 is the minimum of
     the widget (from jump), 1000001 is the maximum. */
  if ((entrynum < 0 && entrynum != -11) || entrynum == 1000001)
    entrynum = total_entries-1;
  if ((entrynum >= total_entries && entrynum != 1000001) || entrynum == -11)
    entrynum = 0;
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (timestep_spin),
			     (gfloat) entrynum);

  strncpy (filename, basedirname, 198);
  strcat (filename, "/");
  strncat (filename, stepnames [entrynum], 198 - strlen (filename));

  ierr = IlluMultiRead (PETSC_COMM_WORLD, theda, global, filename,
			&usermetacount,&usermetanames, &usermetadata);
  CHKERRQ (ierr);

  display_timestep (usermetacount, usermetanames, usermetadata);
}


void on_transform_activate (GtkWidget *widget, gpointer user_data)
{
  GtkWidget *timestep_spin = glade_xml_get_widget (xml, "timestep_spin"),
    *flip_horiz = glade_xml_get_widget (xml, "flip_horiz"),
    *flip_vertical = glade_xml_get_widget (xml, "flip_vertical"),
    *rotate_left = glade_xml_get_widget (xml, "rotate_left");

  transform =
    (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (flip_horiz)) ?
     FLIP_HORIZONTAL : 0) |
    (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (flip_vertical)) ?
     FLIP_VERTICAL : 0) |
    (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (rotate_left)) ?
     ROTATE_LEFT : 0);

  render_dataviews ();
}


void on_save_all_activate (GtkWidget *none, gpointer user_data) {
  GtkWidget *timestep_spin = glade_xml_get_widget (xml, "timestep_spin");
  int i, temp = entrynum;

  for (i=0; i<total_entries; i++)
    {
      gchar appbar_message [30];
      gtk_spin_button_set_value (GTK_SPIN_BUTTON (timestep_spin), (gfloat) i);
      on_timestep_spin_value_changed (timestep_spin, user_data);

      sprintf (appbar_message, "Saving image %d/%d", i+1, total_entries);
      gnome_appbar_set_status
	(GNOME_APPBAR (glade_xml_get_widget (xml, "main_appbar")),
	 appbar_message);
      gnome_appbar_set_progress_percentage
	(GNOME_APPBAR (glade_xml_get_widget (xml, "main_appbar")),
	 (gfloat) (i+1)/total_entries);
      while (gtk_events_pending ())
	gtk_main_iteration ();

      on_save_activate (timestep_spin, user_data);
    }

  gnome_appbar_set_progress_percentage
    (GNOME_APPBAR (glade_xml_get_widget (xml, "main_appbar")), 0.);
  gnome_appbar_refresh (GNOME_APPBAR(glade_xml_get_widget(xml,"main_appbar")));
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (timestep_spin), (gfloat) temp);
  on_timestep_spin_value_changed (timestep_spin, user_data);
}


void on_refresh_activate (GtkWidget *none, gpointer user_data) {
  if (refresh_stepnames ()) exit (1); }


/*++++++++++++++++++++++++++++++++++++++
  This reloads the .log file.

  GtkWidget *none Empty GtkWidget (unusable because it's a menu item).

  gpointer user_data Empty pointer.
  ++++++++++++++++++++++++++++++++++++++*/

void on_log_reload_button_clicked (GtkWidget *none, gpointer user_data)
{
  FILE *run_log;
  unsigned char run_log_filename [300], run_log_buffer [300];
  int log_size=0, nextchar=0;

  strcpy ((char *) run_log_buffer, "Loading new log info");
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (xml, "log_text_label")),
		      (char *) run_log_buffer);

  strncpy ((char *) run_log_filename, the_basename, 298-strlen(".log"));
  strcat ((char *) run_log_filename, ".log");
  DPRINTF ("Loading log file %s\n", run_log_filename);
  if (!(run_log = fopen ((char *) run_log_filename, "r")))
    {
      printf ("Can't find log file %s\n", run_log_filename);
      return;
    }

  /* There's probably a MUCH better way to slurp a file into a string... */
  while (nextchar != EOF)
    {
      int i,j;
      for (i=0; i<300 && nextchar != EOF; i++)
	run_log_buffer[i] = nextchar = fgetc (run_log);
      log_text = (char *) realloc
	(log_text, (log_size += i) * sizeof (char));
      for (j=0; j<i; j++)
	log_text [log_size-i+j] = run_log_buffer [j];
    }
  log_text [log_size-1] = '\0';
  fclose (run_log);
  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (xml, "log_text_label")),
		      log_text);
}


void on_run_log_activate (GtkWidget *none, gpointer user_data) {
  if (!log_text)
    on_log_reload_button_clicked (none, user_data);
  gtk_widget_show (glade_xml_get_widget (xml, "log_window")); }


void on_about_activate (GtkWidget *none, gpointer user_data) {
  gtk_widget_show (glade_xml_get_widget (xml, "about")); }


#undef __FUNCT__
#define __FUNCT__ "main"

/*++++++++++++++++++++++++++++++++++++++
  This is
  +latex+{\tt main()}.
  +html+ <tt>main()</tt>.

  int main It returns an int to the OS.

  int argc Argument count.

  char *argv[] Arguments.
  ++++++++++++++++++++++++++++++++++++++*/

int main (int argc, char *argv[])
{
  int usermetacount=0, i, ierr;
  char **usermetanames, **usermetadata, filename [200], **field_name;
  G_CONST_RETURN gchar *entrytext;
  GtkWidget *variable_options, *variable_menu, **variable_item,
    *ternary_options, *ternary_menu, *ternary_item, *ternary_items [6];

  /*+ After
    +latex+{\tt PETSc}
    +html+ <tt>PETSc</tt>
    and glade/GNOME initialization, it gets the list of files matching the
    basename. +*/
  ierr = PetscInitialize (&argc, &argv, (char *)0, help); CHKERRQ (ierr);

  if (argc<2)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Usage: tsview basename\n");
      CHKERRQ (ierr);
      return 1;
    }

#ifdef DEBUG
  ierr = PetscPrintf (PETSC_COMM_WORLD, "Command line:"); CHKERRQ (ierr);
  for (i=0; i<argc; i++)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, " %s", argv[i]); CHKERRQ (ierr);
    }
  ierr = PetscPrintf (PETSC_COMM_WORLD, "\n"); CHKERRQ (ierr);
#endif

  /* Initial settings */
  ierr = PetscOptionsHasName (PETSC_NULL, "-no_transparency", &transp);
  CHKERRQ (ierr);
  transp = !transp;
  Surf = NULL;
  transform = 0;

  /* Kludge alert!  Setting argc to avoid gnome_program_init errors;
     fix: use GNOME arguments instead of PETSc. */
  argc=2;

  DPRINTF ("Running gnome_program_init with argc=%d\n", argc);
  gnome_program_init ("TSView", VERSION, LIBGNOMEUI_MODULE, argc, argv, NULL);

  strncpy (filename, GLADE_DIRECTORY, 187);
  strcat (filename, "/tsview.glade");
  DPRINTF ("Loading Glade file %s\n", filename);
  xml = glade_xml_new (filename, NULL, NULL);

  DPRINTF ("Autoconnecting...\n",0);
  glade_xml_signal_autoconnect (xml);
  DPRINTF ("Done.\n",0);

  if (argc>1)
    the_basename = argv[1];
  else
    {
      /* Put in filter for .time0000000.cpu0000.meta */
      gtk_widget_show (glade_xml_get_widget (xml, "open_fileselect"));
    }

  DPRINTF ("Loading list of timestep names\n",0);
  if (refresh_stepnames ())
    {
      ierr = PetscFinalize (); CHKERRQ(ierr);
      exit (1);
    }

  DPRINTF ("Loading first timestep, creating distributed array\n",0);
  strncpy (filename, basedirname, 198);
  strcat (filename, "/");
  strncat (filename, stepnames [0], 198 - strlen (stepnames [0]));
  ierr = IlluMultiLoad
    (PETSC_COMM_WORLD, filename, &theda, minmax+1,minmax+3,minmax+5,
     &fieldtypes, &usermetacount, &usermetanames, &usermetadata);
  CHKERRQ (ierr);

  /* Usermetadata xwidth, ywidth, zwidth override minmax in case IlluMulti
     version of saved data is 0.1. */
  DPRINTF ("Checking usermetadata for width information\n",0);
  for (i=0; i<usermetacount; i++)
    {
      if (!strncmp (usermetanames [i], "xwidth", 6))
	sscanf (usermetadata [i], "%lf", minmax+1);
      else if (!strncmp (usermetanames [i], "ywidth", 6))
	sscanf (usermetadata [i], "%lf", minmax+3);
      else if (!strncmp (usermetanames [i], "zwidth", 6))
	sscanf (usermetadata [i], "%lf", minmax+5);
    }

  DPRINTF ("Getting distributed array global vector and info\n",0);
  ierr = DAGetGlobalVector (theda, &global); CHKERRQ (ierr);
  ierr = DAGetInfo (theda, &dimensions, PETSC_NULL,PETSC_NULL,PETSC_NULL,
		    PETSC_NULL,PETSC_NULL,PETSC_NULL, &num_fields,
		    PETSC_NULL,PETSC_NULL,PETSC_NULL); CHKERRQ (ierr);
  if (dimensions == 1)
    SETERRQ (PETSC_ERR_ARG_OUTOFRANGE, "tsview-ng only supports 2-D or 3-D distributed arrays.")
  else if (dimensions == 2)
    bpp = 3;
  else
    {
      bpp = 4;
      gtk_widget_hide (glade_xml_get_widget (xml, "flip_horiz"));
      gtk_widget_hide (glade_xml_get_widget (xml, "flip_vertical"));
      gtk_widget_hide (glade_xml_get_widget (xml, "rotate_left"));
    }

  /* Build menu of field variables */
  variable_options = glade_xml_get_widget (xml, "variable_menu");
  gtk_option_menu_remove_menu (GTK_OPTION_MENU (variable_options));
  variable_menu = gtk_menu_new ();
  variable_item = (GtkWidget **) malloc (num_fields * sizeof (GtkWidget *));
  field_name = (char **) malloc (num_fields * sizeof (char *));
  field_index = (int *) malloc (num_fields * sizeof (int));
  /* For now, use scalar plots for all types in 3-D */
  if (dimensions == 2)
    field_indices (num_fields, dimensions, fieldtypes, field_index);
  else
    for (i=0; i<num_fields; i++)
      field_index[i] = i;
  DPRINTF ("Field indices:\n",0);
  for (i=0; i<num_fields && field_index [i] != -1; i++)
    {
      ierr = DAGetFieldName (theda, field_index [i], field_name+i);
      CHKERRQ (ierr);
      DPRINTF ("%d index %d name %s\n", i, field_index [i], field_name [i]);
      variable_item [i] = gtk_menu_item_new_with_label (field_name [i]);
      gtk_menu_append (GTK_MENU (variable_menu), variable_item [i]);
      gtk_signal_connect_object (GTK_OBJECT (variable_item [i]), "activate",
				 GTK_SIGNAL_FUNC (change_variable),
				 GINT_TO_POINTER (field_index [i]));
      gtk_widget_show (variable_item [i]);
    }
  gtk_option_menu_set_menu (GTK_OPTION_MENU (variable_options), variable_menu);
  gtk_widget_show (variable_menu);
  gtk_widget_show (variable_options);

  /* Ternary type */
  ternary_options = glade_xml_get_widget (xml, "ternary_scale_menu");
  ternary_menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (ternary_options));
  ternary_item = gtk_menu_get_active (GTK_MENU (ternary_menu));
  ternary_items [0] = glade_xml_get_widget (xml, "general_triangle");
  ternary_items [1] = glade_xml_get_widget (xml, "unit_triangle");
  ternary_items [2] = glade_xml_get_widget (xml, "equil_triangle");
  ternary_items [3] = glade_xml_get_widget (xml, "general_rect");
  ternary_items [4] = glade_xml_get_widget (xml, "unit_square");
  ternary_items [5] = glade_xml_get_widget (xml, "square");
  for (i=0; i<6 && ternary_item != ternary_items[i]; i++) ;
  if (i==6)
    {
      ierr = PetscPrintf (PETSC_COMM_WORLD, "Unrecognized ternary scale type");
      exit (1);
    }
  lastern = thistern = (terntype) i;

  /* Scale images */
  entrytext = gtk_entry_get_text
    (GTK_ENTRY (glade_xml_get_widget (xml, "scale_size_entry")));
  sscanf (entrytext, "%d", &scale_size);
  IDispCreate (&scalar_disp, scale_size, scale_size, scale_size, SCALE_BPP, 0);
  IDispCreate (&vector_disp, scale_size, scale_size, scale_size, SCALE_BPP, 0);
  IDispCreate (&shear_disp, scale_size, scale_size, scale_size, SCALE_BPP, 0);
  IDispCreate (&ternary_triangle_disp, scale_size, scale_size, scale_size,
	       SCALE_BPP, 0);
  IDispCreate (&ternary_square_disp, scale_size, scale_size, scale_size,
	       SCALE_BPP, 0);
  ternary_disp = ternary_triangle_disp;
  render_scale_2d (scalar_disp, FIELD_SCALAR, 1);
  render_scale_2d (vector_disp, FIELD_VECTOR, 1);
  render_scale_2d (shear_disp, FIELD_TENSOR_SHEAR, 1);

  gtk_notebook_set_current_page
    (GTK_NOTEBOOK (glade_xml_get_widget (xml, "scale_notebook")),
     fieldtypes [0] >> 4);

  /* Main window title */
  {
    char main_window_title[100] = "TSView: ";
    GtkWidget *main_window = glade_xml_get_widget (xml, "main_window");

    strncat (main_window_title, basename (the_basename), 90);
    gtk_window_set_title (GTK_WINDOW (main_window), main_window_title);
    gtk_widget_show (main_window);
  }

  /* Set initial magnification */
  sizemag = DEFAULT_RENDER_SIZE/PetscMax(minmax[1],minmax[3]);
  DPRINTF ("Max dimension is %g, setting magnification to %g\n",
	   PetscMax(minmax[1],minmax[3]), sizemag);
  gtk_spin_button_set_value
    (GTK_SPIN_BUTTON (glade_xml_get_widget (xml, "mag_spin")), sizemag);

  DPRINTF ("Displaying first timestep\n",0);
  display_timestep (usermetacount, usermetanames, usermetadata);

  DPRINTF ("Running main loop\n",0);
  gtk_main();

  DPRINTF ("Finalizing and exiting.\n",0);
  if (Surf) {
    ierr = ISurfDestroy (Surf); CHKERRQ (ierr); }
  if (Disp[0]) {
    ierr = IDispDestroy (Disp[0]); CHKERRQ (ierr); }
  ierr = PetscFinalize (); CHKERRQ(ierr);
  return 0;
}
