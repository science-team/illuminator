/***************************************
  $Header$

  This is a Glade front end to the little phase field/fluid structure
  interactions program based on PETSc, but can serve as a front end to a
  variety of PETSc programs with minor adjustments.

  Callback functions are grouped here according to where they appear in the
  main window (except on_run_clicked is below main window items), then the run
  control dialog, and save dialog.

  I haven't put function comments in to keep the line count down, the functions
  are generally pretty simple GTK+ callbacks.
***************************************/


/* Includes, using GTK_ENABLE_BROKEN to enable the GtkText widget for now. */
#include <glade/glade.h>
#include <gnome.h>
#include <libgnomeui/libgnomeui.h>
#include <stdio.h>
#include <math.h>

GladeXML *xml;
FILE *simulation_input_file, *simulation_output_file;

/* Simple debugging macro */
#ifdef DEBUG
#define DPRINTF printf
#else
#define DPRINTF(fmt...)
#endif

/* Discretization stuff */
double Lx;
int nx;

void on_width_changed (GtkWidget *width, gpointer user_data) {
  G_CONST_RETURN gchar *entrytext;
  entrytext = gtk_entry_get_text (GTK_ENTRY (width));
  sscanf (entrytext, "%lf", &Lx);
  DPRINTF ("Width set to %g\n", Lx); }

void on_resolution_changed (GtkWidget *resolution, gpointer user_data) {
  /* Note: gtk_spin_button_get_value_as_int doesn't respond to typed entry */
  nx = gtk_spin_button_get_value (GTK_SPIN_BUTTON (resolution));
  DPRINTF ("Resolution set to %d\n", nx); }

/* Timestep stuff */
double dt, dt_factor, dt_max;
int last_tstep;

void on_timestep_changed (GtkWidget *timestep, gpointer user_data) {
  G_CONST_RETURN gchar *entrytext;
  entrytext = gtk_entry_get_text (GTK_ENTRY (timestep));
  sscanf (entrytext, "%lf", &dt);
  DPRINTF ("Timestep set to %g\n", dt); }

void on_time_factor_changed (GtkWidget *time_factor, gpointer user_data) {
  G_CONST_RETURN gchar *entrytext;
  entrytext = gtk_entry_get_text (GTK_ENTRY (time_factor));
  sscanf (entrytext, "%lf", &dt_factor);
  DPRINTF ("Time factor set to %g\n", dt_factor); }

void on_max_timestep_changed (GtkWidget *max_timestep, gpointer user_data) {
  G_CONST_RETURN gchar *entrytext;
  entrytext = gtk_entry_get_text (GTK_ENTRY (max_timestep));
  sscanf (entrytext, "%lf", &dt_max);
  DPRINTF ("Max timestep set to %g\n", dt_max); }

void on_last_timestep_changed (GtkWidget *last_timestep, gpointer user_data) {
  /* Note: gtk_spin_button_get_value_as_int doesn't respond to typed entry */
  last_tstep = gtk_spin_button_get_value (GTK_SPIN_BUTTON (last_timestep));
  DPRINTF ("Last timestep set to %d\n", last_tstep); }

/* Display stuff */
gboolean display_x, display_text;

void on_xdisplay_toggled (GtkWidget *xdisplay, gpointer user_data) {
  display_x = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (xdisplay));
  DPRINTF (display_x ? "Using X display\n" : "Not using X display\n"); }

void on_textdisplay_toggled (GtkWidget *textdisplay, gpointer user_data) {
  display_text = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(textdisplay));
  DPRINTF(display_text ? "Using text display\n" : "Not using text display\n");}

/* Remote host stuff */
gboolean remote_host;
G_CONST_RETURN gchar *remote_hostname;
gchar thetransport[50];

void on_remote_check_toggled (GtkWidget *remote_check, gpointer user_data) {
  if (remote_host = gtk_toggle_button_get_active
      (GTK_TOGGLE_BUTTON (remote_check))) {
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "transport_label"), TRUE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "transport_options"), TRUE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "remote_host_label"), TRUE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "remote_host"), TRUE); }
  else {
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "transport_label"), FALSE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "transport_options"), FALSE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "remote_host_label"), FALSE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "remote_host"), FALSE); }
  DPRINTF (remote_host ? "Using remote host\n" : "Using local host\n"); }

void on_ssh_item_activate (GtkWidget *widget, gpointer user_data) {
  sprintf (thetransport, "ssh");
  DPRINTF ("Transport type = %s\n", thetransport); }

void on_rsh_item_activate (GtkWidget *widget, gpointer user_data) {
  sprintf (thetransport, "rsh");
  DPRINTF ("Transport type = %s\n", thetransport); }

void on_remote_host_changed (GtkWidget *remote_host, gpointer user_data) {
  remote_hostname = gtk_entry_get_text (GTK_ENTRY (remote_host));
  DPRINTF ("Remote host set to %s\n", remote_hostname); }

/* MPI stuff */
G_CONST_RETURN gchar *mpirun_command;
int number_cpus;

void on_mpirun_changed (GtkWidget *mpirun, gpointer user_data) {
  mpirun_command = gtk_entry_get_text (GTK_ENTRY (mpirun));
  DPRINTF ("MPIrun command set to %s\n", mpirun_command); }

void on_num_cpus_changed (GtkWidget *num_cpus, gpointer user_data) {
  /* Note: gtk_spin_button_get_value_as_int doesn't respond to typed entry */
  number_cpus = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (num_cpus));
  if (number_cpus > 1) {
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "mpirun_label"), TRUE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "mpirun"), TRUE); }
  else {
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "mpirun_label"), FALSE);
    gtk_widget_set_sensitive
      (glade_xml_get_widget (xml, "mpirun"), FALSE); }
  DPRINTF ("Num_CPUs set to %d\n", number_cpus); }

/* Dimensionality */
gboolean twodee;

void on_2d_activate (GtkWidget *unused, gpointer user_data) {
  twodee = TRUE; DPRINTF ("Simulating in two dimensions\n"); }

void on_3d_activate (GtkWidget *unused, gpointer user_data) {
  twodee = FALSE; DPRINTF ("Simulating in three dimensions\n"); }

/* Load and save (unfinished!) */
gchar *options_filename = NULL;
void on_load_ok_clicked (GtkWidget *load_file, gpointer user_data) {
  gtk_widget_hide (load_file); }

void on_save_ok_clicked (GtkWidget *save_file, gpointer user_data) {
  gtk_widget_hide (save_file); }

/* Toggle buttons */
void on_show_options_toggled (GtkWidget *show_options, gpointer user_data) {
  GtkWidget *simulation_options =
    glade_xml_get_widget (xml,"simulation_options");
  GtkWidget *options_notebook = glade_xml_get_widget (xml,"options_notebook");
  gboolean show_options_active =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (show_options));
  gboolean simulation_options_active =
    gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (simulation_options));
  DPRINTF ("on_show_options_toggled: show %d, simulation %d\n",
	   show_options_active, simulation_options_active);

  if (show_options_active != simulation_options_active)
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (simulation_options),
				  show_options_active);
  if (show_options_active)
    gtk_widget_show (options_notebook);
  else {
    GtkWidget *main_window = glade_xml_get_widget (xml, "main_window");
    gtk_widget_hide (options_notebook);
    gtk_window_resize (GTK_WINDOW (main_window), 1, 1); }
}

void on_show_output_toggled (GtkWidget *show_output, gpointer user_data) {
  GtkWidget *simulation_output =
    glade_xml_get_widget (xml,"simulation_output");
  GtkWidget *output_window = glade_xml_get_widget (xml,"output_window");
  gboolean show_output_active =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (show_output));
  gboolean simulation_output_active =
    gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (simulation_output));
  DPRINTF ("on_show_output_toggled: show %d, simulation %d\n",
	   show_output_active, simulation_output_active);

  if (show_output_active != simulation_output_active)
  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (simulation_output),
				  show_output_active);
  if (show_output_active)
    gtk_widget_show (output_window);
  else
    gtk_widget_hide (output_window); }

/* Menu items, mainly calling things above (SO annoying that menu signals don't
   have associated objects!) */
void open_params (GtkWidget *null_widget, gpointer user_data) {
  GtkWidget *load_file = glade_xml_get_widget (xml,"load_file");
  gtk_widget_show (load_file); }

void save_params_as (GtkWidget *null_widget, gpointer user_data) {
  GtkWidget *save_file = glade_xml_get_widget (xml,"save_file");
  gtk_widget_show (save_file); }

void save_params (GtkWidget *null_widget, gpointer user_data) {
  if (options_filename == NULL)
    save_params_as (null_widget, user_data); }

void on_show_options_activate (GtkWidget *null_widget, gpointer user_data) {
  GtkWidget *simulation_options =
    glade_xml_get_widget (xml,"simulation_options");
  GtkWidget *show_options = glade_xml_get_widget (xml,"show_options");
  gboolean simulation_options_active =
    gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (simulation_options));
  gboolean show_options_active =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (show_options));
  DPRINTF ("on_show_options_activate: show %d, simulation %d\n",
	   show_options_active, simulation_options_active);

  /* They will be unequal if this one was activated */
  if (simulation_options_active != show_options_active)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (show_options),
				  simulation_options_active); }

void on_show_output_activate (GtkWidget *null_widget, gpointer user_data) {
  GtkWidget *simulation_output =
    glade_xml_get_widget (xml,"simulation_output");
  GtkWidget *show_output = glade_xml_get_widget (xml,"show_output");
  gboolean simulation_output_active =
    gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (simulation_output));
  gboolean show_output_active =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (show_output));
  DPRINTF ("on_show_output_activate: show %d, simulation %d\n",
	   show_output_active, simulation_output_active);

  /* They will be unequal if this one was activated */
  if (simulation_output_active != show_output_active)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (show_output),
				  simulation_output_active); }

void on_about_activate (GtkWidget *null_widget, gpointer user_data) {
  GtkWidget *about_window = glade_xml_get_widget (xml,"about_window");
  gtk_widget_show (about_window); }

/* The run button! */
gint pipe_input_tag; /* Tag number for gdk_input_add/remove */

/* First an ancillary function to read from the pipe and write to the window */
void read_simulation_data (gpointer user_data, gint source,
			   GdkInputCondition condition)
{
  int i, len;
  gfloat residual;
  char simulation_input [200];
  gchar start[50], current[50], final[50];
  static GtkWidget *output_text=NULL,
    *timestep_progressbar, *start_timestep, *current_timestep, *final_timestep,
    *newton_progressbar, *start_newton, *current_newton, *final_newton,
    *gcr_progressbar, *start_gcr, *current_gcr, *final_gcr;
  static gfloat orig_gcr=0., orig_newton=0., tol_gcr=5.0E-4, tol_newton=5.0E-4;
  static int orig_timestep = 0;
  void on_stop_clicked (GtkWidget *, gpointer);

  if (output_text == NULL) {
    output_text = glade_xml_get_widget (xml, "output_text");
    timestep_progressbar = glade_xml_get_widget (xml, "timestep_progressbar");
    start_timestep = glade_xml_get_widget (xml, "start_timestep");
    current_timestep = glade_xml_get_widget (xml, "current_timestep");
    final_timestep = glade_xml_get_widget (xml, "final_timestep");
    newton_progressbar = glade_xml_get_widget (xml, "newton_progressbar");
    start_newton = glade_xml_get_widget (xml, "start_newton");
    current_newton = glade_xml_get_widget (xml, "current_newton");
    final_newton = glade_xml_get_widget (xml, "final_newton");
    gcr_progressbar = glade_xml_get_widget (xml, "gcr_progressbar");
    start_gcr = glade_xml_get_widget (xml, "start_gcr");
    current_gcr = glade_xml_get_widget (xml, "current_gcr");
    final_gcr = glade_xml_get_widget (xml, "final_gcr"); }

  /* Read text until end-of-line */
  if (fgets (simulation_input, sizeof (simulation_input),
	     simulation_input_file) != NULL) {

    /* KSP residual */
    if (!(strncmp (simulation_input+4, "KSP", 3)) ||
	!(strncmp (simulation_input+5, "KSP", 3)) ||
	!(strncmp (simulation_input+6, "KSP", 3)) ||
	!(strncmp (simulation_input+7, "KSP", 3)) ||
	!(strncmp (simulation_input+8, "KSP", 3))) {
      /* Get current GCR residual, set orig and final if necessary */
      sscanf (simulation_input, "%d KSP Residual norm %f", &i, &residual);
      sprintf (current, "%.4e", residual);
      gtk_label_set_text (GTK_LABEL (current_gcr), current);
      if (i == 0) {
	orig_gcr = residual;
	tol_gcr = residual * 1.e-5;
	sprintf (start, "%.4e", orig_gcr);
	gtk_label_set_text (GTK_LABEL (start_gcr), start);
	sprintf (final, "%.4e", tol_gcr);
	gtk_label_set_text (GTK_LABEL (final_gcr), final); }

      /* Reset when falls below tolerance (it's done), plot progress */
      if (residual < tol_gcr)
	residual = orig_gcr;
      if (residual <= orig_gcr)
	gtk_progress_bar_update (GTK_PROGRESS_BAR (gcr_progressbar),
				 (gfloat) log(orig_gcr/residual) /
				 log(orig_gcr/tol_gcr)); }

    /* Newton residual */
    else if (!(strncmp (simulation_input+4, "SNES", 4)) ||
	!(strncmp (simulation_input+5, "SNES", 4)) ||
	!(strncmp (simulation_input+6, "SNES", 4))) {
      /* Reset GCR orig and progress bar */
      orig_gcr = 0.0;
      gtk_progress_bar_update (GTK_PROGRESS_BAR (gcr_progressbar),
			       (gfloat) 0.0);
      /* Get current Newton residual, set orig and final if necessary */
      sscanf (simulation_input, "%d SNES Function norm %f", &i, &residual);
      sprintf (current, "%.4e", residual);
      gtk_label_set_text (GTK_LABEL (current_newton), current);
      if (i == 0) {
	orig_newton = residual;
	tol_newton = residual * 1.e-8;
	sprintf (start, "%.4e", orig_newton);
	gtk_label_set_text (GTK_LABEL (start_newton), start);
	sprintf (final, "%.4e", tol_newton);
	gtk_label_set_text (GTK_LABEL (final_newton), final); }

      /* Reset when falls below tolerance (it's done), plot progress */
      if (residual < tol_newton)
	residual = orig_newton;
      if (residual <= orig_newton)
	gtk_progress_bar_update (GTK_PROGRESS_BAR (newton_progressbar),
				 (gfloat) log(orig_newton/residual) /
				 log(orig_newton/tol_newton)); }

    /* Timestep */
    else if (!(strncmp (simulation_input, "timestep", 8))) {
      sscanf (simulation_input, "timestep %d", &i);
      sprintf (current, "%d", i);
      gtk_label_set_text (GTK_LABEL (current_timestep), current);
      if (i == 0) {
	sprintf (current, "%d", last_tstep);
	gtk_label_set_text (GTK_LABEL (final_timestep), current); }
      gtk_progress_bar_update (GTK_PROGRESS_BAR (timestep_progressbar),
			       (gfloat) i/last_tstep); }

    /* End of simulation */
    else if (!(strncmp (simulation_input, "Game over", 9)))
      on_stop_clicked
	(glade_xml_get_widget (xml, "output_window"), NULL);

    /* Write the text to the output widget in the run dialog */
    else
      {
	GtkTextBuffer *buffer = gtk_text_view_get_buffer
	  (GTK_TEXT_VIEW (output_text));
	GtkTextIter iter;
	gtk_text_buffer_get_end_iter (buffer, &iter);
	gtk_text_buffer_insert
	  (buffer, &iter, simulation_input, strlen (simulation_input));
      }
  }
}
  

#define MAX_COMMAND_LINE_OPTIONS 22

int from_simulation_pipe[2], to_simulation_pipe[2];

void on_run_activate (GtkWidget *null_widget, gpointer user_data) {
  gchar command_line [MAX_COMMAND_LINE_OPTIONS][50],
    clinestr [MAX_COMMAND_LINE_OPTIONS*50];
  int simulation_pid;
  int command = 0, i;

  /* Font: -misc-fixed-medium-r-semicondensed-*-*-110-c-*-iso10646-1, or
     -misc-fixed-medium-r-normal-*-*-80-*-*-c-*-iso10646-1 (or 70) */

  /* Construct the command line string, of special and standard options */
  if (remote_host) {
    strcpy (command_line[command++], thetransport);
    if (!strcmp (thetransport, "ssh"))
      sprintf (command_line[command++], "-AX");
    strcpy (command_line[command++], remote_hostname); }
  if (number_cpus > 1) {
    sprintf (command_line[command++], "%s", mpirun_command);
    sprintf (command_line[command++], "-np");
    sprintf (command_line[command++], "%d", number_cpus); }
  /* Note that if we add to this list, we must also modify the COMMAND_LINE
     macros above, and also the execlp command below. */
  sprintf (command_line[command], CHTS_PATH);
  strcat (command_line[command++], "/chts");
  sprintf (command_line[command++], "-ts_max_steps");
  sprintf (command_line[command++], "%d", last_tstep);
  sprintf (command_line[command++], "-mx");
  sprintf (command_line[command++], "%d", nx);
  sprintf (command_line[command++], "-my");
  sprintf (command_line[command++], "%d", nx);
  sprintf (command_line[command++], "-mz");
  sprintf (command_line[command++], "%d", nx);
  sprintf (command_line[command++], "-dt");
  sprintf (command_line[command++], "%e", dt);
  if (!display_x)
    sprintf (command_line[command++], "-no_contours");
  if (twodee)
    sprintf (command_line[command++], "-twodee");
  sprintf (command_line[command++], "-ts_monitor");
  sprintf (command_line[command++], "-snes_monitor");
  sprintf (command_line[command++], "-ksp_monitor");

  while (command < MAX_COMMAND_LINE_OPTIONS)
    command_line [command++][0] = '\0';
  sprintf (clinestr, "Command line:");
  for (command = 0;
       command < MAX_COMMAND_LINE_OPTIONS && command_line [command][0];
       command++)
    sprintf (clinestr+strlen(clinestr), " %s", command_line [command]);
  sprintf (clinestr+strlen(clinestr), "\n");
  DPRINTF ("%s", clinestr);
  {
    GtkTextBuffer *buffer = gtk_text_view_get_buffer
      (GTK_TEXT_VIEW (glade_xml_get_widget (xml, "output_text")));
    GtkTextIter iter;
    gtk_text_buffer_get_end_iter (buffer, &iter);
    gtk_text_buffer_insert
      (buffer, &iter, clinestr, strlen (clinestr));
  }

  /* Try to spawn a new simulation process.  Most of this was shamelessly
     ripped from Ken Brakke's Surface Evolver. */
  pipe (from_simulation_pipe); /* from simulation stdout */
  pipe (to_simulation_pipe); /* to simulation stdin */
  simulation_pid = fork ();

  if(simulation_pid==0) { /* child */
    close (0);
    dup (to_simulation_pipe[0]);
    close (to_simulation_pipe[0]);
    close (to_simulation_pipe[1]);
    close (1);
    dup (from_simulation_pipe[1]);
    close (from_simulation_pipe[0]);
    close (from_simulation_pipe[1]);

    /* Change to the correct directory */
    /* if (chdir ("/home/hazelsct/davidch/oscsolid")) {
       perror (command_line[0]); exit (1); } */

    /* signal(SIGINT,SIG_IGN); */
    /* Okay, so this is a retarded way to do it... :-) */
    execlp(command_line[0], command_line[0], command_line[1], command_line[2],
	   command_line[3], command_line[4], command_line[5], command_line[6],
	   command_line[7], command_line[8], command_line[9], command_line[10],
	   command_line[11],command_line[12],command_line[13],command_line[14],
	   command_line[15],command_line[16],command_line[17],command_line[18],
	   command_line[19],command_line[20],command_line[21],NULL);
    perror (command_line[0]); /* only error gets here */
    exit (1);
  }

  /* Chui program execution resumes here */
  close (from_simulation_pipe[1]);
  close (to_simulation_pipe[0]);

  /* simulation_input_file is hooked to stdout of simulation */
  simulation_input_file = fdopen (from_simulation_pipe[0], "r");
  /* simulation_output_file is hooked to stdin of simulation */
  simulation_output_file = fdopen (to_simulation_pipe[1], "w");

  /* Sensitize and desensitize various widgets */
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "model_options_vbox"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "petsc_options_scrolledwindow"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "misc_options_vbox"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "open"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save_as"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "run"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "open_button"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save_button"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save_as_button"), FALSE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "run_button"), FALSE);
  /* gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "pause"), TRUE);
  gtk_widget_set_sensitive
  (glade_xml_get_widget (xml, "stop"), TRUE); */

  /* Show the run control table and text output window */
  /* gtk_widget_show (output_window); */
  if (!(gtk_toggle_button_get_active
	(GTK_TOGGLE_BUTTON (glade_xml_get_widget (xml, "show_output"))))) {
    gtk_toggle_button_set_active
      (GTK_TOGGLE_BUTTON (glade_xml_get_widget (xml, "show_output")), TRUE);
    gtk_widget_show (glade_xml_get_widget (xml, "output_window")); }

  /* Add the input function, this will have gtk_main() call
     read_simulation_data() when input is received on the pipe. */
  pipe_input_tag = gdk_input_add (from_simulation_pipe [0], GDK_INPUT_READ,
				  read_simulation_data, NULL);
}

void on_stop_clicked (GtkWidget *output_window, gpointer user_data) {
  /* This should really send some kind of "stop" signal to chts and wait for a
     reply... interactive options setting, etc. */

  /* Remove the pipe sensitivity and close the files and pipes */
  gdk_input_remove (pipe_input_tag);
  fclose (simulation_input_file);
  fclose (simulation_output_file);
  close (from_simulation_pipe [0]);
  close (to_simulation_pipe [1]);

  /* Make appropriate widget readjustments */
  /* gtk_widget_hide (output_window); */
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "model_options_vbox"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "petsc_options_scrolledwindow"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "misc_options_vbox"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "open"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save_as"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "run"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "open_button"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save_button"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "save_as_button"), TRUE);
  gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "run_button"), TRUE);
  /* gtk_widget_set_sensitive
    (glade_xml_get_widget (xml, "pause"), FALSE);
  gtk_widget_set_sensitive
  (glade_xml_get_widget (xml, "stop"), FALSE); */}

/* Does nothing for now */
void on_pause_clicked (GtkWidget *forgot, gpointer user_data) {
  DPRINTF ("Pause was clicked\n");
  return; }

/* And finally, main() */
int main (int argc, char *argv[])
{
  gchar buff[200];
  GtkWidget *transport_options, *transport_menu, *ssh_item, *rsh_item,
    *dimension_options, *dimension_menu;

  /* Basic init stufff */
  gnome_program_init ("CHUI", VERSION, LIBGNOMEUI_MODULE, argc, argv, NULL);

  /* Load the interface, display the initial window, and connect the signals */
  strncpy (buff, GLADE_DIRECTORY, 187);
  strcat (buff, "/chui.glade");
  xml = glade_xml_new (buff, NULL, NULL);
  glade_xml_signal_autoconnect (xml);

  /* For some reason, option menus don't quite work in Glade, so I had to make
     a popup menu and attach it to the option menu */
  /* transport_options = glade_xml_get_widget (xml, "transport_options");
  gtk_option_menu_remove_menu (GTK_OPTION_MENU (transport_options));
  transport_menu = glade_xml_get_widget (xml, "transport_menu");
  gtk_option_menu_set_menu (GTK_OPTION_MENU(transport_options),transport_menu);
  gtk_widget_show (transport_menu);
  dimension_options = glade_xml_get_widget (xml, "dimension_options");
  gtk_option_menu_remove_menu (GTK_OPTION_MENU (dimension_options));
  dimension_menu = glade_xml_get_widget (xml, "dimension_menu");
  gtk_option_menu_set_menu (GTK_OPTION_MENU(dimension_options),dimension_menu);
  gtk_widget_show (dimension_menu); */

  /* Call handlers to set initial values from Glade defaults */
  on_width_changed (glade_xml_get_widget (xml, "width"), NULL);
  on_resolution_changed (glade_xml_get_widget (xml, "resolution"), NULL);
  on_timestep_changed (glade_xml_get_widget (xml, "timestep"), NULL);
  on_time_factor_changed (glade_xml_get_widget (xml, "time_factor"), NULL);
  on_max_timestep_changed (glade_xml_get_widget (xml, "max_timestep"), NULL);
  on_last_timestep_changed (glade_xml_get_widget (xml, "last_timestep"), NULL);
  on_xdisplay_toggled (glade_xml_get_widget (xml, "xdisplay"), NULL);
  on_textdisplay_toggled (glade_xml_get_widget (xml, "textdisplay"), NULL);
  on_remote_check_toggled (glade_xml_get_widget (xml, "remote_check"), NULL);
  on_ssh_item_activate (NULL, NULL);
  on_remote_host_changed (glade_xml_get_widget (xml, "remote_host"), NULL);
  on_mpirun_changed (glade_xml_get_widget (xml, "mpirun"), NULL);
  on_num_cpus_changed (glade_xml_get_widget (xml, "num_cpus"), NULL);
  on_3d_activate (NULL, NULL);

  /* Off we go! */
  gtk_main();

  return 0;
}
